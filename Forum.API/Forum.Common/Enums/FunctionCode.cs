﻿namespace Forum.Common.Enums
{
    public enum FunctionCode
    {
        #region Hệ thống

        /// <summary>
        /// Quyền
        /// </summary>
        ROLE,

        /// <summary>
        /// Trang
        /// </summary>
        NAVIGATION,

        /// <summary>
        /// Phân quyền
        /// </summary>
        ROLE_NAVIGATION,

        /// <summary>
        /// Người dùng
        /// </summary>
        USER,

        /// <summary>
        /// Ngôn ngữ
        /// </summary>
        LANGUAGE,

        /// <summary>
        /// Thủ thuật
        /// </summary>
        TRICKS,

        /// <summary>
        /// Chủ đề
        /// </summary>
        TOPIC,
        #endregion Hệ thống

    }
}