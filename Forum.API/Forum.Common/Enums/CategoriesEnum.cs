﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Common.Enums
{
    public enum CategoriesEnum
    {
        [Description("Ngôn ngữ")]
        LANGUAGE = 1,

        [Description("Thủ thuật")]
        TRICKS = 2,
    }
}
