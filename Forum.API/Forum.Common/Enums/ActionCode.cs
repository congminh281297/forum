﻿namespace Forum.Common.Enums
{
    public enum ActionCode
    {
        /// <summary>
        ///  Quền xem
        /// </summary>
        V = 1,

        /// <summary>
        /// Quyền sửa
        /// </summary>
        E = 2,

        /// <summary>
        /// Quyền xóa
        /// </summary>
        D = 4,

        /// <summary>
        /// Quyền xem, sửa, xóa
        /// </summary>
        V_E_D = 7,

        /// <summary>
        /// Quyền xem, sửa
        /// </summary>
        V_E = 3,

        /// <summary>
        /// Quyền xem, xóa
        /// </summary>
        V_D = 5,

        /// <summary>
        /// Quyền sửa, xóa
        /// </summary>
        E_D = 6
    }
}