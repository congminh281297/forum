﻿using System.Collections.Generic;

namespace Forum.Common.Paged
{
    public class PagedResult<T> where T : class
    {
        public PagedResult()
        {
            Results = new List<T>();
        }

        public int Total { get; set; }
        public IList<T> Results { get; set; }
    }
}