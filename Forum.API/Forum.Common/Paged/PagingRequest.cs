﻿namespace Forum.Common.Paged
{
    public abstract class PagingRequest
    {
        public PagingRequest()
        {
            Page = 1;
            PageSize = 10;
            Column = "";
            Direction = "asc";
        }

        public int Page { get; set; }
        public int PageSize { get; set; }

        /// <summary>
        /// field
        /// </summary>
        public string Column { get; set; }

        /// <summary>
        /// asc | desc
        /// </summary>
        public string Direction { get; set; }
    }
}