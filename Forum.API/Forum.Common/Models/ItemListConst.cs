﻿using System.Text.Encodings.Web;
using System.Text.Json;

namespace Forum.Common.Models
{
    public class ItemListConst
    {
        public int Key { get; set; }
        public string Value { get; set; }
    }
}