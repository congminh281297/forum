﻿namespace Forum.Common.Constants
{
    public static class ClaimConsts
    {
        public const string UserId = "userId";
        public const string UserName = "userName";
        public const string FullName = "fullName";
        public const string Email = "email";
        public const string Image = "image";
    }
}