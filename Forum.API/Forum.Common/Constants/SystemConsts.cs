﻿namespace Forum.Common.Constants
{
    public class SystemConsts
    {
        public const string CorsPolicy = "ForumCorsPolicy";
        public const string JwtTokens = "JwtTokens";
        public const string SigningKey = "0123456789ABCDEF";
        public const string Swagger = "Swagger";
        public const string OpenApiSecurity = "OpenApiSecurity";
        public const string MigrationsAssembly = "Forum.Data";
        public const string UploadConfiguration = "UploadConfiguration";
        public const string RoleAdministrator = "Administrator";
    }
}