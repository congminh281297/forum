﻿using ImageMagick;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Common.Helpers
{
    public static class FileHelper
    {
        public static readonly List<string> ImageMimeTypes = new List<string>
                                   {
                                       "image/jpg",
                                       "image/jpeg",
                                       "image/pjpeg",
                                       "image/gif",
                                       "image/x-png",
                                       "image/png"
                                   };

        public static readonly List<string> DocumentMimeTypes = new List<string>
                                   {
                                       "application/msword",
                                       "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                       "application/vnd.ms-excel",
                                       "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                       "application/pdf"
                                   };

        public static readonly List<string> ImageFileExtensions = new List<string>
                                   {
                                       ".jpg",
                                       ".png",
                                       ".gif",
                                       ".jpeg"
                                   };

        public static readonly List<string> DocumentFileExtensions = new List<string>
                                   {
                                       ".doc",
                                       ".docx",
                                       ".xls",
                                       ".xlsx",
                                       ".pdf"
                                   };

        public static bool IsImageFile(this IFormFile file)
        {
            var contentType = file.ContentType.ToLower();

            // Check the file MIME type
            if (ImageMimeTypes.All(x => x != contentType))
            {
                return false;
            }

            // Check the file extension
            if (ImageFileExtensions.All(x => !file.FileName.ToLower().EndsWith(x)))
            {
                return false;
            }

            return true;
        }

        public static bool IsDocumentFile(this IFormFile file)
        {
            var contentType = file.ContentType.ToLower();

            // Check the file MIME type
            if (DocumentMimeTypes.All(x => x != contentType))
            {
                return false;
            }

            // Check the file extension
            if (DocumentFileExtensions.All(x => !file.FileName.ToLower().EndsWith(x)))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Rotate image and convert to stream
        /// Fix the issue: image auto rotate if width less than height
        ///
        /// </summary>
        /// <param name="formFile"></param>
        /// <returns></returns>
        public static Stream RotateImage(this IFormFile formFile)
        {
            var stream = formFile.OpenReadStream();
            var img = Image.FromStream(stream);
            var imageFormat = img.RawFormat;

            try
            {
                var propOrientation = img.GetPropertyItem(0x0112);
                if (propOrientation != null)
                {
                    short orientation = BitConverter.ToInt16(propOrientation.Value, 0);
                    if (orientation == 6)
                    {
                        img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    }
                    else if (orientation == 8)
                    {
                        img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    }
                    else
                    {
                        // Do nothing
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return img.ToStream(imageFormat);
        }

        /// <summary>
        /// Rotate image and convert to stream
        /// Fix the issue: image auto rotate if width less than height
        ///
        /// </summary>
        /// <param name="formFile"></param>
        /// <returns></returns>
        public static Stream RotateImage(this Stream inputStream)
        {
            var img = Image.FromStream(inputStream);
            var imageFormat = img.RawFormat;

            try
            {
                var propOrientation = img.GetPropertyItem(0x0112);
                if (propOrientation != null)
                {
                    short orientation = BitConverter.ToInt16(propOrientation.Value, 0);
                    if (orientation == 6)
                    {
                        img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    }
                    else if (orientation == 8)
                    {
                        img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    }
                    else
                    {
                        // Do nothing
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return img.ToStream(imageFormat);
        }

        public static byte[] ToBytes(this Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using MemoryStream ms = new MemoryStream();
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, read);
            }
            return ms.ToArray();
        }

        public static Stream ToStream(this Image image, ImageFormat format)
        {
            var stream = new System.IO.MemoryStream();
            image.Save(stream, format);
            stream.Position = 0;
            return stream;
        }

        public static Image ByteArrayToImage(this byte[] byteArrayIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Write(byteArrayIn, 0, byteArrayIn.Length);
                return Image.FromStream(ms, true);//Exception occurs here
            }
            catch
            {
                return null;
            }
        }

        public static bool IsValidImage(this byte[] bytes)
        {
            try
            {
                using MemoryStream ms = new MemoryStream(bytes);
                Image.FromStream(ms);
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Decrease image size by converting Jpeg
        /// </summary>
        /// <param name="fileStream"></param>
        /// <param name="isLinuxHosting"></param>
        /// <returns></returns>
        public static Stream CompressImage(this Stream fileStream, bool isLinuxHosting = true)
        {
            if (isLinuxHosting)
            {
                // work around for now
                fileStream = fileStream.ResizeImage(1366);
                var optimizer = new ImageOptimizer();
                optimizer.Compress(fileStream);
            }
            else
            {
                // On linux server, this code does not work
                var fileBytes = fileStream.ToBytes();
                if (fileBytes.IsValidImage())
                {
                    var image = fileBytes.ByteArrayToImage();
                    //var imageFormat = image.RawFormat;

                    // If current file is Jpeg then do nothing.
                    //if (imageFormat == ImageFormat.Jpeg.Guid)
                    //{
                    //    return fileStream;
                    //}

                    // Compress image from others to jpeg to decrease size
                    var destRect = new Rectangle(0, 0, image.Width, image.Height);
                    var destImage = new Bitmap(image.Width, image.Height);

                    destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                    using (var graphics = Graphics.FromImage(destImage))
                    {
                        graphics.CompositingMode = CompositingMode.SourceCopy;
                        graphics.CompositingQuality = CompositingQuality.Default;
                        graphics.InterpolationMode = InterpolationMode.Default;
                        graphics.SmoothingMode = SmoothingMode.Default;
                        graphics.PixelOffsetMode = PixelOffsetMode.Default;

                        using var wrapMode = new ImageAttributes();
                        wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                        graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                    }
                    // Reture new image with  Jpeg Format
                    return destImage.ToStream(ImageFormat.Jpeg);
                }
            }

            return fileStream;
        }

        public static Stream ResizeImage(this Stream fileStream, int maxWidth)
        {
            if (maxWidth <= 0)
                return fileStream;

            var fileBytes = fileStream.ToBytes();
            if (fileBytes.IsValidImage())
            {
                var image = fileBytes.ByteArrayToImage();
                if (image != null && image.Width > maxWidth)
                {
                    var imageFormat = image.RawFormat;
                    var currentHight = image.Height;
                    var currentWidth = image.Width;
                    var bitmapResized = image.ResizeImage(maxWidth, maxWidth * currentHight / currentWidth);

                    return bitmapResized.ToStream(ImageFormat.Jpeg);
                }
            }

            return fileStream;
        }

        public static byte[] ResizeImage(this byte[] imageBytes, int maxWidth)
        {
            if (imageBytes.IsValidImage())
            {
                var image = imageBytes.ByteArrayToImage();
                if (image != null && image.Width > maxWidth)
                {
                    var imageFormat = image.RawFormat;
                    var currentHight = image.Height;
                    var currentWidth = image.Width;
                    var bitmapResized = image.ResizeImage(maxWidth, maxWidth * currentHight / currentWidth);

                    return bitmapResized.ToStream(imageFormat).ToBytes();
                }
            }

            return imageBytes;
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(this Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using var wrapMode = new ImageAttributes();
                wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            }

            return destImage;
        }

        public static async Task<byte[]> GetBytes(this IFormFile formFile)
        {
            using var memoryStream = new MemoryStream();
            await formFile.CopyToAsync(memoryStream);
            return memoryStream.ToArray();
        }
    }
}