﻿using System;
using System.Security.Cryptography;

namespace Forum.Common.Helpers
{
    public class RefreshTokenHelper
    {
        public static string GenerateRefreshToken()
        {
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[64];
            rngCryptoServiceProvider.GetBytes(randomBytes);

            return Convert.ToBase64String(randomBytes);
        }
    }
}