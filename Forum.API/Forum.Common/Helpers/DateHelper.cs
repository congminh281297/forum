﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Common.Helpers
{
    public static class DateHelper
    {
        /// <summary>
        /// Lấy ngày giữa 2 ngày
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static List<DateTime> GetAllDatesBetweenDates(DateTime startDate, DateTime endDate, bool hasWeekend = false)
        {
            var result = Enumerable.Range(0, 1 + endDate.Subtract(startDate).Days)
                          .Select(offset => startDate.AddDays(offset))
                          .Where(s => hasWeekend ? true : s.DayOfWeek != DayOfWeek.Saturday && s.DayOfWeek != DayOfWeek.Sunday)
                          .ToList();
            return result;
        }
    }
}