﻿using System.Collections.Generic;

namespace Forum.Common.Helpers
{
    public class RoleHelper
    {
        public static List<int> GetRoles(int value)
        {
            var roles = new int[] { 4, 2, 1 };
            var result = new List<int>();

            foreach (int r in roles)
            {
                if (value == 0)
                    break;

                if (value >= r)
                {
                    result.Add(r);
                    value -= r;
                }
            }

            return result;
        }
    }
}