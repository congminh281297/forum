﻿using System;
using System.Security.Cryptography;

namespace Forum.Common.Helpers
{
    public class ApiKeyHelper
    {
        public static string GenerateApiKey()
        {
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[32];
            rngCryptoServiceProvider.GetBytes(randomBytes);

            return Convert.ToBase64String(randomBytes);
        }
    }
}