﻿using System.Text.Json;

namespace Forum.Common.Helpers
{
    public static class JsonHelper
    {
        public static bool JSONEquals(this object obj, object another)
        {
            if (ReferenceEquals(obj, another)) return true;
            if ((obj == null) || (another == null)) return false;
            if (obj.GetType() != another.GetType()) return false;

            var objJson = JsonSerializer.Serialize(obj);
            var anotherJson = JsonSerializer.Serialize(another);

            return objJson == anotherJson;
        }

        public static T Clone<T>(this T source)
        {
            var objJson = JsonSerializer.Serialize<T>(source);
            return JsonSerializer.Deserialize<T>(objJson);
        }
    }
}