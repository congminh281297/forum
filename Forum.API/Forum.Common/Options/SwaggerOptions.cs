﻿namespace Forum.Common.Options
{
    public class SwaggerOptions
    {
        public string Version { get; set; }
        public string Title { get; set; }
        public string EndpointUrl { get; set; }
        public string EndpointName { get; set; }
    }
}