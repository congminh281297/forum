﻿namespace Forum.Common.Options
{
    public class OpenApiSecurityOptions
    {
        public string Description { get; set; }
        public string Name { get; set; }
        public string Scheme { get; set; }
        public string BearerFormat { get; set; }
    }
}