﻿namespace Forum.Common.Options
{
    public class JwtTokensOptions
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public int ExpireAfterDays { get; set; }
    }
}