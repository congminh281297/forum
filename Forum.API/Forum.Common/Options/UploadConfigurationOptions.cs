﻿namespace Forum.Common.Options
{
    public class UploadConfigurationOptions
    {
        public string ProviderName { get; set; }
        public bool IsCompressImageFile { get; set; }
        public bool IsLinuxHosting { get; set; }
        public EhisQuanLyTrangThietBiUploadOptions EhisHRUploadOptions { get; set; }
    }

    public class EhisQuanLyTrangThietBiUploadOptions
    {
        public string ServerApiDomain { get; set; }
        public string UploadApiUrl { get; set; }
    }
}