﻿namespace Forum.Common.ResponseModels
{
    public class UserResModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public bool IsLocked { get; set; }
    }
}