﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Common.ResponseModels
{
    public class TopicResModel
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string CategoryChildId { get; set; }
        public string LinkOnline { get; set; }
        public bool IsLocked { get; set; }
        public int View { get; set; }
        public int Like { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
