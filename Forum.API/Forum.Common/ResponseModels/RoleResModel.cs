﻿namespace Forum.Common.ResponseModels
{
    public class RoleResModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsLocked { get; set; }
    }
}