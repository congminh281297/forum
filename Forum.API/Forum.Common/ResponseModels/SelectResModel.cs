﻿
using Forum.Common.Enums;
using System;

namespace Forum.Common.ResponseModels
{
    /// <summary>
    /// dùng cho select mặc định
    /// </summary>
    public class SelectResModel
    {
        public string Id { get; set; }
        public string Text { get; set; }
    }
}