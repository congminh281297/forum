﻿namespace Forum.Common.ResponseModels
{
    public class NavigationMenuResModel
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public string Code { get; set; }

        public string Url { get; set; }

        public string Icon { get; set; }

        public string Svg { get; set; }

        public bool Root { get; set; }

        public string Bullet { get; set; }
        public string ParentId { get; set; }

        public bool IsSection { get; set; }

        public int SortOrder { get; set; }
        public bool IsLocked { get; set; }
        public bool IsLockClient { get; set; }
    }
}