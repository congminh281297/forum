﻿namespace Forum.Common.ResponseModels
{
    public class LoginResModel
    {
        public string RefreshToken { get; set; }
        public string Token { get; set; }
    }
}