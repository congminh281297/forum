﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Common.ResponseModels
{
    public class LanguageResModel
    {
        public string Id { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }
        public bool IsLocked { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
