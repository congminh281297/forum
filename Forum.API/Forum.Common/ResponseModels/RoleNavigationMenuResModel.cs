﻿namespace Forum.Common.ResponseModels
{
    public class RoleNavigationMenuResModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ParentId { get; set; }
        public bool IsSection { get; set; }
        public int SortOrder { get; set; }
        public int Value { get; set; }
        public bool HasView { get; set; }
        public bool HasEdit { get; set; }
        public bool HasDelete { get; set; }
    }
}