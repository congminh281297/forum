﻿using Forum.Common.Paged;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Common.RequestModels
{
    public class UserReqModel
    {
        [Required, MaxLength(50)]
        public string UserName { get; set; }

        public string Password { get; set; }

        [Required, MaxLength(250)]
        public string FullName { get; set; }

        [MaxLength(250)]
        public string Image { get; set; }

        public bool IsLocked { get; set; }
        public List<string> Roles { get; set; }
    }

    public class UpdatePasswordReqModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }

    public class GetUserReqModel : PagingRequest
    {
        public string? Keyword { get; set; }
    }
}