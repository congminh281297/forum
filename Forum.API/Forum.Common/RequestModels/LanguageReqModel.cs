﻿using Forum.Common.Paged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Common.RequestModels
{
    public class LanguageReqModel
    {
        public string Slug { get; set; }    
        public string Name { get; set; }
        public bool IsLocked { get; set; }

    }

    public class GetLanguageReqModel : PagingRequest
    {
        public string? Keyword { get; set; }
    }
}
