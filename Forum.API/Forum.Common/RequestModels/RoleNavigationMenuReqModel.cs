﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Common.RequestModels
{
    public class RoleNavigationMenuReqModel
    {
        [Required]
        public string RoleId { get; set; }

        public List<RoleNavigationMenuItemReqModel> RoleNavigationMenus { get; set; }
    }

    public class RoleNavigationMenuItemReqModel
    {
        public string Id { get; set; }
        public bool HasView { get; set; }
        public bool HasEdit { get; set; }
        public bool HasDelete { get; set; }
    }
}