﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Common.RequestModels
{
    public class ChangePasswordReqModel
    {
        [Required]
        public string CurrentPassword { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}