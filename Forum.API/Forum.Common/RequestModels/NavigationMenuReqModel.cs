﻿using Forum.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace Forum.Common.RequestModels
{
    public class NavigationMenuReqModel
    {
        [Required, MaxLength(128)]
        public string Name { get; set; }

        [Required, MaxLength(50)]
        public string Code { get; set; }

        [MaxLength(256)]
        public string Url { get; set; }

        [MaxLength(50)]
        public string Icon { get; set; }

        [MaxLength(50)]
        public string Svg { get; set; }

        public bool Root { get; set; }

        public string Bullet { get; set; }

        [MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string ParentId { get; set; }

        public bool IsSection { get; set; }

        public int SortOrder { get; set; }
        public bool IsLocked { get; set; }
        public bool IsLockClient { get; set; }
    }
}