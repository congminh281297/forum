﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Common.RequestModels
{
    public class TokenReqModel
    {
        [Required]
        public string RefreshToken { get; set; }
    }
}