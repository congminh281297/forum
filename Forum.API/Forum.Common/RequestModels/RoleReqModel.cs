﻿using Forum.Common.Paged;
using System.ComponentModel.DataAnnotations;

namespace Forum.Common.RequestModels
{
    public class RoleReqModel
    {
        [Required, MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(250)]
        public string? Description { get; set; }

        public bool IsLocked { get; set; }
    }

    public class GetRoleReqModel : PagingRequest
    {
        public string? Keyword { get; set; }
    }
}