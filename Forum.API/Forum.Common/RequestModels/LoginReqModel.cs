﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Common.RequestModels
{
    public class LoginReqModel
    {
        [Required, MaxLength(50), MinLength(5)]
        public string UserName { get; set; }

        [Required, MaxLength(50), MinLength(5)]
        public string Password { get; set; }
    }
}