﻿using Forum.Common.Paged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Common.RequestModels
{
    public class TopicReqModel
    {
        public string Url { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string CategoryChildId { get; set; }
        public string LinkOnline { get; set; }
        public bool IsLocked { get; set; }
    }

    public class GetTopicReqModel : PagingRequest
    {
        public string? Keyword { get; set; }
    }
}
