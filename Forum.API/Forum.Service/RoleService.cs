﻿using Forum.Common.Exceptions;
using Forum.Common.RequestModels;
using Forum.Common.ResponseModels;
using Forum.Data.EF;
using Forum.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Forum.Service
{
    public interface IRoleService
    {
        Task<Common.Paged.PagedResult<RoleResModel>> GetAllPaging(GetRoleReqModel req);

        Task<List<RoleResModel>> GetAll();

        Task<RoleResModel> GetById(string id);

        Task<Role> Add(RoleReqModel req);

        Task<Role> Edit(string id, RoleReqModel req);

        Task<Role> Delete(string id);
    }

    public class RoleService : IRoleService
    {
        private readonly IRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public RoleService(IRepository repository,
                           IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<Common.Paged.PagedResult<RoleResModel>> GetAllPaging(GetRoleReqModel req)
        {
            var roleQuery = _repository.GetAll<Role>();
            if (!string.IsNullOrEmpty(req.Keyword))
            {
                roleQuery = roleQuery.Where(x => EF.Functions.Like(x.Name, $"%{req.Keyword}%"));
            }

            if (!string.IsNullOrEmpty(req.Column) && !string.IsNullOrEmpty(req.Direction))
                roleQuery = roleQuery.OrderBy($"{req.Column} {req.Direction}");
            else
                roleQuery = roleQuery.OrderBy(x => x.Id);

            var skip = (req.Page - 1) * req.PageSize;
            int total = await roleQuery.Select(x => x.Id).CountAsync();

            var results = await roleQuery.Select(x => new RoleResModel
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                IsLocked = x.IsLocked
            }).Skip(skip).Take(req.PageSize).ToListAsync();

            var data = new Common.Paged.PagedResult<RoleResModel>
            {
                Results = results,
                Total = total
            };

            return await Task.FromResult(data);
        }

        public async Task<List<RoleResModel>> GetAll()
        {
            return await _repository.GetAll<Role>()
                                    .Select(x => new RoleResModel
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        Description = x.Description,
                                        IsLocked = x.IsLocked
                                    }).ToListAsync();
        }

        public async Task<RoleResModel> GetById(string id)
        {
            var role = await _repository.GetAsync<Role>(id);
            if (role == null)
                throw new NotFoundException("Quyền không tồn tại!");

            var model = new RoleResModel
            {
                Id = role.Id,
                Name = role.Name,
                Description = role.Description,
                IsLocked = role.IsLocked
            };

            return await Task.FromResult(model);
        }

        public async Task<Role> Add(RoleReqModel req)
        {
            if (await _repository.GetAll<Role>(x => x.Name == req.Name).AnyAsync())
                throw new DuplicatedException($"Quyền '{req.Name}' đã tồn tại!");

            var role = await _repository.InsertAsync(new Role
            {
                Name = req.Name,
                Description = req.Description,
                IsLocked = req.IsLocked
            });

            await _unitOfWork.CommitAsync();

            return await Task.FromResult(role);
        }

        public async Task<Role> Edit(string id, RoleReqModel req)
        {
            var role = await _repository.GetAsync<Role>(id);
            if (role == null)
                throw new NotFoundException("Quyền không tồn tại!");

            if (await _repository.GetAll<Role>(x => x.Name == req.Name && x.Id != id).AnyAsync())
                throw new DuplicatedException($"Quyền '{req.Name}' đã tồn tại!");

            role.Name = req.Name;
            role.Description = req.Description;
            role.IsLocked = req.IsLocked;

            _repository.Update(role);
            await _unitOfWork.CommitAsync();

            return await Task.FromResult(role);
        }

        public async Task<Role> Delete(string id)
        {
            var role = await _repository.GetAsync<Role>(id);
            if (role == null)
                throw new NotFoundException("Quyền không tồn tại!");

            _repository.Delete(role);
            await _unitOfWork.CommitAsync();

            return await Task.FromResult(role);
        }
    }
}