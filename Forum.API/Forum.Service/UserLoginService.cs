﻿using Forum.Data.EF;
using Forum.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Service
{
    public interface IUserLoginService
    {
        Task<UserLogin> Add(UserLogin req);

        Task<UserLogin> Edit(UserLogin req);
    }

    public class UserLoginService : IUserLoginService
    {
        private readonly IRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public UserLoginService(IRepository repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<UserLogin> Add(UserLogin req)
        {
            var userLogin = await _repository.InsertAsync(req);
            await _unitOfWork.CommitAsync();

            return await Task.FromResult(userLogin);
        }

        public async Task<UserLogin> Edit(UserLogin req)
        {
            var userLogin = _repository.Update(req);
            await _unitOfWork.CommitAsync();

            return await Task.FromResult(userLogin);
        }
    }
}
