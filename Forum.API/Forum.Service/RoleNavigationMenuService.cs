﻿using Forum.Common.Constants;
using Forum.Common.Enums;
using Forum.Common.Extensions;
using Forum.Common.Helpers;
using Forum.Common.RequestModels;
using Forum.Common.ResponseModels;
using Forum.Data.EF;
using Forum.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Service
{
    public interface IRoleNavigationMenuService
    {
        Task<List<RoleNavigationMenuResModel>> GetByRoleId(string roleId);

        Task Save(RoleNavigationMenuReqModel req);

        Task<bool> CheckPermission(ComparisonType comparisonType, ActionCode action, params FunctionCode[] functions);
    }

    public class RoleNavigationMenuService : IRoleNavigationMenuService
    {
        private readonly IRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RoleNavigationMenuService(IRepository repository,
                                         IUnitOfWork unitOfWork,
                                         IHttpContextAccessor httpContextAccessor)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<List<RoleNavigationMenuResModel>> GetByRoleId(string roleId)
        {
            var data = await (from nm in _repository.GetAll<NavigationMenu>()
                              join rnm in _repository.GetAll<RoleNavigationMenu>(x => x.RoleId == roleId)
                              on nm.Id equals rnm.NavigationMenuId into rnmG
                              from rnm in rnmG.DefaultIfEmpty()
                              select new RoleNavigationMenuResModel
                              {
                                  Id = nm.Id,
                                  Name = nm.Name,
                                  ParentId = nm.ParentId,
                                  IsSection = nm.IsSection,
                                  SortOrder = nm.SortOrder,
                                  Value = rnm == null ? 0 : rnm.Value,
                              }).OrderBy(x => x.SortOrder).ToListAsync();

            foreach (var item in data)
            {
                var roles = RoleHelper.GetRoles(item.Value);
                if (roles.Contains((int)ActionCode.V))
                    item.HasView = true;
                if (roles.Contains((int)ActionCode.E))
                    item.HasEdit = true;
                if (roles.Contains((int)ActionCode.D))
                    item.HasDelete = true;
            }

            return await Task.FromResult(data);
        }

        public async Task Save(RoleNavigationMenuReqModel req)
        {
            var oldRoleNavigationMenus = await _repository.GetAll<RoleNavigationMenu>(x => x.RoleId == req.RoleId)
                                                          .ToListAsync();

            var oldRoleNavigationMenuIds = oldRoleNavigationMenus.Select(x => x.Id).ToList();
            var roleNavigationMenuIds = req.RoleNavigationMenus.Select(x => x.Id).ToList();

            var removeRoleNavigationMenuIds = oldRoleNavigationMenuIds.Except(roleNavigationMenuIds).ToList();
            if (removeRoleNavigationMenuIds.Any())
            {
                var removeRoleNavigationMenus = oldRoleNavigationMenus.Where(x => removeRoleNavigationMenuIds.Contains(x.Id))
                                                                      .ToList();
                foreach (var item in removeRoleNavigationMenus)
                    _repository.Delete(item);
            }

            foreach (var item in req.RoleNavigationMenus)
            {
                int value = 0;
                if (item.HasView)
                    value += (int)ActionCode.V;
                if (item.HasEdit)
                    value += (int)ActionCode.E;
                if (item.HasDelete)
                    value += (int)ActionCode.D;

                var roleNavigationMenu = oldRoleNavigationMenus.FirstOrDefault(x => x.Id == item.Id);
                if (roleNavigationMenu != null)
                {
                    roleNavigationMenu.Value = value;
                    _repository.Update(roleNavigationMenu);
                }
                else
                {
                    roleNavigationMenu = new RoleNavigationMenu
                    {
                        RoleId = req.RoleId,
                        NavigationMenuId = item.Id,
                        Value = value
                    };

                    await _repository.InsertAsync(roleNavigationMenu);
                }
            }

            await _unitOfWork.CommitAsync();
        }

        public async Task<bool> CheckPermission(ComparisonType comparisonType, ActionCode action, params FunctionCode[] functions)
        {
            var userId = IdentityExtensions.GetUserId(_httpContextAccessor);
            var isAdministrator = (from ur in _repository.GetAll<UserRole>()
                                   join r in _repository.GetAll<Role>() on new { roleid = ur.RoleId, userid = ur.UserId } equals new { roleid = r.Id, userid = userId }
                                   where ur.UserId == userId && r.Name == SystemConsts.RoleAdministrator
                                   select ur.Id).Count() > 0;
            if (isAdministrator)
                return true;

            var actions = RoleHelper.GetRoles((int)action);
            var functionCodes = new List<string>();
            foreach (var item in functions)
            {
                functionCodes.Add(item.ToString());
            }

            var roleNavigationMenus = await (from rnm in _repository.GetAll<RoleNavigationMenu>(x => x.NavigationMenu)
                                             join ur in _repository.GetAll<UserRole>()
                                             on rnm.RoleId equals ur.RoleId
                                             where ur.UserId == userId
                                             select new
                                             {
                                                 rnm.Value,
                                                 rnm.NavigationMenu.Code
                                             }).Distinct().ToListAsync();
            switch (comparisonType)
            {
                case ComparisonType.All:
                    {
                        roleNavigationMenus = roleNavigationMenus.Where(x => functionCodes.All(y => y == x.Code)).ToList();
                        break;
                    }
                case ComparisonType.Any:
                    {
                        roleNavigationMenus = roleNavigationMenus.Where(x => functionCodes.Any(y => y == x.Code)).ToList();
                        break;
                    }
                default:
                    {
                        throw new System.ArgumentException("New comparison type need to be included.");
                    }
            }

            var roleNavigationValues = roleNavigationMenus.Select(x => x.Value)
                                                          .Distinct()
                                                          .ToList();
            var roles = roleNavigationValues.SelectMany(x => RoleHelper.GetRoles(x))
                                            .Distinct()
                                            .ToList();

            var hasPermission = (from s in actions
                                 join t in roles on s equals t
                                 select s).Any();

            return await Task.FromResult(hasPermission);
        }
    }
}