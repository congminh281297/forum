﻿using Forum.Common.RequestModels;
using Forum.Common.ResponseModels;
using Forum.Data.EF;
using Forum.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace Forum.Service
{
    public interface ITopicService
    {
        Task<Common.Paged.PagedResult<TopicResModel>> GetAllPaging(GetTopicReqModel model);
        Task<Topic> GetById(string id);
        Task<Topic> Add(TopicReqModel req);
        Task<Topic> Edit(string id, TopicReqModel req);
        Task<Topic> Delete(string id);
    }
    public class TopicService : ITopicService
    {
        private readonly IRepository repository;
        private readonly IUnitOfWork unitOfWork;

        public TopicService(IRepository repository, IUnitOfWork unitOfWork)
        {
            this.repository=repository;
            this.unitOfWork=unitOfWork;
        }

        public async Task<Topic> Add(TopicReqModel req)
        {
            var Topic = await repository.GetAsync<Topic>(s => s.Subject == req.Subject);

            if (Topic is not null)
                throw new BadRequestException($"Chủ đề {req.Subject} đã tồn tại");

            Topic = new Topic()
            {
                Subject = req.Subject,
                Url = req.Url,
                Description = req.Description,
                CategoryChildId = req.CategoryChildId,
                Code = req.Code,
                IsLocked = req.IsLocked,
                LinkOnline = req.LinkOnline,
            };

            repository.Insert(Topic);
            await unitOfWork.CommitAsync();
            return Topic;
        }

        public async Task<Topic> Delete(string id)
        {
            var Topic = await repository.GetAsync<Topic>(s => s.Id == id);

            if (Topic is null)
                throw new BadRequestException($"Chủ đề không tồn tại");
            repository.Delete(Topic);
            await unitOfWork.CommitAsync();

            return Topic;
        }

        public async Task<Topic> Edit(string id, TopicReqModel req)
        {
            var Topic = await repository.GetAsync<Topic>(s => s.Id == id);

            if (Topic is null)
                throw new BadRequestException($"Chủ đề {req.Subject} không tồn tại");

            var exist = await repository.GetAsync<Topic>(s => s.Subject == req.Subject && !s.Id.Equals(id));
            if (exist is not null)
                throw new BadRequestException($"Chủ đề {req.Subject} bị trùng");

            Topic.Subject = req.Subject;
            Topic.Url = req.Url;
            Topic.Description = req.Description;
            Topic.Code = req.Code;
            Topic.LinkOnline = req.LinkOnline;
            Topic.CategoryChildId = req.CategoryChildId;
            Topic.IsLocked = req.IsLocked;

            repository.Update(Topic);
            await unitOfWork.CommitAsync();

            return Topic;
        }

        public async Task<Common.Paged.PagedResult<TopicResModel>> GetAllPaging(GetTopicReqModel model)
        {
            var Topics = repository.GetAll<Topic>();

            if (!string.IsNullOrEmpty(model.Keyword))
                Topics = Topics.Where(s => EF.Functions.Like(s.Subject, $"%{model.Keyword}%"));

            if (!string.IsNullOrEmpty(model.Column) && !string.IsNullOrEmpty(model.Direction))
                Topics = Topics.OrderBy($"{model.Column} {model.Direction}");
            else
                Topics = Topics.OrderByDescending(s => s.DateCreated);

            int skip = model.Page * model.PageSize;
            int total = await Topics.CountAsync();

            var result = await Topics.
                Select(s => new TopicResModel
                {
                    Subject = s.Subject,
                    Url = s.Url,
                    Description = s.Description,
                    CategoryChildId = s.CategoryChildId,
                    Code = s.Code,
                    IsLocked = s.IsLocked,
                    LinkOnline = s.LinkOnline,
                }).
                Skip(skip - model.PageSize).Take(model.PageSize).ToListAsync();

            var data = new Common.Paged.PagedResult<TopicResModel>
            {
                Total = total,
                Results = result,
            };

            return data;
        }

        public async Task<Topic> GetById(string id)
        {
            var Topic = await repository.GetAsync<Topic>(id);

            if (Topic is null)
                throw new BadRequestException($"Chủ đề {Topic?.Subject} không tồn tại");

            return Topic;
        }
    }
}
