﻿using Forum.Common.Exceptions;
using Forum.Common.Helpers;
using Forum.Common.RequestModels;
using Forum.Common.ResponseModels;
using Forum.Data.EF;
using Forum.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Forum.Service
{
    public interface IUserService
    {
        Task<User> GetByUserName(string userName);

        Task<User> GetByRefreshToken(string refreshToken);

        Task ChangePassword(string userId, ChangePasswordReqModel req);

        Task<Common.Paged.PagedResult<UserResModel>> GetAllPaging(GetUserReqModel req);

        Task<UserReqModel> GetById(string id);

        Task Add(UserReqModel req);

        Task Edit(string id, UserReqModel req);

        Task Delete(string id);

        Task UpdatePassword(UpdatePasswordReqModel req);

        Task<List<SelectResModel>> GetSelect();
    }

    public class UserService : IUserService
    {
        private readonly IRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IRepository repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<User> GetByUserName(string userName)
        {
            var user = await _repository.GetAsync<User>(x => x.UserName == userName, c => c.UserLogin);
            return await Task.FromResult(user);
        }

        public async Task<User> GetByRefreshToken(string refreshToken)
        {
            var user = await _repository.GetAsync<User>(x => x.UserLogin.RefreshToken == refreshToken, c => c.UserLogin);
            return await Task.FromResult(user);
        }

        public async Task ChangePassword(string userId, ChangePasswordReqModel req)
        {
            var user = await _repository.GetAsync<User>(x => x.Id == userId);

            if (!PasswordHelper.VerifyHashedPassword(user.Password, req.CurrentPassword))
                throw new BadRequestException("Mật khẩu hiện tại không đúng!");

            user.Password = PasswordHelper.HashPassword(req.Password);
            _repository.Update(user);
            await _unitOfWork.CommitAsync();
        }

        public async Task<Common.Paged.PagedResult<UserResModel>> GetAllPaging(GetUserReqModel req)
        {
            var userQuery = _repository.GetAll<User>();
            if (!string.IsNullOrEmpty(req.Keyword))
            {
                userQuery = userQuery.Where(x => EF.Functions.Like(x.FullName, $"%{req.Keyword}%") ||
                                                 EF.Functions.Like(x.UserName, $"%{req.Keyword}%"));
            }

            if (!string.IsNullOrEmpty(req.Column) && !string.IsNullOrEmpty(req.Direction))
                userQuery = userQuery.OrderBy($"{req.Column} {req.Direction}");
            else
                userQuery = userQuery.OrderBy(x => x.Id);

            var skip = (req.Page - 1) * req.PageSize;
            int total = await userQuery.Select(x => x.Id).CountAsync();

            var results = await userQuery.Select(x => new UserResModel
            {
                Id = x.Id,
                FullName = x.FullName,
                UserName = x.UserName,
                IsLocked = x.IsLocked
            }).Skip(skip).Take(req.PageSize).ToListAsync();

            var data = new Common.Paged.PagedResult<UserResModel>
            {
                Results = results,
                Total = total
            };

            return await Task.FromResult(data);
        }

        public async Task<UserReqModel> GetById(string id)
        {
            var user = await _repository.GetAsync<User>(id, c => c.UserRoles);
            if (user == null)
                throw new NotFoundException("Người dùng không tồn tại!");

            var model = new UserReqModel
            {
                FullName = user.FullName,
                UserName = user.UserName,
                Image = user.Image,
                IsLocked = user.IsLocked,
                Roles = user.UserRoles.Select(x => x.RoleId).ToList(),
            };

            return await Task.FromResult(model);
        }

        public async Task Add(UserReqModel req)
        {
            if (await _repository.GetAll<User>(x => x.UserName == req.UserName).AnyAsync())
                throw new DuplicatedException($"Người dùng '{req.UserName}' đã tồn tại!");

            var user = await _repository.InsertAsync(new User
            {
                FullName = req.FullName,
                UserName = req.UserName,
                Password = PasswordHelper.HashPassword(req.Password),
                Image = req.Image,
                IsLocked = req.IsLocked
            });

            if (req.Roles != null && req.Roles.Any())
            {
                var userRoles = req.Roles.Select(x => new UserRole
                {
                    RoleId = x,
                }).ToList();

                user.UserRoles = userRoles;
            }

            await _unitOfWork.CommitAsync();
        }

        public async Task Edit(string id, UserReqModel req)
        {
            var user = await _repository.GetAsync<User>(id, c => c.UserRoles);
            if (user == null)
                throw new NotFoundException("Người dùng không tồn tại!");

            if (await _repository.GetAll<User>(x => x.UserName == req.UserName && x.Id != id).AnyAsync())
                throw new DuplicatedException($"Người dùng '{req.UserName}' đã tồn tại!");

            user.FullName = req.FullName;
            user.UserName = req.UserName;
            user.Image = req.Image;
            user.IsLocked = req.IsLocked;

            if (req.Roles != null && req.Roles.Any())
            {
                var userRoles = req.Roles.Select(x => new UserRole
                {
                    RoleId = x,
                }).ToList();

                user.UserRoles = userRoles;
            }
            else
            {
                user.UserRoles = null;
            }

            _repository.Update(user);
            await _unitOfWork.CommitAsync();
        }

        public async Task Delete(string id)
        {
            var user = await _repository.GetAsync<User>(id);
            if (user == null)
                throw new NotFoundException("Người dùng không tồn tại!");

            _repository.Delete(user);
            await _unitOfWork.CommitAsync();
        }

        public async Task UpdatePassword(UpdatePasswordReqModel req)
        {
            var user = await _repository.GetAsync<User>(req.UserId);
            if (user == null)
                throw new NotFoundException("Người dùng không tồn tại!");

            user.Password = PasswordHelper.HashPassword(req.Password);

            _repository.Update(user);
            await _unitOfWork.CommitAsync();
        }

        public async Task<List<SelectResModel>> GetSelect()
        {
            return await _repository.GetAll<User>(s => s.UserName != "Admin").Select(s => new SelectResModel()
            {
                Id = s.Id,
                Text = s.FullName
            }).ToListAsync();
          
        }
    }
}