﻿using Forum.Common.Constants;
using Forum.Common.Exceptions;
using Forum.Common.RequestModels;
using Forum.Common.ResponseModels;
using Forum.Data.EF;
using Forum.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Service
{
    public interface INavigationMenuService
    {
        Task<List<NavigationMenuResModel>> GetAll();

        Task<List<NavigationMenuResModel>> GetByUserId(string userId);

        Task<NavigationMenuResModel> GetById(string id);

        Task<NavigationMenu> Add(NavigationMenuReqModel req);

        Task<NavigationMenu> Edit(string id, NavigationMenuReqModel req);

        Task MoveUp(string id);

        Task MoveDown(string id);
    }

    public class NavigationMenuService : INavigationMenuService
    {
        private readonly IRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public NavigationMenuService(IRepository repository,
                                     IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<List<NavigationMenuResModel>> GetAll()
        {
            var data = await (from nm in _repository.GetAll<NavigationMenu>()
                              select new NavigationMenuResModel
                              {
                                  Id = nm.Id,
                                  Name = nm.Name,
                                  Code = nm.Code,
                                  Url = nm.Url,
                                  Icon = nm.Icon,
                                  Svg = nm.Svg,
                                  Root = nm.Root,
                                  Bullet = nm.Bullet,
                                  ParentId = nm.ParentId,
                                  IsSection = nm.IsSection,
                                  SortOrder = nm.SortOrder,
                              }).OrderBy(x => x.SortOrder).ToListAsync();

            return await Task.FromResult(data);
        }

        public async Task<List<NavigationMenuResModel>> GetByUserId(string userId)
        {
            var isAdministrator = (from ur in _repository.GetAll<UserRole>()
                                   join r in _repository.GetAll<Role>() on new { roleid = ur.RoleId, userid = ur.UserId } equals new { roleid = r.Id, userid = userId }
                                   where r.Name == SystemConsts.RoleAdministrator && ur.UserId == userId
                                   select ur.Id).Any();

            IQueryable<NavigationMenu> query;
            if (isAdministrator)
                query = _repository.GetAll<NavigationMenu>(s => !s.IsLockClient && !s.IsLocked);
            else
                query = from nm in _repository.GetAll<NavigationMenu>()
                        join rnm in _repository.GetAll<RoleNavigationMenu>()
                        on nm.Id equals rnm.NavigationMenuId
                        join ur in _repository.GetAll<UserRole>()
                        on rnm.RoleId equals ur.RoleId
                        where !nm.IsLockClient && !nm.IsLocked && (ur.UserId == userId || nm.Code == null || nm.Url == null || nm.ParentId == null)
                        select nm;

            var data = query.Select(s => new NavigationMenuResModel
            {
                Id = s.Id,
                Name = s.Name,
                Code = s.Code,
                Url = s.Url,
                Icon = s.Icon,
                Svg = s.Svg,
                Root = s.Root,
                Bullet = s.Bullet,
                ParentId = s.ParentId,
                IsSection = s.IsSection,
                SortOrder = s.SortOrder,
            }).GroupBy(s => s).Select(s => s.Key).OrderBy(x => x.SortOrder);

            return await data.ToListAsync();
        }

        public async Task<NavigationMenuResModel> GetById(string id)
        {
            var nm = await _repository.GetAsync<NavigationMenu>(id);
            if (nm == null)
                throw new NotFoundException("Trang không tồn tại!");

            var model = new NavigationMenuResModel
            {
                Id = nm.Id,
                Name = nm.Name,
                Code = nm.Code,
                Url = nm.Url,
                Icon = nm.Icon,
                Svg = nm.Svg,
                Root = nm.Root,
                Bullet = nm.Bullet,
                ParentId = nm.ParentId,
                IsSection = nm.IsSection,
                SortOrder = nm.SortOrder,
                IsLocked = nm.IsLocked,
                IsLockClient = nm.IsLockClient,
            };

            return await Task.FromResult(model);
        }

        public async Task<NavigationMenu> Add(NavigationMenuReqModel req)
        {
            var nm = await _repository.InsertAsync(new NavigationMenu
            {
                Name = req.Name,
                Code = req.Code,
                Url = req.Url,
                Icon = req.Icon,
                Svg = req.Svg,
                Root = req.Root,
                Bullet = req.Bullet,
                ParentId = req.ParentId,
                IsSection = req.IsSection,
                SortOrder = req.SortOrder,
                IsLocked = req.IsLocked,
                IsLockClient = req.IsLockClient,
            });

            await _unitOfWork.CommitAsync();

            return await Task.FromResult(nm);
        }

        public async Task<NavigationMenu> Edit(string id, NavigationMenuReqModel req)
        {
            var nm = await _repository.GetAsync<NavigationMenu>(id);
            if (nm == null)
                throw new NotFoundException("Trang không tồn tại!");

            nm.Name = req.Name;
            nm.Code = req.Code;
            nm.Url = req.Url;
            nm.Icon = req.Icon;
            nm.Svg = req.Svg;
            nm.Root = req.Root;
            nm.Bullet = req.Bullet;
            nm.ParentId = req.ParentId;
            nm.IsSection = req.IsSection;
            nm.SortOrder = req.SortOrder;
            nm.IsLocked = req.IsLocked;
            nm.IsLockClient = req.IsLockClient;

            _repository.Update(nm);
            await _unitOfWork.CommitAsync();

            return await Task.FromResult(nm);
        }

        public async Task MoveUp(string id)
        {
            var item = await _repository.GetAsync<NavigationMenu>(id);
            if (item == null)
                throw new NotFoundException("Trang không tồn tại!");

            var moveItem = await _repository.GetAll<NavigationMenu>(s => s.SortOrder < item.SortOrder && s.ParentId == item.ParentId)
                                            .OrderByDescending(s => s.SortOrder)
                                            .FirstOrDefaultAsync();
            if (moveItem != null)
            {
                int temp = item.SortOrder;
                item.SortOrder = moveItem.SortOrder;
                moveItem.SortOrder = temp;

                _repository.Update(new List<NavigationMenu> { item, moveItem });
                await _unitOfWork.CommitAsync();
            }
        }

        public async Task MoveDown(string id)
        {
            var item = await _repository.GetAsync<NavigationMenu>(id);
            if (item == null)
                throw new NotFoundException("Trang không tồn tại!");

            var moveItem = await _repository.GetAll<NavigationMenu>(s => s.SortOrder > item.SortOrder && s.ParentId == item.ParentId)
                                            .OrderBy(s => s.SortOrder)
                                            .FirstOrDefaultAsync();

            if (moveItem != null)
            {
                int temp = item.SortOrder;
                item.SortOrder = moveItem.SortOrder;
                moveItem.SortOrder = temp;

                _repository.Update(new List<NavigationMenu> { item, moveItem });
                await _unitOfWork.CommitAsync();
            }
        }
    }
}