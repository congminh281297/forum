﻿using Forum.Common.RequestModels;
using Forum.Common.ResponseModels;
using Forum.Data.EF;
using Forum.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace Forum.Service
{
    public interface ILanguageService
    {
        Task<Common.Paged.PagedResult<LanguageResModel>> GetAllPaging(GetLanguageReqModel model);
        Task<Language> GetById(string id);
        Task<Language> Add(LanguageReqModel req);
        Task<Language> Edit(string id, LanguageReqModel req);
        Task<Language> Delete(string id);
    }
    public class LanguageService : ILanguageService
    {
        private readonly IRepository repository;
        private readonly IUnitOfWork unitOfWork;

        public LanguageService(IRepository repository, IUnitOfWork unitOfWork)
        {
            this.repository=repository;
            this.unitOfWork=unitOfWork;
        }

        public async Task<Language> Add(LanguageReqModel req)
        {
            var language = await repository.GetAsync<Language>(s => s.Name == req.Name);

            if (language is not null)
                throw new BadRequestException($"Ngôn ngữ {req.Name} đã tồn tại");

            language = new Language() { 
                Slug = req.Slug,
                Name = req.Name,
                IsLocked = req.IsLocked,
            };

            repository.Insert(language);
            await unitOfWork.CommitAsync();
            return language;
        }

        public async Task<Language> Delete(string id)
        {
            var language = await repository.GetAsync<Language>(s => s.Id == id);

            if (language is null)
                throw new BadRequestException($"Ngôn ngữ không tồn tại");
            repository.Delete(language);
            await unitOfWork.CommitAsync();

            return language;
        }

        public async Task<Language> Edit(string id, LanguageReqModel req)
        {
            var language = await repository.GetAsync<Language>(s => s.Id == id);

            if (language is null)
                throw new BadRequestException($"Ngôn ngữ {req.Name} không tồn tại");

            var exist = await repository.GetAsync<Language>(s => s.Name == req.Name && !s.Id.Equals(id));
            if (exist is not null)
                throw new BadRequestException($"Ngôn ngữ {req.Name} bị trùng");

            language.Slug = req.Slug;
            language.Name = req.Name;
            language.IsLocked = req.IsLocked;
            repository.Update(language);
            await unitOfWork.CommitAsync();

            return language;
        }

        public async Task<Common.Paged.PagedResult<LanguageResModel>> GetAllPaging(GetLanguageReqModel model)
        {
            var languages = repository.GetAll<Language>();

            if (!string.IsNullOrEmpty(model.Keyword))
                languages = languages.Where(s => EF.Functions.Like(s.Name, $"%{model.Keyword}%"));

            if (!string.IsNullOrEmpty(model.Column) && !string.IsNullOrEmpty(model.Direction))
                languages = languages.OrderBy($"{model.Column} {model.Direction}");
            else
                languages = languages.OrderByDescending(s => s.DateCreated);

            int skip = model.Page * model.PageSize;
            int total = await languages.CountAsync();

            var result = await languages.
                Select(s => new LanguageResModel
                {
                    Id = s.Id,
                    Slug = s.Slug,
                    Name = s.Name,
                    DateCreated = s.DateCreated,
                    IsLocked = s.IsLocked
                }).
                Skip(skip - model.PageSize).Take(model.PageSize).ToListAsync();

            var data = new Common.Paged.PagedResult<LanguageResModel>
            {
                Total = total,
                Results = result,
            };

            return data;
        }

        public async Task<Language> GetById(string id)
        {
            var language = await repository.GetAsync<Language>(id);

            if (language is null)
                throw new BadRequestException($"Ngôn ngữ {language?.Name} không tồn tại");

            return language;
        }
    }
}
