﻿using Forum.API.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace Forum.API.Extensions
{
    public static class ExceptionExtensions
    {
        public static void UseExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }
}