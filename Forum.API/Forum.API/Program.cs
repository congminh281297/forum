using Forum.API.Extensions;
using Forum.Common.BackgroundTasks;
using Forum.Common.Constants;
using Forum.Common.Logger;
using Forum.Common.Options;
using Forum.Data.EF;
using Forum.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using NetCore.AutoRegisterDi;
using System.Configuration;
using System.Reflection;
using System.Text;

var builder = WebApplication.CreateBuilder(args);
IConfiguration configuration = builder.Configuration;
// Add builder.Services to the container.
builder.Services.AddDbContext<ForumContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString(ConnectionStringConsts.DefaultConnection),
                                        b => b.MigrationsAssembly(SystemConsts.MigrationsAssembly)));

builder.Services.AddDbContextPool<AuditLogDbContext>(options =>
    options.UseSqlServer(configuration.GetConnectionString(ConnectionStringConsts.AuditLogConnection),
                            b => b.MigrationsAssembly(SystemConsts.MigrationsAssembly)));

builder.Services.AddTransient<DbInitializer>();
builder.Services.AddTransient(typeof(IUnitOfWork), typeof(UnitOfWork));
builder.Services.AddScoped(typeof(IRepository), typeof(Repository));
builder.Services.AddSingleton<ILoggerManager, LoggerManager>();
builder.Services.AddHostedService<QueuedHostedService>();
builder.Services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

// DI
var assembliesToScan = new[]
{
                Assembly.GetAssembly(typeof(IUserService))
            };

builder.Services.RegisterAssemblyPublicNonGenericClasses(assembliesToScan)
        .Where(c => c.Name.EndsWith("Service"))
        .AsPublicImplementedInterfaces();

builder.Services.Configure<JwtTokensOptions>(configuration.GetSection(SystemConsts.JwtTokens));
builder.Services.Configure<SwaggerOptions>(configuration.GetSection(SystemConsts.Swagger));
builder.Services.Configure<OpenApiSecurityOptions>(configuration.GetSection(SystemConsts.OpenApiSecurity));
builder.Services.Configure<UploadConfigurationOptions>(configuration.GetSection(SystemConsts.UploadConfiguration));

var jwtTokensOption = configuration.GetSection(SystemConsts.JwtTokens);

builder.Services.AddAuthentication(o =>
{
    o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(cfg =>
{
    cfg.RequireHttpsMetadata = false;
    cfg.SaveToken = true;
    cfg.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidIssuer = jwtTokensOption.GetValue<string>(nameof(JwtTokensOptions.Issuer)),
        ValidAudience = jwtTokensOption.GetValue<string>(nameof(JwtTokensOptions.Audience)),
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SystemConsts.SigningKey))
    };
});
builder.Services.AddCors(o => o.AddPolicy(SystemConsts.CorsPolicy, builder =>
{
    builder.AllowAnyOrigin();
    builder.AllowAnyHeader();
    builder.AllowAnyMethod();
}));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();
using var scope = app.Services.CreateScope();
var services = scope.ServiceProvider;
try
{
    var context = services.GetRequiredService<ForumContext>();
    var dbInitializer = services.GetService<DbInitializer>();
    dbInitializer?.Seed().Wait();
}
catch (Exception ex)
{
    var logger = services.GetService<ILoggerManager>();
    logger?.LogError(ex, "An error occurred creating the DB.");
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseDeveloperExceptionPage();
app.UseExceptionMiddleware();
app.UseHttpsRedirection();
app.UseRouting();
app.UseCors(SystemConsts.CorsPolicy);
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
