﻿using Forum.API.Filters;
using Forum.Common.Enums;
using Forum.Common.Extensions;
using Forum.Common.RequestModels;
using Forum.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Forum.API.Controllers
{
    public class NavigationMenuController : BaseController
    {
        private readonly INavigationMenuService _navigationMenuService;
        public NavigationMenuController(INavigationMenuService navigationMenuService)
        {
            _navigationMenuService = navigationMenuService;
        }

        [ClaimRequirement(FunctionCode.NAVIGATION, ActionCode.V)]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _navigationMenuService.GetAll();
            return Ok(data);
        }


        [HttpGet("getByCurrentUser")]
        public async Task<IActionResult> GetByCurrentUser()
        {
            var data = await _navigationMenuService.GetByUserId(User.GetUserId());
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.NAVIGATION, ActionCode.V)]
        [HttpGet("{id}")]
        [ValidateModel]
        public async Task<IActionResult> Get(string id)
        {
            var data = await _navigationMenuService.GetById(id);
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.NAVIGATION, ActionCode.E)]
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Add([FromBody] NavigationMenuReqModel req)
        {
            var data = await _navigationMenuService.Add(req);
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.NAVIGATION, ActionCode.E)]
        [HttpPut("{id}")]
        [ValidateModel]
        public async Task<IActionResult> Edit(string id, [FromBody] NavigationMenuReqModel req)
        {
            var data = await _navigationMenuService.Edit(id, req);
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.NAVIGATION, ActionCode.E)]
        [HttpPut("moveUp/{id}")]
        public async Task<IActionResult> MoveUp(string id)
        {
            await _navigationMenuService.MoveUp(id);
            return Ok();
        }

        [ClaimRequirement(FunctionCode.NAVIGATION, ActionCode.E)]
        [HttpPut("moveDown/{id}")]
        public async Task<IActionResult> MoveDown(string id)
        {
            await _navigationMenuService.MoveDown(id);
            return Ok();
        }
    }
}
