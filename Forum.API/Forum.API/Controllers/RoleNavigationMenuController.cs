﻿using Forum.API.Filters;
using Forum.Common.Enums;
using Forum.Common.RequestModels;
using Forum.Service;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Forum.API.Controllers
{
    public class RoleNavigationMenuController : BaseController
    {
        private readonly IRoleNavigationMenuService _roleNavigationMenuService;

        public RoleNavigationMenuController(IRoleNavigationMenuService roleNavigationMenuService)
        {
            _roleNavigationMenuService = roleNavigationMenuService;
        }

        [ClaimRequirement(FunctionCode.ROLE_NAVIGATION, ActionCode.V)]
        [HttpGet("getByRoleId")]
        public async Task<IActionResult> GetByRoleId([FromQuery] string roleId)
        {
            var data = await _roleNavigationMenuService.GetByRoleId(roleId);
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.ROLE_NAVIGATION, ActionCode.E)]
        [HttpPost]
        public async Task<IActionResult> Save([FromBody] RoleNavigationMenuReqModel req)
        {
            await _roleNavigationMenuService.Save(req);
            return Ok();
        }
    }
}