﻿using Forum.API.Filters;
using Forum.Common.Enums;
using Forum.Common.RequestModels;
using Forum.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Forum.API.Controllers
{
    public class TopicController : BaseController
    {
        private readonly ITopicService TopicService;

        public TopicController(ITopicService TopicService)
        {
            this.TopicService=TopicService;
        }

        [AllowAnonymous]
        [HttpGet("GetAllPaging")]
        public async Task<IActionResult> GetAllPaging([FromQuery] GetTopicReqModel model)
        {
            var data = await TopicService.GetAllPaging(model);
            return Ok(data);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(string id)
        {
            var data = await TopicService.GetById(id);
            return Ok(data);
        }

        [HttpPost]
        [ValidateModel]
        [ClaimRequirement(FunctionCode.TOPIC, ActionCode.E)]
        public async Task<IActionResult> Add([FromBody] TopicReqModel req)
        {
            var data = await TopicService.Add(req);
            return Ok(data);
        }

        [HttpPut("{id}")]
        [ValidateModel]
        [ClaimRequirement(FunctionCode.TOPIC, ActionCode.E)]
        public async Task<IActionResult> Edit(string id, [FromBody] TopicReqModel req)
        {
            var data = await TopicService.Edit(id, req);
            return Ok(data);
        }

        [HttpDelete]
        [ClaimRequirement(FunctionCode.TOPIC, ActionCode.D)]
        public async Task<IActionResult> Delete(string id)
        {
            var data = await TopicService.Delete(id);
            return Ok(data);
        }


    }
}
