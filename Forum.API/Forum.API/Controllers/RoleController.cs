﻿using Forum.API.Filters;
using Forum.Common.Enums;
using Forum.Common.RequestModels;
using Forum.Service;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Forum.API.Controllers
{
    public class RoleController : BaseController
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [ClaimRequirement(FunctionCode.ROLE, ActionCode.V)]
        [HttpGet("getAllPaging")]
        public async Task<IActionResult> GetAllPaging([FromQuery] GetRoleReqModel req)
        {
            var data = await _roleService.GetAllPaging(req);
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.ROLE, ActionCode.V)]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _roleService.GetAll();
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.ROLE, ActionCode.V)]
        [HttpGet("{id}")]
        [ValidateModel]
        public async Task<IActionResult> Get(string id)
        {
            var data = await _roleService.GetById(id);
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.ROLE, ActionCode.E)]
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Add([FromBody] RoleReqModel req)
        {
            var data = await _roleService.Add(req);
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.ROLE, ActionCode.E)]
        [HttpPut("{id}")]
        [ValidateModel]
        public async Task<IActionResult> Edit(string id, [FromBody] RoleReqModel req)
        {
            var data = await _roleService.Edit(id, req);
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.ROLE, ActionCode.D)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var data = await _roleService.Delete(id);
            return Ok(data);
        }
    }
}