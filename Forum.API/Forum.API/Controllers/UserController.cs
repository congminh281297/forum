﻿using Forum.API.Filters;
using Forum.Common.Enums;
using Forum.Common.Extensions;
using Forum.Common.RequestModels;
using Forum.Service;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Forum.API.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("changePassword")]
        [ValidateModel]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordReqModel req)
        {
            await _userService.ChangePassword(User.GetUserId(), req);
            return Ok();
        }

        [ClaimRequirement(FunctionCode.USER, ActionCode.V)]
        [HttpGet("getAllPaging")]
        public async Task<IActionResult> GetAllPaging([FromQuery] GetUserReqModel req)
        {
            var data = await _userService.GetAllPaging(req);
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.USER, ActionCode.V)]
        [HttpGet("{id}")]
        [ValidateModel]
        public async Task<IActionResult> Get(string id)
        {
            var data = await _userService.GetById(id);
            return Ok(data);
        }

        [ClaimRequirement(FunctionCode.USER, ActionCode.E)]
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Add([FromBody] UserReqModel req)
        {
            await _userService.Add(req);
            return Ok();
        }

        [ClaimRequirement(FunctionCode.USER, ActionCode.E)]
        [HttpPut("{id}")]
        [ValidateModel]
        public async Task<IActionResult> Edit(string id, [FromBody] UserReqModel req)
        {
            await _userService.Edit(id, req);
            return Ok();
        }

        [ClaimRequirement(FunctionCode.USER, ActionCode.D)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            await _userService.Delete(id);
            return Ok();
        }

        [HttpPost("updatePassword")]
        [ValidateModel]
        public async Task<IActionResult> UpdatePassword([FromBody] UpdatePasswordReqModel req)
        {
            await _userService.UpdatePassword(req);
            return Ok();
        }


        [HttpGet("getSelect")]
        public async Task<IActionResult> GetSelect()
        {
            var data = await _userService.GetSelect();
            return Ok(data);
        }
    }
}