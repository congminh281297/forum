﻿using Forum.API.Filters;
using Forum.Common.Enums;
using Forum.Common.RequestModels;
using Forum.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Forum.API.Controllers
{
    public class LanguageController : BaseController
    {
        private readonly ILanguageService languageService;

        public LanguageController(ILanguageService languageService)
        {
            this.languageService=languageService;
        }

        [AllowAnonymous]
        [HttpGet("GetAllPaging")]
        public async Task<IActionResult> GetAllPaging([FromQuery] GetLanguageReqModel model)
        {
            var data = await languageService.GetAllPaging(model);
            return Ok(data);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(string id)
        {
            var data = await languageService.GetById(id);
            return Ok(data);
        }

        [HttpPost]
        [ValidateModel]
        [ClaimRequirement(FunctionCode.LANGUAGE, ActionCode.E)]
        public async Task<IActionResult> Add([FromBody] LanguageReqModel req)
        {
            var data = await languageService.Add(req);
            return Ok(data);
        }

        [HttpPut("{id}")]
        [ValidateModel]
        [ClaimRequirement(FunctionCode.LANGUAGE, ActionCode.E)]
        public async Task<IActionResult> Edit(string id, [FromBody] LanguageReqModel req)
        {
            var data = await languageService.Edit(id, req);
            return Ok(data);
        }

        [HttpDelete]
        [ClaimRequirement(FunctionCode.LANGUAGE, ActionCode.D)]
        public async Task<IActionResult> Delete(string id)
        {
            var data = await languageService.Delete(id);
            return Ok(data);
        }


    }
}
