﻿using Forum.API.Filters;
using Forum.Common.Constants;
using Forum.Common.Exceptions;
using Forum.Common.Helpers;
using Forum.Common.Options;
using Forum.Common.RequestModels;
using Forum.Common.ResponseModels;
using Forum.Data.Models;
using Forum.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Forum.API.Controllers
{
    [AllowAnonymous]
    public class AuthController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IUserLoginService _userLoginService;
        private readonly JwtTokensOptions _jwtTokensOptions;

        public AuthController(IUserService userService,
                              IUserLoginService userLoginService,
                              IOptions<JwtTokensOptions> jwtTokensOptions)
        {
            _userService = userService;
            _userLoginService = userLoginService;
            _jwtTokensOptions = jwtTokensOptions.Value;
        }

        [HttpPost]
        [ValidateModel] 
        public async Task<IActionResult> Authenticate([FromBody] LoginReqModel req)
        {
            var user = await _userService.GetByUserName(req.UserName);
            if (user == null)
                throw new BadRequestException("Tên đăng nhập không tồn tại!");

            if (!PasswordHelper.VerifyHashedPassword(user.Password, req.Password))
                throw new BadRequestException("Mật khẩu không đúng!");

            if (user.IsLocked)
                throw new BadRequestException("Tài khoản đang bị khóa!");

            var res = await GenerateToken(user);
            return Ok(res);
        }

        [HttpPost("refreshToken")]
        [ValidateModel]
        public async Task<IActionResult> RefreshToken([FromBody] TokenReqModel req)
        {
            var user = await _userService.GetByRefreshToken(req.RefreshToken);
            if (user == null)
                return Unauthorized();

            if (user.IsLocked)
                throw new BadRequestException("Tài khoản đang bị khóa!");

            var res = await GenerateToken(user);
            return Ok(res);
        }

        private async Task<LoginResModel> GenerateToken(User user)
        {
            var claims = new[]
               {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimConsts.UserId, user.Id),
                    new Claim(ClaimConsts.UserName, user.UserName),
                    new Claim(ClaimConsts.FullName, user.FullName),
                    new Claim(ClaimConsts.Image,  user.Image ?? string.Empty)
                };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SystemConsts.SigningKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var now = DateTime.Now;
            var expires = now.AddDays(_jwtTokensOptions.ExpireAfterDays);

            var securityToken = new JwtSecurityToken(_jwtTokensOptions.Issuer,
                                                     _jwtTokensOptions.Audience,
                                                     claims,
                                                     expires: expires,
                                                     signingCredentials: creds);

            var token = new JwtSecurityTokenHandler().WriteToken(securityToken);
            var refreshToken = RefreshTokenHelper.GenerateRefreshToken();

            var userLogin = user.UserLogin;
            if (userLogin == null)
            {
                userLogin = new UserLogin
                {
                    ProviderKey = Guid.NewGuid().ToString(),
                    LoginTime = now,
                    ExpiresTime = expires,
                    RefreshToken = refreshToken,
                    Id = user.Id,
                };

                await _userLoginService.Add(userLogin);
            }
            else
            {
                userLogin.ProviderKey = Guid.NewGuid().ToString();
                userLogin.LoginTime = now;
                userLogin.ExpiresTime = expires;
                userLogin.RefreshToken = refreshToken;

                await _userLoginService.Edit(userLogin);
            }

            var res = new LoginResModel
            {
                Token = token,
                RefreshToken = refreshToken
            };

            return await Task.FromResult(res);
        }
    }
}