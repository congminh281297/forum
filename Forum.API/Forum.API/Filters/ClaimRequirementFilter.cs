﻿using Forum.Common.Enums;
using Forum.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace Forum.API.Filters
{
    public class ClaimRequirementFilter : IAsyncAuthorizationFilter
    {
        private readonly IRoleNavigationMenuService _roleNavigationMenuService;
        private readonly ComparisonType _comparisonType;
        private readonly ActionCode _action;
        private readonly FunctionCode[] _functions;

        public ClaimRequirementFilter(IRoleNavigationMenuService roleNavigationMenuService,
                                      ComparisonType comparisonType,
                                      ActionCode action,
                                      params FunctionCode[] functions)
        {
            _roleNavigationMenuService = roleNavigationMenuService;
            _comparisonType = comparisonType;
            _action = action;
            _functions = functions;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                var hasPermission = await _roleNavigationMenuService.CheckPermission(_comparisonType, _action, _functions);
                if (!hasPermission)
                {
                    context.Result = new ForbidResult();
                }
            }
            else
            {
                context.Result = new ForbidResult();
            }
        }
    }
}