﻿using Forum.Common.Enums;
using Microsoft.AspNetCore.Mvc;

namespace Forum.API.Filters
{
    public class ClaimRequirementAttribute : TypeFilterAttribute
    {
        public ClaimRequirementAttribute(FunctionCode function, ActionCode action) : base(typeof(ClaimRequirementFilter))
        {
            Arguments = new object[] { ComparisonType.All, action, new FunctionCode[] { function } };
        }

        public ClaimRequirementAttribute(ComparisonType comparisonType, ActionCode action, params FunctionCode[] functions) : base(typeof(ClaimRequirementFilter))
        {
            Arguments = new object[] { comparisonType, action, functions };
        }
    }
}