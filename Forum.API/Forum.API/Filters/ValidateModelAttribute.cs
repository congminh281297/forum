﻿using Forum.Common.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Net;

namespace Forum.API.Filters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var allErrors = context.ModelState.Values.SelectMany(v => v.Errors);

                var errorResult = new ErrorDetails
                {
                    Message = allErrors.First().ErrorMessage,
                    StatusCode = (int)HttpStatusCode.BadRequest
                };

                context.Result = new BadRequestObjectResult(errorResult);
            }
        }
    }
}