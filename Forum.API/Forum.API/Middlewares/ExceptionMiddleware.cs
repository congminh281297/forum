﻿using Forum.Common.BackgroundTasks;
using Forum.Common.Exceptions;
using Forum.Common.Logger;
using Forum.Common.Models;
using Forum.Data.EF;
using Forum.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Forum.API.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILoggerManager _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        public IBackgroundTaskQueue Queue { get; }

        public ExceptionMiddleware(RequestDelegate next,
                                   ILoggerManager logger,
                                   IServiceScopeFactory serviceScopeFactory,
                                   IBackgroundTaskQueue queue)
        {
            _next = next;
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
            Queue = queue;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                HandleLog(ex);
                await HandleExceptionAsync(context, ex);
            }
        }

        public void HandleLog(Exception ex)
        {
            _logger.LogError($"Something went wrong: {ex}");
            Queue.QueueBackgroundWorkItem(async token =>
            {
                using var scope = _serviceScopeFactory.CreateScope();
                var scopedServices = scope.ServiceProvider;
                var auditLogDbContext = scopedServices.GetRequiredService<AuditLogDbContext>();

                var error = new Error
                {
                    DateCreated = DateTime.Now,
                    Message = ex.InnerException?.Message ?? ex.Message,
                    StackTrace = ex.InnerException?.StackTrace ?? ex.StackTrace
                };

                await auditLogDbContext.Errors.AddAsync(error, token);
                await auditLogDbContext.SaveChangesAsync(token);
            });
        }

        public Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            if (ex is NotFoundException)
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            else if (ex is DuplicatedException || ex is BadRequestException)
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            else
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            var errorResult = new ErrorDetails
            {
                Message = ex.InnerException?.Message ?? ex.Message,
                StatusCode = context.Response.StatusCode
            }.ToString();

            context.Response.ContentType = "application/json";
            return context.Response.WriteAsync(errorResult);
        }
    }
}