﻿
using Forum.Common.BackgroundTasks;
using Forum.Common.Extensions;
using Forum.Common.Helpers;
using Forum.Data.Interfaces;
using Forum.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Forum.Data.EF
{
    public class ForumContext : DbContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        public IBackgroundTaskQueue Queue { get; }

        public ForumContext(DbContextOptions<ForumContext> options,
                             IHttpContextAccessor httpContextAccessor,
                             IBackgroundTaskQueue queue,
                             IServiceScopeFactory serviceScopeFactory) : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
            _serviceScopeFactory = serviceScopeFactory;
            Queue = queue;
        }

        public ForumContext(DbContextOptions<ForumContext> options) : base(options)
        {
        }

        public virtual DbSet<NavigationMenu> NavigationMenus { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RoleNavigationMenu> RoleNavigationMenus { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserLogin> UserLogins { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<Topic> Topics { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ConfigureModel(modelBuilder);
        }

        public static void ConfigureModel(ModelBuilder modelBuilder)
        {
           
            modelBuilder.Entity<RoleNavigationMenu>(entity =>
            {
                entity.HasOne(d => d.NavigationMenu)
                    .WithMany(p => p.RoleNavigationMenus)
                    .HasForeignKey(d => d.NavigationMenuId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleNavigationMenus)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<NavigationMenu>(entity =>
            {
                entity.HasOne(d => d.ParentNavMenu)
                    .WithMany(p => p.ChildNavMenus)
                    .HasForeignKey(d => d.ParentId)
                    .OnDelete(DeleteBehavior.NoAction);
                    
            });

            modelBuilder.Entity<UserLogin>(entity =>
            {
                entity.HasOne(d => d.User)
                    .WithOne(p => p.UserLogin)
                    .HasForeignKey<UserLogin>(d => d.Id);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.UserId);
            });
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            var modified = ChangeTracker.Entries().Where(e => e.State == EntityState.Added ||
                                                              e.State == EntityState.Modified ||
                                                              e.State == EntityState.Deleted);
            foreach (var item in modified)
            {
                // DateTracking
                if (item.Entity is IDateTracking dateTracking)
                {
                    if (item.State == EntityState.Added)
                        dateTracking.DateCreated = DateTime.Now;
                    else if (item.State == EntityState.Deleted)
                        dateTracking.DateDeleted = DateTime.Now;

                    dateTracking.DateModified = DateTime.Now;
                }

                // PersonTracking
                if (item.Entity is IPersonTracking personTracking)
                {
                    if (_httpContextAccessor.HttpContext != null &&
                        _httpContextAccessor.HttpContext.User != null &&
                        _httpContextAccessor.HttpContext.User.Identity != null &&
                        _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
                    {
                        var userId = IdentityExtensions.GetUserId(_httpContextAccessor);

                        if (item.State == EntityState.Added)
                            personTracking.CreatorUserId = userId;
                        else if (item.State == EntityState.Deleted)
                            personTracking.DeleterUserId = userId;

                        personTracking.LastModifierUserId = userId;
                    }
                }
            }

            var auditEntries = OnBeforeSaveChanges();

            var result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);

            OnAfterSaveChanges(auditEntries);

            return result;
        }

        #region Private methods

        private List<AuditLogEntry> OnBeforeSaveChanges()
        {
            ChangeTracker.DetectChanges();
            var auditEntries = new List<AuditLogEntry>();
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.Entity is AuditLog || entry.State == EntityState.Detached || entry.State == EntityState.Unchanged)
                    continue;

                var auditEntry = new AuditLogEntry(entry)
                {
                    TableName = entry.Metadata.GetTableName()
                };

                auditEntries.Add(auditEntry);

                foreach (var property in entry.Properties)
                {
                    if (property.IsTemporary)
                    {
                        // value will be generated by the database, get the value after saving
                        auditEntry.TemporaryProperties.Add(property);
                        continue;
                    }

                    string propertyName = property.Metadata.Name;
                    if (property.Metadata.IsPrimaryKey())
                    {
                        auditEntry.KeyValues[propertyName] = property.CurrentValue;
                        continue;
                    }

                    switch (entry.State)
                    {
                        case EntityState.Added:
                            auditEntry.NewValues[propertyName] = property.CurrentValue;

                            break;

                        case EntityState.Modified:
                            if (property.IsModified && !property.OriginalValue.JSONEquals(property.CurrentValue))
                            {
                                auditEntry.OldValues[propertyName] = property.OriginalValue;
                                auditEntry.NewValues[propertyName] = property.CurrentValue;
                            }

                            break;

                        case EntityState.Deleted:
                            auditEntry.OldValues[propertyName] = property.OriginalValue;

                            break;
                    }
                }
            }

            return auditEntries;
        }

        private void OnAfterSaveChanges(List<AuditLogEntry> auditEntries)
        {
            if (auditEntries == null || !auditEntries.Any())
                return;

            Queue.QueueBackgroundWorkItem(async token =>
            {
                using var scope = _serviceScopeFactory.CreateScope();
                var scopedServices = scope.ServiceProvider;
                var auditLogDbContext = scopedServices.GetRequiredService<AuditLogDbContext>();

                // 1. Save audit entities that have all the modifications
                foreach (var auditEntry in auditEntries.Where(_ => !_.HasTemporaryProperties))
                {
                    // Save the Audit entry
                    if (auditEntry.OldValues.Any() || auditEntry.NewValues.Any())
                        await auditLogDbContext.AuditLogs.AddAsync(auditEntry.ToAuditLog(), token);
                }

                // 2 Get the final value of the temporary properties
                var auditEnitiesTemporary = auditEntries.Where(_ => _.HasTemporaryProperties).ToList();
                if (auditEnitiesTemporary.Any())
                {
                    foreach (var auditEntry in auditEnitiesTemporary)
                    {
                        // Get the final value of the temporary properties
                        foreach (var prop in auditEntry.TemporaryProperties)
                        {
                            if (prop.Metadata.IsPrimaryKey())
                                auditEntry.KeyValues[prop.Metadata.Name] = prop.CurrentValue;
                            else
                                auditEntry.NewValues[prop.Metadata.Name] = prop.CurrentValue;
                        }

                        // Save the Audit entry
                        if (auditEntry.OldValues.Any() || auditEntry.NewValues.Any())
                            await auditLogDbContext.AuditLogs.AddAsync(auditEntry.ToAuditLog(), token);
                    }
                }

                await auditLogDbContext.SaveChangesAsync(token);
            });
        }

        #endregion Private methods
    }
}