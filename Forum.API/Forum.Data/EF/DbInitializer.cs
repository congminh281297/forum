﻿using Forum.Common.Constants;
using Forum.Common.Enums;
using Forum.Common.Helpers;
using Forum.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Data.EF
{
    public class DbInitializer
    {
        private readonly ForumContext _context;
        private readonly AuditLogDbContext _auditLogDbContext;

        public DbInitializer(ForumContext context, AuditLogDbContext auditLogDbContext)
        {
            _context = context;
            _auditLogDbContext = auditLogDbContext;
        }

        public async Task Seed()
        {
            await _auditLogDbContext.Database.EnsureCreatedAsync();
            await _context.Database.EnsureCreatedAsync();

            var roles = new Role[]
             {
                new Role{Id = "c8e8ba70-4864-42c1-8e97-f6aea0aa5a73",Name = "Administrator", Description="Administrator"}
             };

            if (!_context.Roles.Any())
            {
                await _context.Roles.AddRangeAsync(roles);
            }

            var user = new User
            {
                Id = "5d9da6a1-a554-4e02-82e7-837561eeb332",
                UserName = "admin",
                FullName = "Administrator",
                Password = PasswordHelper.HashPassword("admin@123"),
            };

            if (!_context.Users.Any())
            {
                await _context.Users.AddAsync(user);
                await _context.UserRoles.AddAsync(new UserRole
                {
                    UserId = user.Id,
                    RoleId = roles.FirstOrDefault(s => s.Name == "Administrator").Id
                });
            }

            var navs = new NavigationMenu[]
            {
                new NavigationMenu{
                    Id= Guid.NewGuid().ToString(),Name="Hệ thống",Svg="Settings-2.svg", Root=true,Bullet="dot",SortOrder=1,
                    ChildNavMenus= new NavigationMenu[]
                    {
                        new NavigationMenu{Id= Guid.NewGuid().ToString(),Name="Khai báo quyền",Url="/system/role",Code=FunctionCode.ROLE.ToString(),SortOrder=1},
                        new NavigationMenu{Id= Guid.NewGuid().ToString(),Name="Khai báo trang",Url="/system/navigation",Code=FunctionCode.NAVIGATION.ToString(),SortOrder=2},
                        new NavigationMenu{Id= Guid.NewGuid().ToString(),Name="Phân quyền",Url="/system/role-navigation",Code=FunctionCode.ROLE_NAVIGATION.ToString(),SortOrder=3},
                        new NavigationMenu{Id= Guid.NewGuid().ToString(),Name="Người dùng",Url="/system/user",Code=FunctionCode.USER.ToString(),SortOrder=4},
                    }
                },
                 new NavigationMenu{
                    Id= Guid.NewGuid().ToString(),Name="Danh mục",Svg="Layout-left-panel-2.svg", Root=true,Bullet="dot",SortOrder=2,
                    ChildNavMenus= new NavigationMenu[]
                    {
                        new NavigationMenu{Id= Guid.NewGuid().ToString(),Name="Ngôn ngữ",Url="/categories/language", Code=FunctionCode.LANGUAGE.ToString(),SortOrder=1},
                        new NavigationMenu{Id= Guid.NewGuid().ToString(),Name="Thủ thuật",Url="/categories/tricks", Code=FunctionCode.TRICKS.ToString(),SortOrder=2},
                    }
                },
                 new NavigationMenu{
                    Id= Guid.NewGuid().ToString(),Name="Chức năng",Svg="Layout-left-panel-2.svg", Root=true,Bullet="dot",SortOrder=3,
                    ChildNavMenus= new NavigationMenu[]
                    {
                        new NavigationMenu{Id= Guid.NewGuid().ToString(),Name="Chủ đề",Url="/features/topic", Code=FunctionCode.TOPIC.ToString(),SortOrder=1},
                        
                    }
                },

            };


            if (!_context.NavigationMenus.Any())
            {
                await _context.NavigationMenus.AddRangeAsync(navs);
            }

            await _context.SaveChangesAsync();
        }
    }
}