﻿using Forum.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Json;

namespace Forum.Data.EF
{
    public class AuditLogDbContext : DbContext
    {
        public AuditLogDbContext(DbContextOptions<AuditLogDbContext> options) : base(options)
        {
        }

        public DbSet<AuditLog> AuditLogs { get; set; }
        public DbSet<Error> Errors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ConfigureModel(modelBuilder);
        }

        public static void ConfigureModel(ModelBuilder modelBuilder)
        {
            var auditLog = modelBuilder.Entity<AuditLog>();
            var error = modelBuilder.Entity<Error>();

            auditLog.HasIndex(_ => _.Id);
            error.HasIndex(_ => _.Id);
        }
    }

    public class AuditLogEntry
    {
        public AuditLogEntry(EntityEntry entry)
        {
            Entry = entry;
        }

        public EntityEntry Entry { get; }
        public string TableName { get; set; }
        public Dictionary<string, object> KeyValues { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> OldValues { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> NewValues { get; } = new Dictionary<string, object>();
        public List<PropertyEntry> TemporaryProperties { get; } = new List<PropertyEntry>();

        public bool HasTemporaryProperties => TemporaryProperties.Any();

        public AuditLog ToAuditLog()
        {
            var jso = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping
            };

            var audit = new AuditLog
            {
                TableName = TableName,
                DateCreated = DateTime.Now,
                KeyValues = !KeyValues.Any() ? null : JsonSerializer.Serialize(KeyValues, jso),
                OldValues = !OldValues.Any() ? null : JsonSerializer.Serialize(OldValues, jso),
                NewValues = !NewValues.Any() ? null : JsonSerializer.Serialize(NewValues, jso),
            };

            return audit;
        }
    }
}