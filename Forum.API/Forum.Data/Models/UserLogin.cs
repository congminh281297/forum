﻿using Forum.Common.Constants;
using Forum.Data.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.Data.Models
{
    [Table("UserLogins")]
    public class UserLogin : BaseEntity
    {
        [Required, MaxLength(ValidatorConsts.MaxLengthGuid)]

        public string ProviderKey { get; set; }
        public DateTime LoginTime { get; set; }
        public DateTime ExpiresTime { get; set; }

        [MaxLength(500)]
        public string RefreshToken { get; set; }

        public virtual User User { get; set; }
    }
}
