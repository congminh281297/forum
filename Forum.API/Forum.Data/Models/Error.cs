﻿using Forum.Data.Abstract;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.Data.Models
{
    [Table("Errors")]
    public class Error : BaseEntity
    {
        public Error()
        {
            Id = Guid.NewGuid().ToString();
        }

        public DateTime DateCreated { set; get; }
        public string? Message { set; get; }
        public string? StackTrace { set; get; }
    }
}