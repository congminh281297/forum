﻿using Forum.Common.Constants;
using Forum.Data.Abstract;
using Forum.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.Data.Models
{
    [Table("Users")]
    public class User : BaseEntity, IDateTracking, IPersonTracking, IHasSoftDelete, ISwitchable
    {
        public User()
        {
            Id = Guid.NewGuid().ToString();
            UserRoles = new HashSet<UserRole>();
        }

        [Required, MaxLength(50)]
        public string UserName { get; set; }

        [Required, MaxLength(128)]
        public string Password { get; set; }

        [Required, MaxLength(250)]
        public string FullName { get; set; }

        [MaxLength(250)]
        public string? Image { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public DateTime? DateDeleted { get; set; }

        [MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string? CreatorUserId { get; set; }

        [MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string? LastModifierUserId { get; set; }

        [MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string? DeleterUserId { get; set; }

        public bool IsDeleted { get; set; }
        public bool IsLocked { get; set; }

        public virtual UserLogin UserLogin { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}