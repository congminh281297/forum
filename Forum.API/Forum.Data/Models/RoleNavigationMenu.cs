﻿using Forum.Common.Constants;
using Forum.Data.Abstract;
using Forum.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Forum.Data.Models
{
    [Table("RoleNavigationMenus")]
    public class RoleNavigationMenu: BaseEntity
    {
        public RoleNavigationMenu()
        {
            Id = Guid.NewGuid().ToString();
        }
        [Required,MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string RoleId { get; set; }
        [Required, MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string NavigationMenuId { get; set; }
        public int Value { get; set; }

        public virtual NavigationMenu NavigationMenu { get; set; }
        public virtual Role Role { get; set; }
    }
}
