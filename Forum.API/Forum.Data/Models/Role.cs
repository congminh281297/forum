﻿using Forum.Common.Constants;
using Forum.Data.Abstract;
using Forum.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.Data.Models
{
    [Table("Roles")]
    public class Role: BaseEntity, IDateTracking, IPersonTracking, IHasSoftDelete, ISwitchable
    {
        public Role()
        {
            Id = Guid.NewGuid().ToString();
            RoleNavigationMenus = new HashSet<RoleNavigationMenu>();
            UserRoles = new HashSet<UserRole>();
        }

        [Required,MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(250)]
        public string? Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        [MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string? CreatorUserId { get; set; }
        [MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string? LastModifierUserId { get; set; }
        [MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string? DeleterUserId { get; set; }
        public bool IsLocked { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<RoleNavigationMenu> RoleNavigationMenus { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
