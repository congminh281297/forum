﻿using Forum.Data.Abstract;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.Data.Models
{
    [Table("AuditLogs")]
    public class AuditLog : BaseEntity
    {
        public AuditLog()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string? TableName { get; set; }
        public DateTime DateCreated { get; set; }
        public string? KeyValues { get; set; }
        public string? OldValues { get; set; }
        public string? NewValues { get; set; }
    }
}