﻿using Forum.Common.Constants;
using Forum.Data.Abstract;
using Forum.Data.Interfaces;
using Forum.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Forum.Data.Models
{
    public class NavigationMenu: BaseEntity, ISortable, ISwitchable
    {
        public NavigationMenu()
        {
            Id = Guid.NewGuid().ToString();
            IsLockClient = false;
            RoleNavigationMenus = new HashSet<RoleNavigationMenu>();
            ChildNavMenus = new HashSet<NavigationMenu>();
        }

        [Required, MaxLength(128)]
        public string Name { get; set; }
        [MaxLength(50)]
        public string? Code { get; set; }
        [MaxLength(250)]
        public string? Url { get; set; }
        [MaxLength(50)]
        public string? Icon { get; set; }
        [MaxLength(50)]
        public string? Svg { get; set; }
        public bool Root { get; set; }
        [MaxLength(50)]
        public string? Bullet { get; set; }
        [MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string? ParentId { get; set; }
        public bool IsSection { get; set; }
        public bool IsLockClient { get; set; }
        public int SortOrder { get; set; }
        public bool IsLocked { get; set; }

        public NavigationMenu ParentNavMenu { get; set; }
        public ICollection<NavigationMenu> ChildNavMenus { get; set; }
        public virtual ICollection<RoleNavigationMenu> RoleNavigationMenus { get; set; }
    }
}
