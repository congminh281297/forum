﻿using Forum.Data.Abstract;
using Forum.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Data.Models
{
    [Table("Topics")]
    public class Topic : BaseEntity, IDateTracking, ISortable, IPersonTracking, ISwitchable
    {
        public Topic()
        {
            Id = Guid.NewGuid().ToString();
        }
        [Required]
        public string Url { get; set; }
        
        [Required]
        public string Subject { get; set; }
        
        [Required]
        public string Description { get; set; }

        [Required]
        public string Code { get; set; }
        
        [Required]
        public string CategoryChildId { get; set; }

        public string LinkOnline { get; set; }
        public int View { get; set; }
        public int Like { get;set; }
        public DateTime DateCreated { get ; set ; }
        public DateTime DateModified { get ; set ; }
        public DateTime? DateDeleted { get ; set ; }
        public int SortOrder { get ; set ; }
        public string? CreatorUserId { get; set; }
        public string? LastModifierUserId { get ; set ; }
        public string? DeleterUserId { get ; set ; }
        public bool IsLocked { get ; set ; }
    }
}
