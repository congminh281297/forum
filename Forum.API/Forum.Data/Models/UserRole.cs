﻿using Forum.Common.Constants;
using Forum.Data.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.Data.Models
{
    [Table("UserRoles")]
    public class UserRole : BaseEntity
    {
        public UserRole()
        {
            Id = Guid.NewGuid().ToString();
        }

        [MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string UserId { get; set; }
        [MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string RoleId { get; set; }

        public virtual Role Role { get; set; }
        public virtual User User { get; set; }
    }
}
