﻿using Forum.Data.Abstract;
using Forum.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Data.Models
{
    [Table("Languages")]
    public class Language : BaseEntity, IDateTracking, IPersonTracking, ISwitchable
    {
        public Language()
        {
            Id = Guid.NewGuid().ToString();
        }
        [Required]
        public string Slug { get; set; }

        [Required]
        public string Name { get; set; }

        public bool IsLocked { get; set; }
        public string CreatorUserId { get; set; }
        public string LastModifierUserId { get; set; }
        public string? DeleterUserId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public DateTime? DateDeleted { get; set; }
    }
}
