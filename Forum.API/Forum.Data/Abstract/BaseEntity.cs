﻿using Forum.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace Forum.Data.Abstract
{
    public abstract class BaseEntity
    {
        [Key, Required, MaxLength(ValidatorConsts.MaxLengthGuid)]
        public string Id { get; set; }
    }
}