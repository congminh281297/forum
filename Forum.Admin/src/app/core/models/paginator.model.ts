export interface IPaginatorState {
    page: number;
    pageSize: number;
    total: number;
}

export class PaginatorState implements IPaginatorState {
    pageSizes: number[] = [10, 20, 50, 100];
    page = 1;
    pageSize = this.pageSizes[0];
    total = 0;

    get start() {
        if(this.total == 0)
            return 0;
        return (this.page - 1) * this.pageSize + 1;
    }

    get end() {
        if (this.pageSize > this.total ||
            this.pageSize * this.page > this.total) {
            return this.total;
        } else {
            return this.page * this.pageSize;
        }
    }
}
