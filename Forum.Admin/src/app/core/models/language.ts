export interface Language {
    id: string;
    slug: string;
    name: string;
    isLocked: boolean;
}