import { FormGroup } from '@angular/forms';
import * as moment from 'moment';

export function CompareDateValidator(fromDateControlName: string, toDateControlName: string) {
    return (formGroup: FormGroup) => {
        const from = formGroup.controls[fromDateControlName];
        const to = formGroup.controls[toDateControlName];
        if (to.errors && !to.errors.compareValidator) {
            return;
        }
        if (moment(from.value).isAfter(to.value)) {
            to.setErrors({ compareValidator: true });
        } else {
            to.setErrors(null);
        }
    }
}