import { TranslationWidth } from '@angular/common';
import { Injectable } from '@angular/core';
import { NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
@Injectable({
    providedIn: 'root',
})
export class CustomAdapter extends NgbDateAdapter<string> {

    readonly DELIMITER = '-';

    fromModel(value: string | null): NgbDateStruct | null {
        if (value) {
            let date = value.split(/[-/]/);
            if (date[0].length == 4)
                return {
                    day: parseInt(date[2], 10),
                    month: parseInt(date[1], 10),
                    year: parseInt(date[0], 10)
                };
            return {
                day: parseInt(date[0], 10),
                month: parseInt(date[1], 10),
                year: parseInt(date[2], 10)
            };
        }
        return null;
    }

    toModel(date: NgbDateStruct | null): string | null {
        return date ? (date.day < 10 ? '0' + date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0' + date.month : date.month) + this.DELIMITER + date.year : null;
    }
}

/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable({
    providedIn: 'root',
})
export class CustomDateParserFormatter extends NgbDateParserFormatter {

    readonly DELIMITER = '/';

    parse(value: string): NgbDateStruct | null {
        if (value) {
            let date = value.split(this.DELIMITER);
            return {
                day: parseInt(date[0], 10),
                month: parseInt(date[1], 10),
                year: parseInt(date[2], 10)
            };
        }
        return null;
    }

    format(date: NgbDateStruct | null): string {
        return date ? (date.day < 10 ? '0' + date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0' + date.month : date.month) + this.DELIMITER + date.year : '';
    }
}

@Injectable({
    providedIn: 'root',
})
export class CustomDatepickerI18n extends NgbDatepickerI18n {
    getWeekdayLabel(weekday: number, width?: TranslationWidth): string {
        throw new Error('Method not implemented.');
    }
    I18N_VALUES: any = {
        'vi': {
            weekdays: ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'],
            months: ['Th 1', 'Th 2', 'Th 3', 'Th 4', 'Th 5', 'Th 6', 'Th 7', 'Th 8', 'Th 9', 'Th 10', 'Th 11', 'Th 12'],
        }
    };

    language = 'vi';

    constructor() {
        super();
    }

    getWeekdayShortName(weekday: number): string {
        return this.I18N_VALUES[this.language].weekdays[weekday - 1];
    }

    getMonthShortName(month: number): string {
        return this.I18N_VALUES[this.language].months[month - 1];
    }

    getMonthFullName(month: number): string {
        return this.getMonthShortName(month);
    }

    getDayAriaLabel(date: NgbDateStruct): string {
        return `${date.day}-${date.month}-${date.year}`;
    }
}