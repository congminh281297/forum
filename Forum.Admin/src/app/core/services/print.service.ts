import { Injectable } from '@angular/core';

import * as $ from "jquery";
@Injectable({
    providedIn: 'root',
})
export class PrintService {
    private opt: any;
    constructor() { }
    /**
     * 
     * @param el 
     * @param callback hàm sau khi in hoặc kết thúc in
     * @param options 
     */
    Print(el: any, callback: any = null, options: any = null) {
        this.opt = options || new PrintOption();
        this.opt.loadCSS.push("../../assets/base/print.css");
        var $element = $(el);
        var strFrameName = "printThis-" + (new Date()).getTime();
        if (window.location.hostname !== document.domain && navigator.userAgent.match(/msie/i)) {
            var iframeSrc = "javascript:document.write(\"<head><script>document.domain=\\\"" + document.domain + "\\\";</s" + "cript></head><body></body>\")";
            var printI = document.createElement('iframe');
            printI.name = "printIframe";
            printI.id = strFrameName;
            printI.className = "MSIE";
            document.body.appendChild(printI);
            printI.src = iframeSrc;

        } else {
            var $frame = $("<iframe id='" + strFrameName + "' name='printIframe' />");
            $frame.appendTo("body");
        }
        $("body").addClass('notransition');
        var $iframe = $("#" + strFrameName);
        if (!this.opt.debug)
            $iframe.css({
                position: "absolute",
                width: "0px",
                height: "0px",
                left: "-600px",
                top: "-600px"
            });
        else {
            $iframe.css({
                width: "250mm",
                height: "100%",
                left: "0px",
                position: "fixed",
                'z-index': 99999,
                'background-color': 'black'
            });
        }
        setTimeout(() => {
            function setDocType($iframe: any, doctype: any) {
                var win, doc;
                win = $iframe.get(0);
                win = win.contentWindow || win.contentDocument || win;
                doc = win.document || win.contentDocument || win;
                doc.open();
                doc.write(doctype);
                doc.close();
            }
            if (this.opt.doctypeString) {
                setDocType($iframe, this.opt.doctypeString);
            }
            var $doc = $iframe.contents(),
                $head = $doc.find("head"),
                $body = $doc.find("body"),
                $base = $('base'),
                baseURL;
            $head.append(`<style type="text/css">@page {margin: 0px;}body{margin: 0px;padding:0px}</style>`);
            if (this.opt.base === true && $base.length > 0) {
                baseURL = $base.attr('href');
            } else if (typeof this.opt.base === 'string') {
                baseURL = this.opt.base;
            } else {
                baseURL = document.location.protocol + '//' + document.location.host;
            }
            /**
              * add style sheet
              */
            // $head.append('<base href="' + baseURL + '">');
            // if (this.opt.importCSS) $("link[rel=stylesheet]").each(function () {
            //     var href = $(this).attr("href");
            //     if (href) {
            //         var media = $(this).attr("media") || "all";
            //         $head.append("<link type='text/css' rel='stylesheet' href='" + href + "' media='" + media + "'>");
            //     }
            // });
            // if (this.opt.importStyle) $("style").each(function () {
            //     $head.append(this.outerHTML);
            // });
            if (this.opt.pageTitle) $head.append("<title>" + this.opt.pageTitle + "</title>");
            if (this.opt.loadCSS) {
                if ($.isArray(this.opt.loadCSS)) {
                    $.each(this.opt.loadCSS, function (index: any, value: any) {
                        $head.append("<link type='text/css' rel='stylesheet' href='" + value + "'>");
                    });
                } else {
                    $head.append("<link type='text/css' rel='stylesheet' href='" + this.opt.loadCSS + "'>");
                }
            }
            var tag = this.opt.copyTagClasses;
            if (tag) {
                tag = tag === true ? 'bh' : tag;
                if (tag.indexOf('b') !== -1) {
                    $body.addClass($('body')[0].className);
                }
                if (tag.indexOf('h') !== -1) {
                    $doc.find('html').addClass($('html')[0].className);
                }
            }
            /**
             * add canavas
             */
            this.appendContent($body, this.opt.header);
            // if (this.opt.canvas) {
            //     var canvasId = 0;
            //     $element.find('canvas').addBack('canvas').each(function () {
            //         $(this).attr('data-printthis', canvasId++);
            //     });
            // }
            this.appendBody($body, $element, this.opt);
            // if (this.opt.canvas) {
            //     $body.find('canvas').each(function () {
            //         var cid = $(this).data('printthis'),
            //             $src = $('[data-printthis="' + cid + '"]');
            //         this.getContext('2d').drawImage($src[0], 0, 0);
            //         $src.removeData('printthis');
            //     });
            // }
            if (this.opt.removeInline) {
                if ($.isFunction($.removeData)) {
                    $doc.find("body *").removeAttr("style");
                } else {
                    $doc.find("body *").attr("style", "");
                }
            }
            this.appendContent($body, this.opt.footer);
            setTimeout(() => {
                if ($iframe.hasClass("MSIE")) {
                    (window.frames as { [key: string]: any })["printIframe"].focus();
                    $head.append("<script>  window.print(); </s" + "cript>");
                } else {
                    if (callback != null)
                        $iframe[0].addEventListener("afterprint", function (event: any) { callback(); });
                    if (document.queryCommandSupported("print")) {
                    } else {
                        $iframe[0].focus();
                    }
                }
                $("body").removeClass('notransition');

                if (!this.opt.debug) {
                    setTimeout(() => {
                        $iframe.remove();
                    }, 200);
                }
            }, this.opt.printDelay);
        }, 333);
    }
    appendContent($el: any, content: any) {
        if (!content) return;
        $el.append(content.jquery ? content.clone() : content);
    }
    appendBody($body: any, $element: any, opt: any) {
        var $content = $element.clone(opt.formValues);
        if (opt.formValues) {
            this.copyValues($element, $content, 'select, textarea');
        }
        if (opt.removeScripts) {
            $content.find('script').remove();
        }
        if (opt.printContainer) {
            $content.find('#print-bottom').appendTo($body);
            $content.appendTo($body);
        } else {
            $content.each(() => {
                $(this).children().appendTo($body)
            });
        }
    }
    copyValues(origin: any, clone: any, elementSelector: any) {
        var $originalElements = origin.find(elementSelector);
        clone.find(elementSelector).each((index: any, item: any) => {
            $(item).val($originalElements.eq(index).val());
        });
    }
}
export interface IPrintOption {
    importCSS: boolean;
    importStyle: boolean;
    printContainer: boolean;
    loadCSS: any[];
    pageTitle: string;
    removeInline: boolean;
    printDelay: number;
    header: any;
    footer: any;
    formValues: boolean;
    canvas: boolean;
    base: boolean;
    doctypeString: string;
    removeScripts: boolean;
    copyTagClasses: boolean;
    debug: boolean;
}
export class PrintOption implements IPrintOption {
    importCSS = false;
    importStyle = false;
    printContainer = true;
    loadCSS = [];
    pageTitle = '';
    removeInline = false;
    printDelay = 333;
    header = null;
    footer = `<div class='sa-rpt-footer'>Design by SongAn Co.LTD : <u>WWW.EHIS.VN</u></div>`;
    formValues = true;
    canvas = false;
    base!: boolean;
    doctypeString = '<!DOCTYPE html>';
    removeScripts = true;
    copyTagClasses = !1;
    debug = !1;
}