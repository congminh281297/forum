import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class UtilityService {
    private _router: Router;

    constructor(router: Router) {
        this._router = router;
    }

    navigate(path: string) {
        this._router.navigate([path]);
    }

    unflattenMenus = (arr: any[]): any[] => {
        let map: any = {};
        let roots: any[] = [];
        for (var i = 0; i < arr.length; i += 1) {
            let node = arr[i];
            if (node.isSection) {
                roots.push({ id: node.id, section: node.name });
            } else {
                node.submenu = [];
                map[node.id] = i;
                if (node.parentId !== null && arr.some(x => x.id === node.parentId && !x.isSection)) {
                    arr[map[node.parentId]].submenu.push({
                        id: node.id,
                        parentId: node.parentId,
                        title: node.name,
                        page: node.url,
                        root: node.root,
                        bullet: node.bullet,
                        icon: node.icon,
                        svg: node.svg,
                        submenu: node.submenu,
                    });
                } else {
                    roots.push({
                        id: node.id,
                        parentId: node.parentId,
                        title: node.name,
                        root: node.root,
                        bullet: node.bullet,
                        icon: node.icon,
                        svg: node.svg,
                        submenu: node.submenu,
                    });
                }
            }
        }

        return roots;
    }

    unflatten = (arr: any[]): any[] => {
        let map: any = {};
        let roots: any[] = [];
        for (var i = 0; i < arr.length; i += 1) {
            let node = arr[i];
            node.children = [];
            map[node.id] = i;
            if (node.parentId !== null) {
                const parent = arr.find(x => x.id === node.parentId);
                arr[i].tenantId = parent.tenantId;
                arr[map[node.parentId]]?.children.push(node);
            } else {
                arr[i].tenantId = this.uuidv4();
                roots.push(node);
            }
        }
        return roots;
    }

    /**
     * chuyển dữ liệu sang tree
     */
    build_tree_data(data: any[], parent: any) {
        let result: any[] = [];
        data.filter(s => s.parentId == parent).forEach(s => {
            var item = { ...s };
            item.submenu = this.build_tree_data(data, s.id)
            if (item.submenu.length > 0 || item.url != null)
                result.push({
                    id: item.id,
                    parentId: item.parentId,
                    title: item.name,
                    page: item.url,
                    root: item.root,
                    bullet: item.bullet,
                    icon: item.icon,
                    svg: item.svg,
                    submenu: item.submenu,
                });
        });
        return result;
    }

    printConsoleWarning() {
        const warningTitleCSS = 'color:red; font-size:80px; font-weight: bold; -webkit-text-stroke: 1px black;';
        const warningDescCSS = 'font-size: 28px;';

        console.log('%cStop!', warningTitleCSS);
        console.log("%cThis is a browser feature intended for developers. If you are not a developer, or unsure what you are doing here, STOP RIGHT NOW!!!", warningDescCSS);
    }

    uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}
