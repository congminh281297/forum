import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConfigModel } from '../models/app-config.model';
@Injectable()
export class AppConfigService {
    static settings: AppConfigModel;
    constructor(private http: HttpClient) { }
    load() {
        const jsonFile = `config/config.json`;
        return new Promise<void>((resolve, reject) => {
            this.http.get(jsonFile).toPromise().then((response: any) => {
                AppConfigService.settings = <AppConfigModel>response;
                resolve();
            }).catch((response: any) => {
                reject(`Lỗi file cấu hình!`);
            });
        });
    }
}