import { Injectable, } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from './login.service';
import { throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { UrlConstants } from '../constants/url.constant';
import { environment } from 'src/environments/environment';
import { MessageConstants } from '../constants/message.constants';
import { NotificationService } from './notification.service';
import { UtilityService } from './utility.service';

@Injectable()
export class HttpClientService {

    constructor(
        private activated: ActivatedRoute,
        private httpClient: HttpClient,
        private router: Router,
        private notificationService: NotificationService,
        private loginService: LoginService,
        private utilityService: UtilityService) { }

    get(url: string, loadding?: loadingModel) {
        if (loadding) {
            (<loadingModel>loadding).isGet = true;
        }

        return this.httpClient.get(environment.BASE_API + url, { headers: this.getHeaders() })
            .pipe(
                catchError(error => {
                    return this.handleError(error);
                }),
                finalize(() => {
                    if (loadding) {
                        (<loadingModel>loadding).isGet = false;
                    }
                })
            );
    }

    getWithParams(url: string, params: any, loadding?: loadingModel) {
        if (loadding) {
            (<loadingModel>loadding).isGet = true;
        }

        let paramStr: string = '';
        for (let param in params) {
            paramStr += param + "=" + params[param] + '&';
        }

        return this.httpClient.get(environment.BASE_API + url + "?" + paramStr, { headers: this.getHeaders() })
            .pipe(
                catchError((error) => {
                    return this.handleError(error);
                }),
                finalize(() => {
                    if (loadding) {
                        (<loadingModel>loadding).isGet = false;
                    }
                })
            );
    }

    post(url: string, data: any, loadding?: loadingModel) {
        if (loadding) {
            (<loadingModel>loadding).isPost = true;
        }

        return this.httpClient.post(environment.BASE_API + url, data, { headers: this.getHeaders() })
            .pipe(
                catchError((error) => {
                    return this.handleError(error);
                }),
                finalize(() => {
                    if (loadding) {
                        (<loadingModel>loadding).isPost = false;
                    }
                })
            );
    }

    put(url: string, data: any, loadding?: loadingModel) {
        if (loadding) {
            (<loadingModel>loadding).isPut = true;
        }

        return this.httpClient.put(environment.BASE_API + url, data, { headers: this.getHeaders() })
            .pipe(
                catchError((error) => {
                    return this.handleError(error);
                }),
                finalize(() => {
                    if (loadding) {
                        (<loadingModel>loadding).isPut = false;
                    }
                })
            );
    }

    delete(url: string, loadding?: loadingModel) {
        if (loadding) {
            (<loadingModel>loadding).isDelete = true;
        }

        return this.httpClient.delete(environment.BASE_API + url, { headers: this.getHeaders() })
            .pipe(
                catchError((error) => {
                    return this.handleError(error);
                }),
                finalize(() => {
                    if (loadding) {
                        (<loadingModel>loadding).isDelete = false;
                    }
                })
            );
    }

    deleteWithParams(url: string, params: any, loadding?: loadingModel) {
        if (loadding) {
            (<loadingModel>loadding).isDelete = true;
        }

        let paramStr: string = '';
        for (let param in params) {
            paramStr += param + "=" + params[param] + '&';
        }

        return this.httpClient.delete(environment.BASE_API + url + "?" + paramStr, { headers: this.getHeaders() })
            .pipe(
                catchError((error) => {
                    return this.handleError(error);
                }),
                finalize(() => {
                    if (loadding) {
                        (<loadingModel>loadding).isDelete = false;
                    }
                })
            );
    }

    deleteWithBody(url: string, data: any, loadding?: loadingModel) {
        if (loadding) {
            (<loadingModel>loadding).isDelete = true;
        }

        const options = {
            headers: this.getHeaders(),
            body: data
        };

        return this.httpClient.delete(environment.BASE_API + url, options)
            .pipe(
                catchError((error) => {
                    return this.handleError(error);
                }),
                finalize(() => {
                    if (loadding) {
                        (<loadingModel>loadding).isDelete = false;
                    }
                })
            );
    }

    postFile(url: string, data: any, loadding?: loadingModel) {
        if (loadding) {
            (<loadingModel>loadding).isPost = true;
        }

        let newHeader = new HttpHeaders();
        const userlogin = this.loginService.getLoggedInUser();
        if (userlogin) {
            newHeader = newHeader.set("Authorization", "Bearer " + userlogin.token);
        }

        return this.httpClient.post(environment.BASE_API + url, data, { headers: newHeader })
            .pipe(
                catchError((error) => {
                    return this.handleError(error);
                }),
                finalize(() => {
                    if (loadding) {
                        (<loadingModel>loadding).isPost = false;
                    }
                })
            );
    }

    postFileToServerUpload(files: any[]) {
        let newHeader = new HttpHeaders();
        const userlogin = this.loginService.getLoggedInUser();
        if (userlogin) {
            newHeader = newHeader.set("Authorization", "Bearer " + userlogin.token);
        }

        let filesToUpload: File[] = files;
        const formData = new FormData();

        Array.from(filesToUpload).map((file, index) => {
            return formData.append('file' + index, file, file.name);
        });

        return this.httpClient.post(environment.BASE_API + '/api/upload', formData, { headers: newHeader })
            .pipe(catchError(this.handleError));
    }

    getHeaders() {
        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json');
        const userlogin = this.loginService.getLoggedInUser();
        if (userlogin) {
            headers = headers.set("Authorization", "Bearer " + userlogin.token);
        }

        return headers;
    }

    handleError(error: any) {
        if (error.status == 401) {
            this.loginService.logout();
            this.notificationService.error(MessageConstants.LOGIN_AGAIN_MSG);
            if (this.router.url.includes("/login")) {
                const returnUrl = this.activated.snapshot.queryParams['returnUrl'] || '/';
                this.router.navigate([UrlConstants.LOGIN], { queryParams: { returnUrl: returnUrl } });
            } else
                this.router.navigate([UrlConstants.LOGIN], { queryParams: { returnUrl: this.router.url } });
            return throwError(MessageConstants.LOGIN_AGAIN_MSG);
        } else if (error.status == 403) {
            this.notificationService.error(MessageConstants.FORBIDDEN);
            this.utilityService.navigate(UrlConstants.FORBIDDEN)
            return throwError(MessageConstants.FORBIDDEN);
        } else if (error.status == 404 || error.status == 400) {
            this.notificationService.error(error.error.message);
            return throwError(error.error.message);
        } else {
            if (environment.production) {
                this.notificationService.error(MessageConstants.SYSTEM_ERROR_MSG);
                return throwError(MessageConstants.SYSTEM_ERROR_MSG);
            } else {
                this.notificationService.error((error.error && error.error.message) || error.message);
                return throwError(error);
            }
        }
    }
}

export class loadingModel {
    isGet: boolean;
    isPut: boolean;
    isPost: boolean;
    isDelete: boolean;
    constructor() {
        this.isGet = false;
        this.isPut = false;
        this.isPost = false;
        this.isDelete = false;
    }
}