import { Injectable } from '@angular/core';
const ChuSo = [' không ', ' một ', ' hai ', ' ba ', ' bốn ', ' năm ', ' sáu ', ' bảy ', ' tám ', ' chín '];
const Tien = [' ', ' nghìn', ' triệu', ' tỷ', ' nghìn tỷ', ' triệu tỷ'];
@Injectable()
export class NumberService {
  public format(number: any, symbol: string = '', places: any = 4, thousand: string = '.', decimal: string = ','): string {
    symbol = symbol || ''; places = places || 4; thousand = thousand || '.'; decimal = decimal || ',';
    if (!(number)) { number = '0'; } number = '' + number;
    if(0 < number && number < 1)
      return number;
    const so = parseFloat((parseFloat(number) || 0).toFixed(places));
    const str_so = so.toString();
    const str_songuyen = str_so.substr(0, (str_so.indexOf('.') > -1 ? str_so.indexOf('.') : str_so.length));
    let sole = decimal + (str_so.substr(str_so.indexOf('.') > -1 ? str_so.indexOf('.') + 1 : str_so.length));
    if (parseInt(sole.substr(1), undefined) === 0 || sole.substr(1) === '')
      sole = '';
    else
      do {
        if (sole.substr(sole.length - 1) === '0')
          sole = sole.substr(0, sole.length - 1);
        else
          break;
      }
      while (true);
    let num = str_songuyen.replace(/^0*/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, thousand);
    if (!num) { num = ''; }
    return num + sole + symbol;
  }
  public format_theodonvi(number: any, symbol: string = '', places: any = 4, thousand: string = '.', decimal: string = ','): string {
    const value = number;
    if (value > 999999999) {
      return this.format(value / 1000000000, symbol, places, thousand, decimal) + ' tỷ';
    }
    else if (value > 999999) {
      return this.format(value / 1000000, symbol, places, thousand, decimal) + ' triệu';
    }
    else if (value > 999) {
      return this.format(value / 1000, symbol, places, thousand, decimal) + ' nghìn';
    }
    else {
      return this.format(value, symbol, places, thousand, decimal) + ' đồng';
    }
  }
  /** Đọc số thành chữ */
  public DocSoTien(SoTien: number, DonVi: string = ''): string {
    let lan = 0;
    let i = 0;
    let so = 0;
    let KetQua = '';
    let tmp = '';
    DonVi = (DonVi && DonVi != null ? DonVi : 'đồng');
    const ViTri = new Array();

    if (SoTien < 0) {
      KetQua = ' Âm ';
      SoTien = 0 - SoTien;
    }
    if (SoTien === 0) {
      return `Không ${DonVi.trim()}.`;
    }
    if (SoTien > 0) {
      so = SoTien;
    } else {
      so = -SoTien;
    }
    if (SoTien > 9999999999999998) { return 'Số quá lớn!'; }
    ViTri[5] = Math.floor(so / 1000000000000000);
    if (isNaN(ViTri[5])) { ViTri[5] = '0'; }

    so = so - parseFloat(ViTri[5].toString()) * 1000000000000000;
    ViTri[4] = Math.floor(so / 1000000000000);
    if (isNaN(ViTri[4])) { ViTri[4] = '0'; }

    so = so - parseFloat(ViTri[4].toString()) * 1000000000000;
    ViTri[3] = Math.floor(so / 1000000000);

    if (isNaN(ViTri[3])) { ViTri[3] = '0'; }

    so = so - parseFloat(ViTri[3].toString()) * 1000000000;
    ViTri[2] = parseInt((so / 1000000).toString(), undefined);

    if (isNaN(ViTri[2])) { ViTri[2] = '0'; }

    ViTri[1] = parseInt(((so % 1000000) / 1000).toString(), undefined);

    if (isNaN(ViTri[1])) { ViTri[1] = '0'; }

    ViTri[0] = parseInt((so % 1000).toString(), undefined);

    if (isNaN(ViTri[0])) {
      ViTri[0] = '0';
    }

    if (ViTri[5] > 0) {
      lan = 5;
    } else if (ViTri[4] > 0) {
      lan = 4;
    } else if (ViTri[3] > 0) {
      lan = 3;
    } else if (ViTri[2] > 0) {
      lan = 2;
    } else if (ViTri[1] > 0) {
      lan = 1;
    } else {
      lan = 0;
    }

    for (i = lan; i >= 0; i--) {
      tmp = this.DocSo3ChuSo(ViTri[i]);
      KetQua += tmp;
      if (ViTri[i] > 0) {
        KetQua += Tien[i];
      }
      if ((i > 0) && (tmp.length > 0)) {
        KetQua += ',';
      }
    }
    if (KetQua.substring(KetQua.length - 1) === ',') {
      KetQua = KetQua.substring(0, KetQua.length - 1);
    }
    KetQua = KetQua.substring(1, 2).toUpperCase() + KetQua.substring(2);
    while (KetQua.includes('  ')) {
      KetQua = KetQua.replace('  ', ' ');
    }
    if (KetQua.trim().length === 0) {
      return '';
    }
    return `${KetQua.trim()} ${DonVi.trim()}.`;
  }
  private DocSo3ChuSo(baso: number) {
    let tram: number;
    let chuc: number;
    let donvi: number;
    let KetQua = '';
    tram = parseInt((baso / 100).toString(), 10);
    chuc = parseInt(((baso % 100) / 10).toString(), 10);
    donvi = baso % 10;
    if (tram === 0 && chuc === 0 && donvi === 0) {
      return '';
    }
    if (tram !== 0) {
      KetQua += ChuSo[tram] + ' trăm ';
      if ((chuc === 0) && (donvi !== 0)) { KetQua += ' linh '; }
    }
    if ((chuc !== 0) && (chuc !== 1)) {
      KetQua += ChuSo[chuc] + ' mươi';
      if ((chuc === 0) && (donvi !== 0)) {
        KetQua = KetQua + ' linh ';
      }
    }
    if (chuc === 1) {
      KetQua += ' mười ';
    }
    switch (donvi) {
      case 1:
        if ((chuc !== 0) && (chuc !== 1)) {
          KetQua += ' mốt ';
        } else {
          KetQua += ChuSo[donvi];
        }
        break;
      case 5:
        if (chuc === 0) {
          KetQua += ChuSo[donvi];
        } else {
          KetQua += ' lăm ';
        }
        break;
      default:
        if (donvi !== 0) {
          KetQua += ChuSo[donvi];
        }
        break;
    }
    return KetQua;
  }
  /** Hàm làm xử lý số mặc định 2 số lẻ
   ** number: Số cần xử lý
   ** fixed: Độ dài số thập phân mặc định 2 số
   */
  public parseFloat(number: any, fixed: number = 2) {
    return parseFloat(number.toFixed(2));
  }
}
