import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class NotificationService {
    constructor(private toastrService: ToastrService) { }

    validator() {
        this.toastrService.error('Vui lòng kiểm tra lại những nơi được tô đỏ.', 'Thông tin chưa hợp lệ!');
    }

    success(mes: string, title?: string) {
        this.toastrService.success(mes, title);
    }

    createSuccess() {
        this.toastrService.success('Đã thêm mới thành công thông tin.', 'Thêm mới thành công!');
    }

    updateSuccess() {
        this.toastrService.success('Đã lưu thành công thông tin chỉnh sửa.', 'Chỉnh sửa thành công!');
    }

    deleteSuccess() {
        this.toastrService.success('Đã xóa thành công thông tin.', 'Xóa thành công!');
    }

    resetCache() {
        this.toastrService.success('Cập nhập thành công bộ nhớ Cache.');
    }

    error(mes: string, title?: string) {
        this.toastrService.error(mes, title);
    }

    saveSuccess(add: boolean) {
        add ? this.createSuccess() : this.updateSuccess();
    }

    info(mes: string, title?: string) {
        this.toastrService.info(mes, title);
    }

    warning(mes: string, title?: string) {
        this.toastrService.warning(mes, title);
    }
}
