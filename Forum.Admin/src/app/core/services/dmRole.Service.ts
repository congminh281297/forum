import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClientService } from './http-client.service';

@Injectable({
    providedIn: 'root',
})
export class DMRoleService {
    private _dataSubject: BehaviorSubject<any> = new BehaviorSubject([]);
    public readonly data$: Observable<any> = this._dataSubject.asObservable();
    public data: any;
    constructor(private httpClientService: HttpClientService) {
        this.loadData();
    }
    loadData() {
        this.httpClientService.get('/api/role')
            .subscribe((res:any) => {
                const result= res.map((s:any)=>{return{
                    id:s.id,
                    text:s.name
                }});
                this._dataSubject.next(result)
                this.data = res;
            });
    }
}