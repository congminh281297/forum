export class PaperConfig {
	public defaults: any = {
        pageSizeOptions:[5, 10, 25, 50, 100],
        pageSize:10,
	};

	public get configs(): any {
		return this.defaults;
	}
}
