import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';

export interface ToggleOptions {
	target?: string;
	targetState?: string;
	toggleState?: string;
}

@Directive({
	selector: '[saToggle]',
	exportAs: 'saToggle'
})
export class ToggleDirective implements AfterViewInit {
	@Input() options?: ToggleOptions;
	toggle: any;
	constructor(private el: ElementRef) { }

	ngAfterViewInit(): void {
		this.toggle = new SAToggle(this.el.nativeElement, SAUtil.getBody(), this.options);
	}
}
