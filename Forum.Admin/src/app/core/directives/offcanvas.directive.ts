import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';

export interface OffcanvasOptions {
	baseClass: string;
	overlay?: boolean;
	closeBy: string;
	toggleBy?: any;
	placement?: string;

}

@Directive({
	selector: '[saOffcanvas]',
	exportAs: 'saOffcanvas',
})
export class OffcanvasDirective implements AfterViewInit {
	@Input() options?: OffcanvasOptions;
	private offcanvas: any;

	constructor(private el: ElementRef) { }

	ngAfterViewInit(): void {
		setTimeout(() => {
			this.offcanvas = new SAOffcanvas(this.el.nativeElement, this.options);
		});
	}

	getOffcanvas() {
		return this.offcanvas;
	}
	close() {
		if (this.offcanvas)
			this.offcanvas.close()
	}
}
