import { Directive, ElementRef, Input, SimpleChanges } from '@angular/core';
@Directive({
    selector: "[sa-gender-icon]",
})
export class SAGenderIconDirective {
    @Input() Gender: any;
    constructor(private el: ElementRef) {

    }
    ngOnChanges(changes: SimpleChanges): void {
        let icon: HTMLElement = this.el.nativeElement;
        if (this.Gender.toString() == '1') {
            icon.classList.add('fas', 'fa-mars', 'text-primary');
        } else {
            if (this.Gender.toString() == '2') {
                icon.classList.add('fas', 'fa-venus', 'text-danger');
            } else {
                icon.classList.add('fas', 'fa-venus-mars');
            }
        }

        icon.title = `Giới tính ${this.Gender.toString() == '1' ? 'nam' : this.Gender.toString() == '2' ? 'nữ' : 'khác'}`;
    }
}