import { Directive, ElementRef, Input, HostListener } from '@angular/core';
@Directive({
    selector: "[btnLoader]",
})
export class BtnLoaderDirective {
    private loaderElement = false;
    private btnElement: HTMLElement;
    @Input('loader')
    set loader(value: boolean) {
        if (value && this.loaderElement) {
            this.btnElement.classList.add('spinner', 'spinner-white', 'spinner-center');
            setTimeout(() => {
                this.btnElement.setAttribute('disabled', 'true');
            });
        } else {
            this.btnElement.classList.remove('spinner', 'spinner-white', 'spinner-center');
            setTimeout(() => {
                this.btnElement.removeAttribute('disabled');
            });
        }
    }

    constructor(private el: ElementRef) {
        this.btnElement = this.el.nativeElement;
    }

    ngAfterViewInit(): void {
        this.loaderElement = !1;
    }

    @HostListener('click', ['$event']) onClick(_: any) {
        this.loaderElement = !0;
        setTimeout(() => {
            this.loaderElement = !1;
        });
    }
}