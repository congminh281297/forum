import { Directive, HostListener } from "@angular/core";

@Directive({
    selector: '[saDateMask]',
    exportAs: 'saDateMask',
})
export class SADateMaskDirective {

    private div = '/';
    private arrayvalue: string[] = [];
    private defaulvalue = '';
    constructor() {
        let vrr = 'ddmmyyyy'.match(/.{2}/g);
        vrr?.forEach((el, i) => {
            if (!this.arrayvalue.find(s => s == el)) {
                this.arrayvalue.push(el);
            } else {
                this.arrayvalue.pop();
                this.arrayvalue.push(el + el);
            }
        });
        this.defaulvalue = this.arrayvalue.join(this.div).toUpperCase();
        //this.el.nativeElement.attributes.maxlength = this.defaulvalue.length;
    }
    @HostListener('keydown', ['$event'])
    onkeydown(e: any) {
        let ctrl = e.ctrlKey ? e.ctrlKey : ((e.keyCode === 17) ? true : false);
        if (e.keyCode == 9) {
            return;
        }
        if (e.keyCode == 39 || e.keyCode == 37 || (ctrl && e.keyCode == 86) || (ctrl && e.keyCode == 67) || (ctrl && e.keyCode == 65)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
            && (e.keyCode < 96 || e.keyCode > 105)
            && !([46, 8, 9, 27, 13].find(s => s == e.keyCode))) {
            e.preventDefault();
        }
        if (e.keyCode == 8) {
            if (e.target.value.endsWith(this.div)) {
                e.target.value = (e.target.value.replace(new RegExp(this.div + '$'), ''));
                e.preventDefault();
            }
        } else {
            if (e.target.value.length !== this.defaulvalue.length) {
                if (e.target.value.length == this.arrayvalue[0].length) {
                    e.target.value = (e.target.value + this.div);
                } else if (e.target.value.length == (this.arrayvalue[0].length + this.arrayvalue[1].length + this.div.length)) {
                    e.target.value = (e.target.value + this.div);
                }
            } else
                e.preventDefault();
        }
    }
}