import { Directive, ElementRef, HostListener, Input } from '@angular/core';
@Directive({
  selector: '[sa-textarea-height]',
})
export class SATextareaHeightDirective {
  @Input() maxHeight = 250;
  private minHeight = 38;
  line = 19.4;
  py = 16;
  @HostListener('keyup', ['$event.target'])
  onKeyup(event: any) {
    this.resize();
  }

  constructor(private element: ElementRef) { }
  ngAfterViewInit(): void {
    if (this.element.nativeElement.value == null || this.element.nativeElement.value.trim().toString() == "")
      return;
    const row = this.element.nativeElement.getAttribute('rows') || 1;
    if (row > 1)
      this.minHeight = (row * this.line) + this.py;
    this.resize();
  }
  resize() {
    this.element.nativeElement.style.height = 'auto';
    const h = this.element.nativeElement.scrollHeight + 2;
    this.element.nativeElement.style.height = `${h < this.minHeight ? this.minHeight : h < this.maxHeight ? h : this.maxHeight}px`;
  }
}
