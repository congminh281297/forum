export class SpecialistConstants {
    public static Specialist = [
        {
            id: '1',
            text: 'Tuần hoàn',
        },
        {
            id: '2',
            text: 'Hô hấp',
        },
        {
            id: '3',
            text: 'Tiêu hóa',
        },
        {
            id: '4',
            text: 'Thận tiết niệu',
        },
        {
            id: '5',
            text: 'Cơ-Xương-Khớp',
        },
        {
            id: '6',
            text: 'Thần kinh',
        },
        {
            id: '7',
            text: 'Tâm thần',
        },
        {
            id: '8',
            text: 'Ngoại khoa',
        },
        {
            id: '8',
            text: 'Sản phụ khoa',
        },
        {
            id: '10',
            text: 'Mắt',
        },
        {
            id: '11',
            text: 'Tai-Mũi-Họng',
        },
        {
            id: '12',
            text: 'Răng-Hàm-Mặt',
        },
        {
            id: '13',
            text: 'Da liễu',
        },
        {
            id: '14',
            text: 'Thể lực',
        },
    ];
}