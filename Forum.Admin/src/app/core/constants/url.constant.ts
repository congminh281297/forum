export class UrlConstants {
  static readonly HOME = '/tong-quan';
  static readonly LOGIN = '/login';
  static readonly FORBIDDEN = '/forbidden';
}