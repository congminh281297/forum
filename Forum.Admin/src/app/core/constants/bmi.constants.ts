export class BMIConstants {
    public static male = [
        {
            heightFrom: 164,
            heightTo: 1000,
            weightFrom: 50,
            weightTo: 1000,
            value: 1,
        },
        {
            heightFrom: 158,
            heightTo: 162,
            weightFrom: 47,
            weightTo: 49,
            value: 2,
        },
        {
            heightFrom: 154,
            heightTo: 157,
            weightFrom: 45,
            weightTo: 46,
            value: 3,
        },
        {
            heightFrom: 150,
            heightTo: 153,
            weightFrom: 41,
            weightTo: 44,
            value: 4,
        },
        {
            heightFrom: 0,
            heightTo: 149,
            weightFrom: 0,
            weightTo: 40,
            value: 5,
        }
    ];

    public static famale = [
        {
            heightFrom: 156,
            heightTo: 1000,
            weightFrom: 45,
            weightTo: 1000,
            value: 1,
        },
        {
            heightFrom: 151,
            heightTo: 154,
            weightFrom: 43,
            weightTo: 44,
            value: 2,
        },
        {
            heightFrom: 147,
            heightTo: 150,
            weightFrom: 40,
            weightTo: 42,
            value: 3,
        },
        {
            heightFrom: 143,
            heightTo: 146,
            weightFrom: 38,
            weightTo: 39,
            value: 4,
        },
        {
            heightFrom: 0,
            heightTo: 143,
            weightFrom: 0,
            weightTo: 37,
            value: 5,
        }
    ];
}