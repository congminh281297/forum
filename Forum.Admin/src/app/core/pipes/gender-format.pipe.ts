import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: "genderFormat"
})
export class GenderFormatPipe implements PipeTransform {
    transform(value: number): string {
        let str = 'Khác';
        switch (value) {
            case 2||'2':
                str = 'Nữ';
                break;
            case 1||'1':
                str = 'Nam';
                break;
            default:
                break;
        }

        return str;
    }
}