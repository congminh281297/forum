import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: "numberFormat"
})
export class NumberFormat implements PipeTransform {
    /** Format số theo định dạng
     * @number dãy số
     * @symbol đơn vị tính nếu có
     * @places độ dài số thập phân
     * @thousand phân cách hàng nghìn mặc định là .
     * @decimal phân cách số thập phân mặc định ,
     */
    transform(number: any, symbol: string = '', places: any = 4, thousand: string = '.', decimal: string = ','): string {
        symbol = symbol || ''; places = places || 4; thousand = thousand || '.'; decimal = decimal || ',';
        if (!(number) || number == 0) { return ''; }
        number = '' + number;
        // if (0 < number && number < 1)
        //     return number;        
        const so = parseFloat((parseFloat(number) || 0).toFixed(places));
        const str_so = so.toString();
        const str_songuyen = str_so.substr(0, (str_so.indexOf('.') > -1 ? str_so.indexOf('.') : str_so.length));
        let sole = decimal + (str_so.substr(str_so.indexOf('.') > -1 ? str_so.indexOf('.') + 1 : str_so.length));
        if (parseInt(sole.substr(1), undefined) === 0 || sole.substr(1) === '')
            sole = '';
        else
            do {
                if (sole.substr(sole.length - 1) === '0')
                    sole = sole.substr(0, sole.length - 1);
                else
                    break;
            }
            while (true);

        // Kiểm tra nếu là số thập phân < 1
        if (str_songuyen === '0' && sole)
            return str_songuyen + sole + symbol;
        if (str_songuyen === '0' && !sole)
            return str_songuyen + symbol;

        let num = str_songuyen.replace(/^0*/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, thousand);
        if (!num) { num = ''; }
        return num + sole + symbol;
    }
}