import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: "nameFormat"
})
export class NameFormatPipe implements PipeTransform {

    transform(code: string, name: string): string {
        let newStr = `<strong class="text-danger">${code}</strong> - ${name}`;
        return newStr;
    }
}