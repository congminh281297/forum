﻿import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "titlecase"
})
export class TitleCaseFormat implements PipeTransform {

    transform(str: string): string {
        if (str && str != null) {
            let _str = str.toLowerCase().split(' ').map((word) => {
                return word.replace(word[0], word[0]?.toUpperCase());
            });
            return _str.join(' ');
        }
        else return '';
    }
}