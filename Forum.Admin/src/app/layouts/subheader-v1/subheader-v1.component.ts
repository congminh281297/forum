import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { LayoutConfigService, SubheaderService } from 'src/app/core/services';
import { Breadcrumb } from 'src/app/core/services/subheader.service';
import { Subscription } from 'rxjs';
import { NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

@Component({
	selector: "sa-subheader-v1",
	templateUrl: "./subheader-v1.component.html",
	styleUrls: ['./subheader-v1.component.scss']
})
export class SubHeaderV1Component implements OnInit {

	@Input() fluid?: boolean;
	@Input() clear?: boolean;

	@ViewChild("dateDrop") drop?: NgbDropdown;

	today: number = Date.now();
	title = '';
	desc = '';
	breadcrumbs: Breadcrumb[] = [];
	private subscriptions: Subscription[] = [];
	showCalenda: boolean = false;
	activeMenu: string = 'today';
	constructor(public subheaderService: SubheaderService,
		private calendar: NgbCalendar,
		public formatter: NgbDateParserFormatter) {
		this.fromDate = calendar.getToday();
		this.toDate = calendar.getToday();
		this.emiter();
	}

	ngOnInit(): void {

	}
	ngAfterViewInit(): void {
		this.subscriptions.push(this.subheaderService.title$.subscribe((bt: any) => {
			if (bt) {
				Promise.resolve(null).then(() => {
					this.title = bt[0];
				});
			}
		}));

		this.subscriptions.push(this.subheaderService.breadcrumbs$.subscribe(bc => {
			Promise.resolve(null).then(() => {
				this.breadcrumbs = bc;
			});
		}));
	}
	ngOnDestroy(): void {
		this.subscriptions.forEach(sb => sb.unsubscribe());
		this.fromDate = this.calendar.getToday();
		this.toDate = this.calendar.getToday();
		this.emiter();
	}

	titleDrop(): string {
		if (this.fromDate && this.toDate) {
			if (this.fromDate?.equals(this.toDate))
				return 'Ngày ' + this.fromDate.day + '/' + this.fromDate.month;
			else
				return 'Từ ' + this.fromDate.day + '/' + this.fromDate.month + ' đến ' + this.toDate.day + '/' + this.toDate.month;
		}
		return 'Chọn ngày'
	}

	todayChange() {
		this.showCalenda = false;
		this.fromDate = this.calendar.getToday();
		this.toDate = this.calendar.getToday();
		this.activeMenu = 'today';
		this.emiter();
	}
	yesterdayChange() {
		this.showCalenda = false;
		this.fromDate = this.calendar.getPrev(this.calendar.getToday(), 'd', 1);
		this.toDate = this.calendar.getPrev(this.calendar.getToday(), 'd', 1);
		this.activeMenu = 'yesterday';
		this.emiter();
	}
	last7Change() {
		this.showCalenda = false;
		this.fromDate = this.calendar.getPrev(this.calendar.getToday(), 'd', 7);
		this.toDate = this.calendar.getToday();
		this.activeMenu = 'last7day';
		this.emiter();
	}
	last30Change() {
		this.showCalenda = false;
		this.fromDate = this.calendar.getPrev(this.calendar.getToday(), 'd', 30);
		this.toDate = this.calendar.getToday();
		this.activeMenu = 'last30day';
		this.emiter();
	}
	thisMonthChange() {
		this.showCalenda = false;
		const date = this.calendar.getToday();
		this.fromDate = new NgbDate(date.year, date.month, 1);
		this.toDate = this.calendar.getPrev(new NgbDate(date.year, date.month + 1, 1), 'd', 1);
		this.activeMenu = 'thismonth';
		this.emiter();
	}
	lastMonthChange() {
		this.showCalenda = false;
		const date = this.calendar.getToday();
		this.fromDate = new NgbDate(date.year, date.month - 1, 1);
		this.toDate = this.calendar.getPrev(new NgbDate(date.year, date.month, 1), 'd', 1);
		this.activeMenu = 'lastmonth';
		this.emiter();
	}
	customChange() {
		this.showCalenda = true;
		this.activeMenu = 'custom';
	}
	emiter() {
		if (this.drop)
			this.drop.close();
		this.subheaderService.dateChange.next({
			fromDate: this.dateToServer(this.fromDate),
			toDate: this.dateToServer(this.toDate),
		});
	}
	//#region Date Range

	hoveredDate: NgbDate | null = null;
	fromDate: NgbDate | null;
	toDate: NgbDate | null;
	onDateSelection(date: NgbDate) {
		if (!this.fromDate && !this.toDate) {
			this.fromDate = date;
		}
		else if (this.fromDate && !this.toDate && date && (date.after(this.fromDate) || date.equals(this.fromDate))) {
			this.toDate = date;
		}
		else {
			this.toDate = null;
			this.fromDate = date;
		}
	}

	isHovered(date: NgbDate) {
		return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
	}

	isInside(date: NgbDate) {
		return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
	}

	isRange(date: NgbDate) {
		return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
	}

	isStartRange(date: NgbDate) {
		return date.equals(this.fromDate);
	}

	isEndRange(date: NgbDate) {
		return this.toDate && date.equals(this.toDate);
	}

	isValid() {
		return (this.fromDate && this.fromDate != null) && (this.toDate && this.toDate != null);
	}

	validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
		const parsed = this.formatter.parse(input);
		return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
	}

	dateToServer(date: NgbDate | null) {
		if (!(date && date != null))
			return null;
		return moment(this.formatter.format(date), 'DD-MM-YYYY').format('YYYY-MM-DD');
	}
	strToDate(str: string | null): NgbDate {
		if (!str && str != '') {
			const date = moment(str, 'DD-MM-YYYY');
			return new NgbDate(date.year(), date.month(), date.date());
		}
		return this.calendar.getToday();
	}
	//#endregion

}
