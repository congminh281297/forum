import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import * as objectPath from 'object-path';
import { LayoutConfigService, MenuConfigService, PageConfigService } from 'src/app/core/services';
import { LayoutConfig } from 'src/app/core/common/layout.config';
import { MenuConfig } from 'src/app/core/common/menu.config';
import { PageConfig } from 'src/app/core/common/page.config';
import { HtmlClassService } from 'src/app/core/services/html-class.service';
import { OffcanvasOptions } from 'src/app/core/directives/offcanvas.directive';

@Component({
	selector: 'sa-base',
	templateUrl: './base.component.html',
	styleUrls: ['./base.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class BaseComponent implements OnInit, OnDestroy {

	menuCanvasOptions: OffcanvasOptions = {
		baseClass: 'aside',
		overlay: true,
		closeBy: "sa_aside_close_btn",
		toggleBy: {
			target: 'sa_aside_mobile_toggle',
			state: 'mobile-toggle-active'
		}
	};
	selfLayout?: string;
	asideDisplay?: boolean;
	asideSecondary?: boolean;
	subheaderDisplay?: boolean;
	desktopHeaderDisplay?: boolean;
	fitTop?: boolean;
	fluid?: boolean;

	private unsubscribe: Subscription[] = [];
	constructor(
		private layoutConfigService: LayoutConfigService,
		private menuConfigService: MenuConfigService,
		private pageConfigService: PageConfigService,
		public htmlClassService: HtmlClassService,
	) {

		this.loadRolesWithPermissions();

		// register configs by demos
		this.layoutConfigService.loadConfigs(new LayoutConfig().configs);
		this.menuConfigService.loadConfigs(new MenuConfig().configs);
		this.pageConfigService.loadConfigs(new PageConfig().configs);

		// // setup element classes
		this.htmlClassService.setConfig(this.layoutConfigService.getConfig());

		const subscr = this.layoutConfigService.onConfigUpdated$.subscribe(layoutConfig => {
			// reset body class based on global and page level layout config, refer to html-class.service.ts
			document.body.className = '';
			this.htmlClassService.setConfig(layoutConfig);
		});
		this.unsubscribe.push(subscr);
	}

	ngOnInit(): void {
		const config = this.layoutConfigService.getConfig();
		this.selfLayout = objectPath.get(config, 'self.layout');
		this.asideDisplay = objectPath.get(config, 'aside.self.display');
		this.subheaderDisplay = objectPath.get(config, 'subheader.display');
		this.desktopHeaderDisplay = objectPath.get(config, 'header.self.fixed.desktop');
		this.fitTop = objectPath.get(config, 'content.fit-top');
		this.fluid = objectPath.get(config, 'content.width') === 'fluid';

		// let the layout type change
		const subscr = this.layoutConfigService.onConfigUpdated$.subscribe(cfg => {
			setTimeout(() => {
				this.selfLayout = objectPath.get(cfg, 'self.layout');
			});
		});
		this.unsubscribe.push(subscr);
	}


	ngOnDestroy(): void {
		this.unsubscribe.forEach(sb => sb.unsubscribe());
	}

	loadRolesWithPermissions() {
		// this.currentUserPermissions$ = this.store.pipe(select(currentUserPermissions));
		// const subscr = this.currentUserPermissions$.subscribe(res => {
		// 	if (!res || res.length === 0) {
		// 		return;
		// 	}

		// 	this.permissionsService.flushPermissions();
		// 	res.forEach((pm: Permission) => this.permissionsService.addPermission(pm.name));
		// });
		// this.unsubscribe.push(subscr);
	}
}
