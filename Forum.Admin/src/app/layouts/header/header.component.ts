// Angular
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
	ActivatedRoute,
	NavigationCancel,
	NavigationEnd,
	NavigationStart,
	RouteConfigLoadEnd,
	RouteConfigLoadStart,
	Router
} from '@angular/router';
import * as objectPath from 'object-path';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { LayoutConfigService, LayoutRefService } from 'src/app/core/services';
import { HtmlClassService } from 'src/app/core/services/html-class.service';
@Component({
	selector: "sa-header",
	templateUrl: "./header.component.html"
})
export class HeaderComponent {

	menuHeaderDisplay?: boolean;
	fluid?: boolean;

	@ViewChild('saHeader', { static: true }) saHeader?: ElementRef;
	private isload: any;
	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private layoutRefService: LayoutRefService,
		private layoutConfigService: LayoutConfigService,
		public loader: LoadingBarService,
		public htmlClassService: HtmlClassService
	) {
		this.router.events.subscribe(event => {
			if (event instanceof NavigationStart) {
				const i = event.url.indexOf('?')
				if (i > 0) {
					if (this.router.url.indexOf(event.url.substr(0, i)))
						this.loader.start();
				}
				else
					this.loader.start();
			}
			if (event instanceof RouteConfigLoadStart) {
				this.loader.increment(35);
			}
			if (event instanceof RouteConfigLoadEnd) {
				this.loader.increment(75);
			}
			if (event instanceof NavigationEnd || event instanceof NavigationCancel) {
				this.loader.complete();
			}
		});
	}

	ngOnInit(): void {
		const config = this.layoutConfigService.getConfig();
		this.menuHeaderDisplay = objectPath.get(config, 'header.menu.self.display');
		this.fluid = objectPath.get(config, 'header.self.width') === 'fluid';
	}

	ngAfterViewInit(): void {
		this.layoutRefService.addElement('header', this.saHeader?.nativeElement);
	}
}
