import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LayoutConfigService } from 'src/app/core/services';
import * as objectPath from 'object-path';
import { NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject, ReplaySubject, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
    selector: "sa-subheader-wrapper",
    templateUrl: "./subheader-wrapper.component.html"
})
export class SubHeaderWrapperComponent {

    layout?: string;
    fluid?: boolean;
    clear?: boolean;

    constructor(private layoutConfigService: LayoutConfigService, private router: Router,) {
        const routing = () => {
            if (this.router.url.startsWith('/tong-quan'))
                this.layout = 'subheader-v1';
            else
                this.layout = '';
        }
        routing();
        this.router.events
            .pipe(filter((event: any) => event instanceof NavigationEnd))
            .subscribe(routing);
    }
    ngOnInit(): void {
        const config = this.layoutConfigService.getConfig();
        this.fluid = objectPath.get(config, 'footer.self.width') === 'fluid';
        this.clear = objectPath.get(config, 'subheader.clear');
    }
}
