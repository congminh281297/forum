import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScrollTopOptions } from 'src/app/core/directives/scroll-top.directive';
@Component({
    selector: "sa-scroll-top",
    templateUrl: "./scroll-top.component.html"
})
export class ScrollTopComponent implements  AfterViewInit {
    scrollTopOptions: ScrollTopOptions = {
		offset: 300,
		speed: 600
	};
    constructor() {
    }
    ngAfterViewInit() {
      
    }
    
}