import { NumberRange } from '../core/pipes/number-range.pipe';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import * as Layout from './index';

import * as Component from '../components';
import * as Directive from '../core/directives';
import { GenderFormatPipe } from '../core/pipes/gender-format.pipe';
import { NameFormatPipe } from '../core/pipes/name-format.pipe';
import { NumberFormat } from '../core/pipes/number-format.pipe';
import { SafeHtml } from '../core/pipes/safe-html.pipe';

@NgModule({
    declarations: [
        Layout.BaseComponent,
        Layout.HeaderMobileComponent,
        Layout.AsideLeftComponent,
        Layout.BrandComponent,
        Layout.HeaderComponent,
        Layout.MenuHorizontalComponent,
        Layout.TopBarComponent,
        Layout.NotificationComponent,
        Layout.QuickActionComponent,
        Layout.UserPanelComponent,
        Layout.QuickPanelComponent,
        Layout.ScrollTopComponent,
        Layout.FooterComponent,
        Layout.SubHeaderComponent,
        Layout.SubHeaderV1Component,
        Layout.SubHeaderWrapperComponent,
        Directive.ToggleDirective,
        Directive.OffcanvasDirective,
        Directive.MenuDirective,
        Directive.HeaderDirective,
        Directive.ScrollTopDirective,
        Directive.BtnLoaderDirective,
        Directive.SADateMaskDirective,
        Directive.SAGenderIconDirective,
        Directive.SATextareaHeightDirective,
        Directive.SANumberMaskDirective,
        Directive.ContentEditableFormDirective,
        Component.PaginatorComponent,
        Component.NgPaginationComponent,
        Component.SortIconComponent,
        Component.ConfirmDeleteComponent,
        Component.LoadingComponent,
        GenderFormatPipe,
        NumberFormat,
        SafeHtml,
        NameFormatPipe,
        NumberRange,
        Component.DateRangeComponent,
        Component.DateRangeFilterComponent,
        Component.DateBaoCaoComponent
    ],
    exports: [
        Directive.BtnLoaderDirective,
        Directive.SADateMaskDirective,
        Component.PaginatorComponent,
        Component.NgPaginationComponent,
        Component.SortIconComponent,
        Component.LoadingComponent,
        Directive.OffcanvasDirective,
        Directive.SAGenderIconDirective,
        Directive.SATextareaHeightDirective,
        Directive.SANumberMaskDirective,
        Directive.ContentEditableFormDirective,
        GenderFormatPipe,
        NumberFormat,
        SafeHtml,
        NameFormatPipe,
        Component.DateRangeComponent,
        Component.DateRangeFilterComponent,
        Component.DateBaoCaoComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        NgbProgressbarModule,
        PerfectScrollbarModule,
        NgbModule,
        NgSelectModule,
        InlineSVGModule,
    ],
    entryComponents: [
        Component.ConfirmDeleteComponent,
    ],
    providers: []
})
export class LayoutAdminModule { }