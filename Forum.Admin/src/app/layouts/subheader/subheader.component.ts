import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { LayoutConfigService, SubheaderService } from 'src/app/core/services';
import * as objectPath from 'object-path';
import { Breadcrumb } from 'src/app/core/services/subheader.service';
import { Subscription } from 'rxjs';
@Component({
	selector: "sa-subheader",
	templateUrl: "./subheader.component.html"
})
export class SubHeaderComponent implements OnInit {

	@Input() fluid?: boolean;
	@Input() clear?: boolean;

	today: number = Date.now();
	title = '';
	desc = '';
	breadcrumbs: Breadcrumb[] = [];

	private subscriptions: Subscription[] = [];
	constructor(public subheaderService: SubheaderService) {
	}

	ngOnInit(): void {

	}

	ngAfterViewInit(): void {
		this.subscriptions.push(this.subheaderService.title$.subscribe((bt: any) => {
			if (bt) {
				Promise.resolve(null).then(() => {
					this.title = bt;
					this.desc = bt.desc;
				});
			}
		}));

		this.subscriptions.push(this.subheaderService.breadcrumbs$.subscribe(bc => {
			Promise.resolve(null).then(() => {
				this.breadcrumbs = bc;
			});
		}));
	}

	ngOnDestroy(): void {
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}
}
