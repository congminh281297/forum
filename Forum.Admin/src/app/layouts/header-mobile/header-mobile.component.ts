import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ToggleOptions } from 'src/app/core/directives/toggle.directive';
import { LayoutConfigService } from 'src/app/core/services';
@Component({
	selector: "sa-header-mobile",
	templateUrl: "./header-mobile.component.html"
})
export class HeaderMobileComponent {

	// Public properties
	headerLogo?: string;
	asideDisplay?: boolean;
	toggleOptions: ToggleOptions = {
		target: 'body',
		targetState: "topbar-mobile-on",
		toggleState: "active"
	};

	toggleOptions1: ToggleOptions = {
		target: 'body',
		targetState: "sa_header_mobile_toggle",
		toggleState: "mobile-toggle-active"
	}
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private layoutConfigService: LayoutConfigService) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.headerLogo = this.layoutConfigService.getStickyLogo();
		this.asideDisplay = this.layoutConfigService.getConfig('aside.self.display');
	}
}
