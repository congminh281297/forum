import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UrlConstants } from 'src/app/core/constants/url.constant';
import { OffcanvasOptions } from 'src/app/core/directives/offcanvas.directive';
import { HttpClientService } from 'src/app/core/services';
import { LoginService } from 'src/app/core/services/login.service';

@Component({
    selector: "sa-user-panel",
    templateUrl: "./user-panel.component.html"
})
export class UserPanelComponent {
    offcanvasOptions: OffcanvasOptions = {
        overlay: true,
        baseClass: "offcanvas",
        placement: "right",
        closeBy: "sa_quick_user_close",
        toggleBy: "sa_quick_user_toggle"
    };

    public currentUser: any;
    constructor(
        private modalService: NgbModal,
        private httpClientService: HttpClientService,
        private router: Router,
        private loginService: LoginService) {
        this.currentUser = this.loginService.getLoggedInUser();
    }

    logout() {
        this.loginService.logout();
        this.router.navigate([UrlConstants.LOGIN], { queryParams: { returnUrl: this.router.url } });
    }
    close() {
        const el = document.getElementById('sa_quick_user_close');
        el?.click()
    }

    changePassword() {

    }

}
