import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	Renderer2,
	ViewChild
} from '@angular/core';
import { filter } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';
import * as objectPath from 'object-path';
import { HtmlClassService } from 'src/app/core/services/html-class.service';
import { HttpClientService, LayoutConfigService, MenuAsideService } from 'src/app/core/services';
import { MenuOptions } from 'src/app/core/directives/menu.directive';
import { UtilityService } from 'src/app/core/services/utility.service';
@Component({
	selector: "sa-aside-left",
	templateUrl: "./aside-left.component.html",
	styleUrls: ['./aside-left.component.scss'],
})
export class AsideLeftComponent {
	@ViewChild('asideMenu', { static: true }) asideMenu?: ElementRef;

	currentRouteUrl = '';
	insideTm: any;
	outsideTm: any;
	menuOptions: MenuOptions = {
		scroll: null,
		submenu: {
			desktop: {
				default: 'dropdown',
			},
			tablet: 'accordion',
			mobile: 'accordion'
		},
		accordion: {
			expandAll: false
		}
	};

	public menus: any[] = [];

	constructor(
		public htmlClassService: HtmlClassService,
		public menuAsideService: MenuAsideService,
		public layoutConfigService: LayoutConfigService,
		private router: Router,
		private render: Renderer2,
		private cdr: ChangeDetectorRef,
		private utilityService: UtilityService,
		private httpClientService: HttpClientService) {
	}

	ngAfterViewInit(): void {

	}

	async ngOnInit() {
		await this.getNavigationMenus();
		this.currentRouteUrl = this.router.url.split(/[?#]/)[0];
		this.router.events
			.pipe(filter(event => event instanceof NavigationEnd))
			.subscribe(event => {
				this.currentRouteUrl = this.router.url.split(/[?#]/)[0];
				this.cdr.markForCheck();
			});

		const config = this.layoutConfigService.getConfig();

		if (objectPath.get(config, 'aside.menu.dropdown')) {
			this.render.setAttribute(this.asideMenu?.nativeElement, 'data-samenu-dropdown', '1');
			this.render.setAttribute(this.asideMenu?.nativeElement, 'data-samenu-dropdown-timeout', objectPath.get(config, 'aside.menu.submenu.dropdown.hover-timeout'));
		}
	}

	isMenuItemIsActive(item: any): boolean {
		if (item.submenu && item.submenu.length > 0) {
			return this.isMenuRootItemIsActive(item);
		}
		if (!item.page) {
			return false;
		}
		return this.currentRouteUrl.indexOf(item.page) !== -1;
	}

	isMenuRootItemIsActive(item: any): boolean {
		let result = false;
		for (const subItem of item.submenu) {
			result = this.isMenuItemIsActive(subItem);
			if (result) {
				return true;
			}
		}
		return false;
	}

	mouseEnter(e: Event) {
		if (document.body.classList.contains('aside-fixed')) {
			if (this.outsideTm) {
				clearTimeout(this.outsideTm);
				this.outsideTm = null;
			}

			this.insideTm = setTimeout(() => {
				if (document.body.classList.contains('aside-minimize') && SAUtil.isInResponsiveRange('desktop')) {
					this.render.removeClass(document.body, 'aside-minimize');
					this.render.addClass(document.body, 'aside-minimize-hover');
				}
			}, 50);
		}
	}

	mouseLeave(e: Event) {
		if (document.body.classList.contains('aside-fixed')) {
			if (this.insideTm) {
				clearTimeout(this.insideTm);
				this.insideTm = null;
			}

			this.outsideTm = setTimeout(() => {
				if (document.body.classList.contains('aside-minimize-hover') && SAUtil.isInResponsiveRange('desktop')) {
					this.render.removeClass(document.body, 'aside-minimize-hover');
					this.render.addClass(document.body, 'aside-minimize');
				}
			}, 100);
		}
	}
	getItemCssClasses(item: any): string {
		let classes = 'menu-item';
		if (objectPath.get(item, 'submenu')) {
			classes += ' menu-item-submenu';
		}
		if (!(item.submenu || item.submenu.length == 0) && this.isMenuItemIsActive(item)) {
			classes += ' menu-item-active';
		}
		if (item.submenu && item.submenu.length > 0 && this.isMenuItemIsActive(item)) {
			classes += ' menu-item-open';
		}
		return classes;
	}

	disableScroll() {
		return this.layoutConfigService.getConfig('aside.menu.dropdown') || false;
	}

	async getNavigationMenus() {
		await this.httpClientService.get('/api/navigationMenu/getByCurrentUser').toPromise()
			.then((res: any) => {
				this.menus = this.utilityService.build_tree_data(res, null);
        console.log(this.menus);
			});
	}
}
