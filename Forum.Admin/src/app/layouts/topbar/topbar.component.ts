import { Component } from '@angular/core';
import { LoginService } from 'src/app/core/services/login.service';
//import { HeadeMenuService } from 'src/app/_services/header-menu.service';
@Component({
    selector: "topbar",
    templateUrl: "./topbar.component.html"
})
export class TopBarComponent {
    public currentUser: any;
    constructor(private loginService: LoginService) {
        this.currentUser = this.loginService.getLoggedInUser();
    }

    getLetter(name: string) {
        return name.split(' ').map(s => s[0]);
    }
}
