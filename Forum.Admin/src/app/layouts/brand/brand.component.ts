import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ToggleOptions } from 'src/app/core/directives/toggle.directive';
import { LayoutConfigService } from 'src/app/core/services';
import { HtmlClassService } from 'src/app/core/services/html-class.service';
@Component({
	selector: "sa-brand",
	templateUrl: "./brand.component.html"
})
export class BrandComponent implements OnInit {

	headerLogo?: string;
	headerStickyLogo?: string;
	toggleOptions: ToggleOptions = {
		target: 'body',
		targetState: "aside-minimize",
		toggleState: "active"
	};
	constructor(private layoutConfigService: LayoutConfigService,
		public htmlClassService: HtmlClassService) {
	}
	ngOnInit(): void {
		this.headerLogo = this.layoutConfigService.getLogo();
		this.headerStickyLogo = this.layoutConfigService.getStickyLogo();
	}
}
