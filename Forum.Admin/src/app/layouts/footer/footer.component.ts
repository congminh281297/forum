import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LayoutConfigService } from 'src/app/core/services';
import * as objectPath from 'object-path';
import { loadingModel } from 'src/app/core/services/http-client.service';
@Component({
    selector: "sa-footer",
    templateUrl: "./footer.component.html"
})
export class FooterComponent {
    fluid?: boolean;
    loading: loadingModel = new loadingModel()
    constructor(private layoutConfigService: LayoutConfigService) {
    }
    ngOnInit(): void {
        const config = this.layoutConfigService.getConfig();
        // footer width fluid
        this.fluid = objectPath.get(config, 'footer.self.width') === 'fluid';
    }
}
