import { Component, } from '@angular/core';
import { LoginService } from 'src/app/core/services';
@Component({
	selector: "sa-menu-horizontal",
	templateUrl: "./menu-horizontal.component.html"
})
export class MenuHorizontalComponent {
	currentUser:any;
	constructor( private loginService: LoginService) {
        this.currentUser = this.loginService.getLoggedInUser();
	}
}
