import { Component, OnInit, AfterViewInit } from '@angular/core';
import { OffcanvasOptions } from 'src/app/core/directives/offcanvas.directive';
@Component({
    selector: "sa-quick-panel",
    templateUrl: "./quick-panel.component.html"
})
export class QuickPanelComponent{
  
    offcanvasOptions: OffcanvasOptions = {
		overlay: true,
        baseClass: "offcanvas",
        placement: "right",
        closeBy: "sa_quick_panel_close",
        toggleBy: "sa_quick_panel_toggle"
	};
    constructor() {
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
      
    }
}
