import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PaginatorState } from 'src/app/core/models/paginator.model';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html'
})
export class PaginatorComponent implements OnInit {
  @Input() paginator!: PaginatorState;
  @Input() isLoading?: boolean;
  @Output() paginate: EventEmitter<PaginatorState> = new EventEmitter();
  pageSizes?: number[] = [];

  constructor() { }

  ngOnInit(): void {
    this.pageSizes = this.paginator?.pageSizes;
  }

  pageChange(num: number) {
    this.paginator!.page = num;
    this.paginate.emit(this.paginator);
  }

  sizeChange() {
    this.paginator!.pageSize = + this.paginator!.pageSize;
    this.paginator!.page = 1;
    this.paginate.emit(this.paginator);
  }
}
