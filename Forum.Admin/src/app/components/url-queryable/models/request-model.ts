export class PaginationBaseFilterModel {
    page?: number;
    pageSize?: number;
}

export class BaseRequestModel extends PaginationBaseFilterModel { }
