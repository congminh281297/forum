import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
    template: ''
})
export abstract class BaseUrlQueryableComponent implements OnInit {
    abstract requestModel: any;

    constructor(protected router: Router,
        protected activatedRoute: ActivatedRoute) {

    }

    async ngOnInit() {
        this.activatedRoute.queryParamMap.subscribe(async (queryParams: any) => {
            this.convertUrlParamsToRequestModel(queryParams);
            await this.onUrlUpdated();
        });

        await this.ngOnComponentInit();
    }

    protected abstract onUrlUpdated(): any;
    protected abstract ngOnComponentInit(): any;

    /**
     * call this method to force the URL updated
     */
    async updateUrlParameters() {
        // we need to do modifications here for updating the query url
        const queryParams = this.convertRequestModelToUrlParams(this.requestModel);

        // then update url by calling this method
        await this.router.navigate(
            [],
            {
                relativeTo: this.activatedRoute,
                queryParams,
                queryParamsHandling: 'merge'
            });
    }

    /**
     * Convert the JS object into URL parameters
     * i.e: {
     *     sortColumn = "id",
     *     sortDirection = "asc"
     * } ==> ?sortColumn=id&sortDirection=asc
     * @param requestModel
     */
    protected convertRequestModelToUrlParams(requestModel: any) {
        return requestModel;
    }

    /**
     * Convert the URL parameters into JS object that we can use in the logic
     * i.e: ?sortColumn=id&sortDirection=asc => {
     *     sortColumn = "id",
     *     sortDirection = "asc"
     * }
     * @param queryParams
     */
    protected abstract convertUrlParamsToRequestModel(queryParams: any): any;


    protected checkFormEdit() {
        return this.router.url.startsWith('/quan-ly-danh-muc/');
    }
}
