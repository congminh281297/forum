import { Component, Input, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { DataConstants } from "src/app/core/constants/data.constants";
import { DateTimeService } from "src/app/core/services/datetime.service";
import * as moment from "moment";

@Component({
    selector: 'sa-date-bao-cao',
    templateUrl: './sa-date-bao-cao.component.html',
})
export class DateBaoCaoComponent implements OnInit {
    @Input() fromDateControl: any;
    @Input() toDateControl: any;
    @Input() timeTypeControl: any;
    @Input() customClass: any;
    dataConst = DataConstants;

    type = [
        { id: 1, text: 'Ngày' },
        { id: 2, text: 'Tháng' },
        { id: 3, text: 'Quý' },
        { id: 4, text: 'Năm' },
    ];

    date = new Date();
    timeGroup = new FormGroup({
        month: new FormControl(null),
        quarter: new FormControl(null),
        year: new FormControl(null)
    });

    constructor(private dateTimeService: DateTimeService) { }

    ngOnInit() {
        if (this.timeTypeControl.value != 1) {
            const from = this.fromDateControl.value;
            this.timeGroup.controls["month"].setValue(moment(from).month() + 1);
            this.timeGroup.controls["quarter"].setValue(moment(from).quarter());
            this.timeGroup.controls["year"].setValue(moment(from).year());
        }
    }

    onChange() {
        this.initial();
        const value = this.timeTypeControl.value;
        const group = this.timeGroup.value;
        let start;
        let end
        switch (value) {
            case 1:
                this.setValue(this.dateTimeService.formatdate_toserve(new Date(this.date.getFullYear(), this.date.getMonth(), 1)), this.dateTimeService.formatdate_toserve(this.date))
                break;
            case 2:
                start = this.dateTimeService.formatdate_toserve(moment().month(group.month - 1).startOf('month').toDate());
                end = this.dateTimeService.formatdate_toserve(moment().month(group.month - 1).endOf('month').toDate());
                this.setValue(start, end);
                break;
            case 3:
                start = this.dateTimeService.formatdate_toserve(moment().quarter(group.quarter).startOf('quarter').toDate());
                end = this.dateTimeService.formatdate_toserve(moment().quarter(group.quarter).endOf('quarter').toDate());
                this.setValue(start, end);
                break;
            case 4:
                start = this.dateTimeService.formatdate_toserve(moment().year(group.year).startOf('year').toDate());
                end = this.dateTimeService.formatdate_toserve(moment().year(group.year).endOf('year').toDate());
                this.setValue(start, end);
                break;
        }
    }

    initial() {
        this.timeGroup.controls["month"].setValue(moment().month() + 1);
        this.timeGroup.controls["quarter"].setValue(moment().quarter());
        this.timeGroup.controls["year"].setValue(moment().year());
    }

    setValue(from: any, to: any) {
        this.fromDateControl.setValue(from);
        this.toDateControl.setValue(to);
    }

    dateChange = (event: any) => {
        this.setValue(event.fromDate, event.toDate)
    }

    monthChange() {
        const group = this.timeGroup.value;
        const start = this.dateTimeService.formatdate_toserve(moment(new Date(group.year, group.month - 1, 1)).month(group.month - 1).startOf('month').toDate());
        const end = this.dateTimeService.formatdate_toserve(moment(new Date(group.year, group.month - 1, 1)).month(group.month - 1).endOf('month').toDate());
        this.setValue(start, end);
    }
    quarterChange() {
        const group = this.timeGroup.value;
        const start = this.dateTimeService.formatdate_toserve(moment(new Date(group.year, group.month - 1, 1)).quarter(group.quarter).startOf('quarter').toDate());
        const end = this.dateTimeService.formatdate_toserve(moment(new Date(group.year, group.month - 1, 1)).quarter(group.quarter).endOf('quarter').toDate());
        this.setValue(start, end);
    }

    yearChange() {
        const group = this.timeGroup.value;
        let start;
        let end
        if (this.timeTypeControl.value == 2) {
            start = this.dateTimeService.formatdate_toserve(moment(new Date(group.year, group.month - 1, 1)).month(group.month - 1).startOf('month').toDate());
            end = this.dateTimeService.formatdate_toserve(moment(new Date(group.year, group.month - 1, 1)).month(group.month - 1).endOf('month').toDate());
        }
        if (this.timeTypeControl.value == 3) {
            start = this.dateTimeService.formatdate_toserve(moment(new Date(group.year, group.month - 1, 1)).quarter(group.quarter).startOf('quarter').toDate());
            end = this.dateTimeService.formatdate_toserve(moment(new Date(group.year, group.month - 1, 1)).quarter(group.quarter).endOf('quarter').toDate());
        }
        if (this.timeTypeControl.value == 4) {
            start = this.dateTimeService.formatdate_toserve(moment(new Date(group.year, group.month - 1, 1)).year(group.year).startOf('year').toDate());
            end = this.dateTimeService.formatdate_toserve(moment(new Date(group.year, group.month - 1, 1)).year(group.year).endOf('year').toDate());

        }
        this.setValue(start, end);
    }
}