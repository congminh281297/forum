import {
    AfterContentInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { NgbCalendar, NgbDate, NgbDateParserFormatter, NgbTimeAdapter } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ValidatorConstants } from 'src/app/core/constants/validator.constants';
import { ValidatorService } from 'src/app/core/services/validator.service';
import { CustomAdapter } from 'src/app/core/utils/date-picker.utils';
import { NgbTimeStringAdapter } from 'src/app/core/utils/time-picker.utils';

@Component({
    selector: 'sa-date-range',
    templateUrl: './sa-date-range.component.html',
    providers: [{ provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter }],
})
export class DateRangeComponent implements OnInit, AfterContentInit, OnChanges {

    @Input() fromDateControl: any;
    @Input() toDateControl: any;
    @Input() fromDateTitle: string = 'Từ ngày';
    @Input() toDateTitle: string = 'Đến ngày';
    @Input() customClass: any;

    /**Dùng để search */
    @Input() isSearch: any;

    @Output() dateChange = new EventEmitter();

    validatorConsts = ValidatorConstants;
    fromDate!: NgbDate | null;
    toDate!: NgbDate | null;
    hoveredDate: NgbDate | null = null;
    fromDateRequired: any;
    toDateRequired: any;
    validator = true;

    dateTypeConstant = 'DD/MM/YYYY';

    constructor(
        public validatorService: ValidatorService,
        private calendar: NgbCalendar,
        public formatter: NgbDateParserFormatter,
        public customAdapter: CustomAdapter,
        private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.setValue();
    }

    ngOnChanges() {
        this.setValue();
    }

    ngAfterContentInit() {
        if (this.fromDateControl.validator) {
            const fromDate = this.fromDateControl.validator({} as AbstractControl);
            if (fromDate && fromDate.required) {
                this.fromDateRequired = true;
            }
        }

        if (this.toDateControl.validator) {
            const toDate = this.toDateControl.validator({} as AbstractControl);
            if (toDate && toDate.required) {
                this.toDateRequired = true;
            }
        }
        this.cdr.detectChanges();
    }

    setValue() {
        var fromDate = this.customAdapter.fromModel(this.fromDateControl.value);
        var toDate = this.customAdapter.fromModel(this.toDateControl.value);
        if (fromDate)
            this.fromDate = new NgbDate(fromDate.year, fromDate.month, fromDate.day);
        if (toDate)
            this.toDate = new NgbDate(toDate.year, toDate.month, toDate.day);
    }

    saveData() {
        this.dateChange.emit({ fromDate: this.dateToServer(this.fromDate!), toDate: this.dateToServer(this.toDate!) });
    }

    // kiểm tra giá trị
    checkDateType(date: string, toDate: boolean) {
        if (!date)
            if (toDate)
                if (!this.toDateRequired)
                    return true;
                else
                    return false;
            else
                if (!this.fromDateRequired)
                    return true;
                else
                    return false;

        if (moment(date, this.dateTypeConstant, true).isValid())
            return true;
        else
            return false;
    }

    /**Kiểm tra ngày kết thúc phải lớn hơn ngày bắt đầu */
    checkMinDate() {
        if (moment(this.dateToServer(this.fromDate!)).isAfter(moment(this.dateToServer(this.toDate!))))
            return true;
        return false
    }

    /**Kiểm tra validator */
    checkValidator(input: string = '', toDate = true) {
        if (input ? !this.checkDateType(input, toDate) : false || !this.checkDateType(this.formatter.format(this.fromDate!), false)
            || !this.checkDateType(this.formatter.format(this.toDate!), true) || this.checkMinDate()) {
            return this.validator = false;
        }
        return this.validator = true;
    }

    //#region Date Range   
    onDateSelection(date: NgbDate) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        }
        else if (this.fromDate && !this.toDate && date && (date.after(this.fromDate) || date.equals(this.fromDate))) {
            this.toDate = date;
        }
        else {
            this.toDate = null;
            this.fromDate = date;
        }
    }

    isHovered(date: NgbDate) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    }

    isInside(date: NgbDate) {
        return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
    }

    isRange(date: NgbDate) {
        return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
    }

    isStartRange(date: NgbDate) {
        return date.equals(this.fromDate);
    }

    isEndRange(date: NgbDate) {
        return this.toDate && date.equals(this.toDate);
    }

    isValid() {
        return (this.fromDate && this.fromDate != null) && (this.toDate && this.toDate != null) && (this.toDate.after(this.fromDate) || this.toDate.equals(this.fromDate));
    }

    validateInput(currentValue: NgbDate | null, input: string, toDate: boolean, event: any) {
        if (event && (event.keyCode === 8 || event.keyCode === 46) && input && !this.isSearch) {
            return this.checkValidator(input, toDate);
        }

        const parsed = this.formatter.parse(input);
        var parsedDate = parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;

        if (!toDate) {
            if (this.fromDateRequired) {
                if (moment(input, this.dateTypeConstant, true).isValid()) {
                    this.dateChange.emit({ fromDate: this.dateToServer(parsedDate), toDate: this.dateToServer(this.toDate!) });
                    this.fromDate = parsedDate;
                    this.checkValidator();
                    this.validator = true;
                } else
                    this.validator = false;
            } else {
                if (input.length === 0) {
                    this.fromDate = null;
                    this.dateChange.emit({ fromDate: null, toDate: this.dateToServer(this.toDate!) });
                    return null;
                } else if (moment(input, this.dateTypeConstant, true).isValid()) {
                    this.dateChange.emit({ fromDate: this.dateToServer(parsedDate), toDate: this.dateToServer(this.toDate!) });
                    this.fromDate = parsedDate;
                    this.validator = true;
                }
                else
                    this.validator = false;
            }
        }

        else {
            if (this.toDateRequired) {
                if (moment(input, this.dateTypeConstant, true).isValid() && (this.isSearch ? !moment(this.dateToServer(this.fromDate!)).isAfter(moment(this.dateToServer(parsedDate))) : true)) {
                    this.dateChange.emit({ fromDate: this.dateToServer(this.fromDate!), toDate: this.dateToServer(parsedDate) });
                    this.toDate = parsedDate;
                    this.checkValidator();
                    this.validator = true;
                } else
                    this.validator = false;

            } else {
                if (input.length === 0) {
                    this.toDate = null;
                    this.dateChange.emit({ fromDate: this.dateToServer(this.fromDate!), toDate: null });
                    return null;
                } else if (moment(input, this.dateTypeConstant, true).isValid() && (this.isSearch ? !moment(this.dateToServer(this.fromDate!)).isAfter(moment(this.dateToServer(parsedDate))) : true)) {
                    this.dateChange.emit({ fromDate: this.dateToServer(this.fromDate!), toDate: this.dateToServer(parsedDate) });
                    this.toDate = parsedDate;
                    this.validator = true;
                }
                else
                    this.validator = false;
            }
        }
        return parsedDate;
    }

    dateToServer(date: NgbDate | null) {
        if (!(date && date != null))
            return null;
        return moment(this.formatter.format(date), this.dateTypeConstant).format('YYYY-MM-DD');
    }
    //#endregion

}