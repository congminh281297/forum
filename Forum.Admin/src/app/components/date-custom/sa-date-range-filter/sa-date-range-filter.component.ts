import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { NgbCalendar, NgbDate, NgbDateParserFormatter, NgbTimeAdapter } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ValidatorConstants } from 'src/app/core/constants/validator.constants';
import { ValidatorService } from 'src/app/core/services/validator.service';
import { CustomAdapter } from 'src/app/core/utils/date-picker.utils';
import { NgbTimeStringAdapter } from 'src/app/core/utils/time-picker.utils';

@Component({
    selector: 'sa-date-range-filter',
    templateUrl: './sa-date-range-filter.component.html',
    styles: [`
    .from-date .input-group-text {border-radius:0px!important;border-right-width: 0px; padding:0.6rem;cursor: default;}
    .to-date .form-control {border-radius:0px!important;}
    `],
    providers: [{ provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter }],
})
export class DateRangeFilterComponent implements OnInit, AfterViewInit, OnChanges {

    @Input() fromDateControl: any;
    @Input() toDateControl: any;


    @Output() dateChange = new EventEmitter();

    validatorConsts = ValidatorConstants;
    fromDate!: NgbDate | null;
    toDate!: NgbDate | null;
    hoveredDate: NgbDate | null = null;
    fromDateRequired: any;
    toDateRequired: any;

    constructor(
        public validatorService: ValidatorService,
        private calendar: NgbCalendar,
        public formatter: NgbDateParserFormatter,
        public customAdapter: CustomAdapter,
        private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.setValue();
    }

    ngOnChanges() {
        this.setValue();
    }

    ngAfterViewInit() {
        const fromDate = this.fromDateControl.validator({} as AbstractControl);
        if (fromDate && fromDate.required) {
            this.fromDateRequired = true;
        }
        const toDate = this.toDateControl.validator({} as AbstractControl);
        if (toDate && toDate.required) {
            this.toDateRequired = true;
        }
        this.cdr.detectChanges();
    }

    setValue() {

        var fromDate = this.customAdapter.fromModel(this.fromDateControl.value);
        var toDate = this.customAdapter.fromModel(this.toDateControl.value);
        if (fromDate)
            this.fromDate = new NgbDate(fromDate.year, fromDate.month, fromDate.day);
        if (toDate)
            this.toDate = new NgbDate(toDate.year, toDate.month, toDate.day);
    }

    setValueToControls() {
        this.fromDateControl.setValue(this.fromDate ? this.dateToServer(this.fromDate) : null);
        this.toDateControl.setValue(this.toDate ? this.dateToServer(this.toDate) : null);
    }

    saveData() {
        this.dateChange.emit({ fromDate: this.dateToServer(this.fromDate!), toDate: this.dateToServer(this.toDate!) });
    }

    // kiểm tra giá trị
    checkDateType(date: string, toDate: boolean, stop: boolean = false) {
        if (stop)
            return true;
        if (!date)
            if (toDate)
                if (!this.toDateRequired)
                    return true;
                else
                    return false;
            else
                if (!this.fromDateRequired)
                    return true;
                else
                    return false;

        if (moment(date, 'DD/MM/YYYY', true).isValid())
            return true;
        else
            return false;
    }

    /**Kiểm tra ngày kết thúc phải lớn hơn ngày bắt đầu */
    checkMinDate() {
        if (moment(this.dateToServer(this.fromDate!)).isAfter(moment(this.dateToServer(this.toDate!))))
            return true;
        return false
    }

    /**Kiểm tra validator */
    checkValidator() {
        if (!this.checkDateType(this.formatter.format(this.fromDate!), false) || !this.checkDateType(this.formatter.format(this.toDate!), true) || this.checkMinDate()) {
            return false;
        }
        return true;
    }

    //#region Date Range   
    onDateSelection(date: NgbDate) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        }
        else if (this.fromDate && !this.toDate && date && (date.after(this.fromDate) || date.equals(this.fromDate))) {
            this.toDate = date;
        }
        else {
            this.toDate = null;
            this.fromDate = date;
        }
        this.setValueToControls();
    }

    isHovered(date: NgbDate) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    }

    isInside(date: NgbDate) {
        return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
    }

    isRange(date: NgbDate) {
        return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
    }

    isStartRange(date: NgbDate) {
        return date.equals(this.fromDate);
    }

    isEndRange(date: NgbDate) {
        return this.toDate && date.equals(this.toDate);
    }

    isValid() {
        return (this.fromDate && this.fromDate != null) && (this.toDate && this.toDate != null) && (this.toDate.after(this.fromDate) || this.toDate.equals(this.fromDate));
    }

    validateInput(currentValue: NgbDate | null, input: string, toDate: boolean): NgbDate | null {
        const parsed = this.formatter.parse(input);
        var parsedDate = parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
        if (!toDate) {
            if (input.length === 0) {
                this.fromDate = null;
            } else if (moment(input, 'DD/MM/YYYY', true).isValid()) {
                this.fromDate = parsedDate;
            }
        }
        else {
            if (input.length === 0) {
                this.toDate = null;
            } else if (moment(input, 'DD/MM/YYYY', true).isValid()) {
                this.toDate = parsedDate;
            }
        }
        this.setValueToControls();
        return parsedDate;
    }

    dateToServer(date: NgbDate | null) {
        if (!(date && date != null))
            return null;
        return moment(this.formatter.format(date), 'DD-MM-YYYY').format('YYYY-MM-DD');
    }
    //#endregion

}