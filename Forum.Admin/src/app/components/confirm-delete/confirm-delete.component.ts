import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    templateUrl: "./confirm-delete.component.html"
})
export class ConfirmDeleteComponent {
    @Input() title?: string;
    @Input() question?: string;
    @Input() callback: any;
    bar: boolean = false;
    constructor(public modal: NgbActiveModal) {
    }
    close() {
        this.bar = true;
        if (this.callback)
            this.callback().then(() => {
                this.bar = false;
                this.modal.close();
            });
    }
}