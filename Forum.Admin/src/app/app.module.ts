import { LayoutAdminModule } from './layouts/layout-admin.module';
import { LayoutConfig } from './core/common/layout.config';
import { CustomAdapter, CustomDateParserFormatter, CustomDatepickerI18n } from './core/utils/date-picker.utils';
import { DialogService } from './core/services/dialog.service';
import { HttpClientService } from './core/services/http-client.service';
import { UtilityService } from './core/services/utility.service';
import { NotificationService } from './core/services/notification.service';
import { LoginService } from './core/services/login.service';
import { DMCoSoService } from './core/services/dmCoSo.Service';
import { ValidatorService } from './core/services/validator.service';
import { HtmlClassService } from './core/services/html-class.service';
import { MenuAsideService } from './core/services/menu-aside.service';
import { MenuHorizontalService } from './core/services/menu-horizontal.service';
import { SubheaderService } from './core/services/subheader.service';
import { SplashScreenService } from './core/services/splash-screen.service';
import { PageConfigService } from './core/services/page-config.service';
import { MenuConfigService } from './core/services/menu-config.service';
import { LayoutRefService } from './core/services/layout-ref.service';
import { AppConfigService } from './core/services/app-config.service';
import { LayoutConfigService } from './core/services/layout-config.service';
import { AuthGuard } from './core/guards/auth.guard';
import { NumberFormat } from './core/pipes/number-format.pipe';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbProgressbarModule, NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgxPermissionsModule } from 'ngx-permissions';
import { SplashScreenComponent } from './layouts/splash-screen/splash-screen.component';
import { Page403Component } from './pages/error/page-403/page-403.component';
import { Page404Component } from './pages/error/page-404/page-404.component';
import { IndexComponent } from './pages/index/index.component';
import { LoginComponent } from './pages/login/login.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  PerfectScrollbarConfigInterface,
  PERFECT_SCROLLBAR_CONFIG,
} from 'ngx-perfect-scrollbar';

export function initializeLayoutConfig(
  layoutConfigService: LayoutConfigService
) {
  return () => {
    if (layoutConfigService.getConfig() === null) {
      layoutConfigService.loadConfigs(new LayoutConfig().configs);
    }
  };
}

export function initializeApp(appConfigService: AppConfigService) {
  return () => appConfigService.load();
}

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  wheelSpeed: 0.5,
  swipeEasing: true,
  minScrollbarLength: 40,
  maxScrollbarLength: 300,
};

@NgModule({
  imports: [
    BrowserAnimationsModule,
    NgbModule,
    AppRoutingModule,
    HttpClientModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      timeOut: 3000,
      enableHtml: true,
    }),
    CommonModule,
    NgxPermissionsModule.forRoot(),
    NgbProgressbarModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    InlineSVGModule,
    LayoutAdminModule
  ],
  exports: [],
  declarations: [
    AppComponent,
    SplashScreenComponent,
    LoginComponent,
    Page404Component,
    Page403Component,
    IndexComponent,
  ],
  providers: [
    NumberFormat,
    TitleCasePipe,
    AuthGuard,
    LayoutConfigService,
    AppConfigService,
    LayoutRefService,
    MenuConfigService,
    PageConfigService,
    SplashScreenService,
    SubheaderService,
    MenuHorizontalService,
    MenuAsideService,
    HtmlClassService,
    ValidatorService,
    DMCoSoService,
    LoginService,
    NotificationService,
    UtilityService,
    HttpClientService,
    DialogService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeLayoutConfig,
      multi: true,
      deps: [LayoutConfigService],
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (loginService: LoginService) => () =>
        loginService.startRefreshTokenTimer(),
      multi: true,
      deps: [LoginService],
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (utilityService: UtilityService) => () =>
        utilityService.printConsoleWarning(),
      multi: true,
      deps: [UtilityService],
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter },
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
