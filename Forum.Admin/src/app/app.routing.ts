import { AppComponent } from './app.component';
import { AuthGuard } from './core/guards/auth.guard';
import { IndexComponent } from './pages/index/index.component';
import { LoginComponent } from './pages/login/login.component';
import { Page404Component } from './pages/error/page-404/page-404.component';
import { Page403Component } from './pages/error/page-403/page-403.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaseComponent } from './layouts/base/base.component';

const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },
      {
        path: 'index',
        component: IndexComponent,
        data: { title: 'Trang chủ' },
      },
      {
        path: 'system',
        loadChildren: () =>
          import('./pages/admin/administrator.module').then((m) => m.AdministratorModule),
      },
      {
        path: 'categories',
        loadChildren: () =>
          import('./pages/categories/categories.module').then((m) => m.CategoriesModule),
      },
      {
        path: 'features',
        loadChildren: () =>
          import('./pages/features/features.module').then((m) => m.FeaturesModule),
      },
    ],
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Đăng nhập',
    },
  },
  {
    path: 'not-found',
    component: Page404Component,
    data: {
      title: 'Không tìm thấy trang yêu cầu',
    },
  },
  {
    path: 'forbidden',
    component: Page403Component,
    data: {
      title: 'Bạn bị giới hạn quyền truy cập',
    },
  },
  {
    path: '**',
    redirectTo: 'not-found',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
