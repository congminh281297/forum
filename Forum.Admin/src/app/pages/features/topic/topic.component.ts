import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'topic',
    templateUrl: 'topic.component.html'
})


export class TopicComponent implements OnInit {
    constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

    ngOnInit() { }

    moveToEditTopic() {
        this.router.navigate(['edit/1233'], { relativeTo: this.activatedRoute });
    }
}