import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'

@Component({
    selector: 'edit-topic',
    templateUrl: 'edit-topic.component.html'
})

export class EditTopicComponent implements OnInit {
    id?: string;
    constructor(private location: Location, private router: Router, private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.id = this.activatedRoute.snapshot.params["id"];

    }

    back() {
        // let back = this.id ? ".." : "..";
        // this.router.navigate([back]);
        this.location.back();
    }
}