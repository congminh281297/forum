import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TopicComponent, FeaturesComponent, EditTopicComponent } from './features.index';

const routes: Routes = [
    {
        path: '', component: FeaturesComponent,
        children: [
            { path: 'topic', component: TopicComponent },
            { path: 'topic/edit', component: EditTopicComponent },
            { path: 'topic/edit/:id', component: EditTopicComponent },
        ]
    }
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [],
})

export class FeaturesRoutingModule { }