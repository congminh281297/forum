import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InlineSVGModule } from 'ng-inline-svg';
import * as Features from './features.index';
import { FeaturesRoutingModule } from './features.routing';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        InlineSVGModule,
        ReactiveFormsModule,
        FeaturesRoutingModule
    ],
    exports: [],
    declarations: [
        Features.FeaturesComponent,
        Features.TopicComponent,
        Features.EditTopicComponent,
    ],
    providers: [],
})
export class FeaturesModule { }
