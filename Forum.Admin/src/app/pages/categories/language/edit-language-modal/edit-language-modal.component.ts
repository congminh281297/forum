import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidatorConstants } from 'src/app/core/constants/validator.constants';
import { HttpClientService, loadingModel } from 'src/app/core/services/http-client.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ValidatorService } from 'src/app/core/services/validator.service';



@Component({
    selector: 'app-edit-language-modal',
    templateUrl: './edit-language-modal.component.html'
})
export class EditLanguageModalComponent implements OnInit {
    @Input() id: any;
    ValidatorConsts = ValidatorConstants;
    isLoading = new loadingModel();
    language?: any;
    formGroup!: FormGroup;

    EMPTY_language: any = {
        id: '',
        name: '',
        slug: '',
        isLocked: false
    };

    constructor(
        private notificationService: NotificationService,
        private httpClientService: HttpClientService,
        public validatorService: ValidatorService,
        public modal: NgbActiveModal) { }

    async ngOnInit() {
        await this.loadLanguage();
    }

    async loadLanguage() {
        if (!this.id) {
            this.language = this.EMPTY_language;
            this.loadForm();
        } else {
            await this.httpClientService.get(`/api/language/${this.id}`, this.isLoading).toPromise()
                .then(res => {
                    this.language = res;
                    this.loadForm();
                });
        }
    }

    loadForm() {
        this.formGroup = new FormGroup({
            name: new FormControl(this.language.name, [Validators.required]),
            slug: new FormControl(this.language.slug, [Validators.required]),
            isLocked: new FormControl(this.language.isLocked),
        });
    }

    async save() {
        if (this.formGroup.invalid) {
            this.formGroup.markAllAsTouched();
            this.notificationService.validator();
            return;
        }

        if (this.id) {
            await this.edit();
        } else {
            await this.create();
        }
    }

    async edit() {
        const formData = this.formGroup.value;
        await this.httpClientService.put(`/api/language/${this.id}`, formData, this.isLoading).toPromise()
            .then(() => {
                this.modal.close();
            });
    }

    async create() {
        const formData = this.formGroup.value;
        await this.httpClientService.post(`/api/language`, formData, this.isLoading).toPromise()
            .then(() => {
                this.modal.close();
            });
    }
}
