import { LanguageComponent } from './language/language.component';
import { CategoriesComponent } from './categories.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: CategoriesComponent,
        data: {
            title: 'Danh mục'
        },
        children: [
            {
                path: 'language',
                component: LanguageComponent,
                data: {
                    title: 'Ngôn ngữ'
                }
            }
        ]
    }
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [],
})

export class CategoriesRoutingModule { }