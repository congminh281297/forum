import { LayoutAdminModule } from './../../layouts/layout-admin.module';
import { CategoriesComponent } from './categories.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import * as Categories from './categories.index';
import { CategoriesRoutingModule } from './categories.routing';
import { InlineSVGModule } from 'ng-inline-svg';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        InlineSVGModule,
        ReactiveFormsModule,
        CategoriesRoutingModule,
        LayoutAdminModule
    ],
    exports: [],
    declarations: [
        CategoriesComponent,
        Categories.LanguageComponent,
        Categories.EditLanguageModalComponent
    ],
    providers: [],
})
export class CategoriesModule { }
