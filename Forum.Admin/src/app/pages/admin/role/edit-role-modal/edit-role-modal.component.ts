import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidatorConstants } from 'src/app/core/constants/validator.constants';
import { HttpClientService, loadingModel } from 'src/app/core/services/http-client.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ValidatorService } from 'src/app/core/services/validator.service';

const EMPTY_ROLE = {
    name: '',
    description: '',
    isLocked: false
};

@Component({
    selector: 'app-edit-role-modal',
    templateUrl: './edit-role-modal.component.html'
})
export class EditRoleModalComponent implements OnInit {
    @Input() id: any;
    ValidatorConsts = ValidatorConstants;
    isLoading = new loadingModel();
    role: any;
    formGroup!: FormGroup;
    constructor(
        private notificationService: NotificationService,
        private httpClientService: HttpClientService,
        public validatorService: ValidatorService,
        public modal: NgbActiveModal) { }

    async ngOnInit() {
        await this.loadRole();
    }

    async loadRole() {
        if (!this.id) {
            this.role = EMPTY_ROLE;
            this.loadForm();
        } else {
            await this.httpClientService.get(`/api/role/${this.id}`, this.isLoading).toPromise()
                .then(res => {
                    this.role = res;
                    this.loadForm();
                });
        }
    }

    loadForm() {
        this.formGroup = new FormGroup({
            name: new FormControl(this.role.name, [Validators.required]),
            description: new FormControl(this.role.description),
            isLocked: new FormControl(this.role.isLocked),
        });
    }

    async save() {
        if (this.formGroup.invalid) {
            this.formGroup.markAllAsTouched();
            this.notificationService.validator();
            return;
        }

        if (this.id) {
            await this.edit();
        } else {
            await this.create();
        }
    }

    async edit() {
        const formData = this.formGroup.value;
        await this.httpClientService.put(`/api/role/${this.id}`, formData, this.isLoading).toPromise()
            .then(() => {
                this.modal.close();
            });
    }

    async create() {
        const formData = this.formGroup.value;
        await this.httpClientService.post(`/api/role`, formData, this.isLoading).toPromise()
            .then(() => {
                this.modal.close();
            });
    }
}
