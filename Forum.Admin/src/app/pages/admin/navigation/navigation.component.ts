import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientService, loadingModel } from 'src/app/core/services/http-client.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { UtilityService } from 'src/app/core/services/utility.service';
import { EditNavigationModalComponent } from './edit-navigation-modal/edit-navigation-modal.component';

@Component({
    templateUrl: "./navigation.component.html",
    styleUrls: ["./navigation.component.scss"]
})
export class NavigationComponent implements OnInit {
    isLoading = new loadingModel();
    options = {};
    menusHierachy!: any[];
    @ViewChild('tree') tree: any;

    constructor(
        private notificationService: NotificationService,
        private modalService: NgbModal,
        private utilityService: UtilityService,
        private httpClientService: HttpClientService) {
    }

    async ngOnInit() {
        await this.loadData();
    }

    async loadData() {
        await this.httpClientService.get('/api/navigationMenu', this.isLoading).toPromise()
            .then((res: any) => {
                this.menusHierachy = this.utilityService.unflatten(res);
                setTimeout(() => {
                    this.tree.treeModel.expandAll();
                });
            });
    }

    edit(id: any) {
        const modalRef = this.modalService.open(EditNavigationModalComponent,
            {
                backdrop: 'static',
                keyboard: false
            });
        modalRef.componentInstance.id = id;
        modalRef.result.then(async () => {
            if (id) {
                this.notificationService.updateSuccess();
            } else {
                this.notificationService.createSuccess();
            }

            await this.loadData();
        }, () => { });
    }

    async moveDown(id: any) {
        await this.httpClientService.put(`/api/navigationMenu/moveDown/${id}`, this.isLoading).toPromise()
            .then(async () => {
                this.notificationService.updateSuccess();
                await this.loadData();
            });
    }

    async moveUp(id: any) {
        await this.httpClientService.put(`/api/navigationMenu/moveUp/${id}`, this.isLoading).toPromise()
            .then(async () => {
                this.notificationService.updateSuccess();
                await this.loadData();
            });
    }
}
