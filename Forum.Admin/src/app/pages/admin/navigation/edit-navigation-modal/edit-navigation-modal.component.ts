import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidatorConstants } from 'src/app/core/constants/validator.constants';
import { HttpClientService, loadingModel } from 'src/app/core/services/http-client.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ValidatorService } from 'src/app/core/services/validator.service';

const EMPTY_NAVIGATION = {
    name: '',
    code: '',
    url: '',
    icon: '',
    svg: '',
    root: false,
    bullet: 'dot',
    parentId: null,
    isSection: false,
    sortOrder: 0,
    isLocked: false,
    isLockClient: false
};

@Component({
    selector: 'app-edit-navigation-modal',
    templateUrl: './edit-navigation-modal.component.html'
})
export class EditNavigationModalComponent implements OnInit {
    @Input() id!: number;
    ValidatorConsts = ValidatorConstants;
    isLoading = new loadingModel();
    navigation: any;
    formGroup!: FormGroup;
    constructor(
        private notificationService: NotificationService,
        private httpClientService: HttpClientService,
        public validatorService: ValidatorService,
        public modal: NgbActiveModal) { }

    async ngOnInit() {
        await this.loadNavigation();
    }

    async loadNavigation() {
        if (!this.id) {
            this.navigation = EMPTY_NAVIGATION;
            this.loadForm();
        } else {
            await this.httpClientService.get(`/api/navigationMenu/${this.id}`, this.isLoading).toPromise()
                .then(res => {
                    this.navigation = res;
                    this.loadForm();
                });
        }
    }

    loadForm() {
        this.formGroup = new FormGroup({
            name: new FormControl(this.navigation.name, [Validators.required]),
            code: new FormControl(this.navigation.code, [Validators.required]),
            url: new FormControl(this.navigation.url),
            icon: new FormControl(this.navigation.icon),
            svg: new FormControl(this.navigation.svg),
            root: new FormControl(this.navigation.root),
            bullet: new FormControl(this.navigation.bullet),
            parentId: new FormControl(this.navigation.parentId),
            isSection: new FormControl(this.navigation.isSection),
            sortOrder: new FormControl(this.navigation.sortOrder),
            isLocked: new FormControl(this.navigation.isLocked),
            isLockClient: new FormControl(this.navigation.isLockClient),
        });
    }

    async save() {
        if (this.formGroup.invalid) {
            this.notificationService.validator();
            return;
        }

        if (this.id) {
            await this.edit();
        } else {
            await this.create();
        }
    }

    async edit() {
        const formData = this.formGroup.value;
        await this.httpClientService.put(`/api/navigationMenu/${this.id}`, formData, this.isLoading).toPromise()
            .then(() => {
                this.modal.close();
            });
    }

    async create() {
        const formData = this.formGroup.value;
        await this.httpClientService.post(`/api/navigationMenu`, formData, this.isLoading).toPromise()
            .then(() => {
                this.modal.close();
            });
    }
}
