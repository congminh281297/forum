import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import * as Admin from "./administrator-index";

const routes: Routes = [
    {
        path: "",
        component: Admin.AdministratorComponent,
        data: {
            title: 'Cấu hình'
        },
        children: [
            {
                path: "role",
                component: Admin.RoleComponent,
                data: {
                    title: 'Khai báo quyền'
                }
            },
            {
                path: "user",
                component: Admin.UserComponent,
                data: {
                    title: 'Khai báo tài khoản'
                }
            },
            {
                path: "navigation",
                component: Admin.NavigationComponent,
                data: {
                    title: 'Khai báo trang'
                }
            },
            {
                path: "role-navigation",
                component: Admin.RoleNavigationComponent,
                data: {
                    title: 'Phân quyền'
                }
            },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdministratorRouting { }
