import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClientService, loadingModel } from 'src/app/core/services/http-client.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { UtilityService } from 'src/app/core/services/utility.service';

@Component({
    templateUrl: "./role-navigation.component.html",
    styleUrls: ["./role-navigation.component.scss"]
})
export class RoleNavigationComponent implements OnInit {
    isLoading = new loadingModel();

    menus!: any[];
    menusHierachy!: any[];

    searchGroup: FormGroup = new FormGroup({
        roleId: new FormControl(),
    });

    roles!: any[];
    @ViewChild('tree') tree: any;

    constructor(
        private notificationService: NotificationService,
        private utilityService: UtilityService,
        private httpClientService: HttpClientService) {
    }

    async ngOnInit() {
        await Promise.all([this.loadData(), this.loadRoles()]);
    }

    async loadData() {
        const roleId = this.searchGroup.controls['roleId'].value;
        if (roleId) {
            await this.httpClientService.get('/api/roleNavigationMenu/getByRoleId?roleId=' + roleId, this.isLoading)
                .toPromise()
                .then((res: any) => {
                    this.menus = res;

                    for (let index = this.menus.length - 1; index >= 0; index--) {
                        if (this.menus.some(x => x.parentId === this.menus[index].id)) {
                            const nodes = this.menus.filter(x => x.parentId === this.menus[index].id);
                            this.menus[index].hasView = nodes.every(x => x.hasView);
                            this.menus[index].hasViewIndeterminate = nodes.some(x => x.hasView) && !this.menus[index].hasView;
                            this.menus[index].hasEdit = nodes.every(x => x.hasEdit);
                            this.menus[index].hasEditIndeterminate = nodes.some(x => x.hasEdit) && !this.menus[index].hasEdit;
                            this.menus[index].hasDelete = nodes.every(x => x.hasDelete);
                            this.menus[index].hasDeleteIndeterminate = nodes.some(x => x.hasDelete) && !this.menus[index].hasDelete;
                        }
                    }

                    this.menusHierachy = this.utilityService.unflatten(this.menus);
                    setTimeout(() => {
                        this.tree.treeModel.expandAll();
                    });
                });
        } else {
            this.menus = [];
            this.menusHierachy = [];
        }
    }

    async loadRoles() {
        await this.httpClientService.get('/api/role').toPromise()
            .then((res: any) => {
                this.roles = res;
            });
    }

    async onChangeRole() {
        await this.loadData();
    }

    selectHasAction(id: any, action: number) {
        const node = this.menus.find(x => x.id === id);
        this.changeHasAction(node, action);

        for (let index = this.menus.length - 1; index >= 0; index--) {
            if (this.menus[index].tenantId === node.tenantId) {
                const nodes = this.menus.filter(x => x.parentId === this.menus[index].id);
                if (nodes.length > 0) {
                    if (this.menus[index].parentId) {
                        this.menus[index].hasView = nodes.every(x => x.hasView);
                        this.menus[index].hasViewIndeterminate = nodes.some(x => x.hasView) && !this.menus[index].hasView;
                        this.menus[index].hasEdit = nodes.every(x => x.hasEdit);
                        this.menus[index].hasEditIndeterminate = nodes.some(x => x.hasEdit) && !this.menus[index].hasEdit;
                        this.menus[index].hasDelete = nodes.every(x => x.hasDelete);
                        this.menus[index].hasDeleteIndeterminate = nodes.some(x => x.hasDelete) && !this.menus[index].hasDelete;
                    } else {
                        this.menus[index].hasView = nodes.some(x => x.hasView);
                        this.menus[index].hasViewIndeterminate = nodes.some(x => x.hasView) && !nodes.every(x => x.hasView);
                        this.menus[index].hasEdit = nodes.some(x => x.hasEdit);
                        this.menus[index].hasEditIndeterminate = nodes.some(x => x.hasEdit) && !nodes.every(x => x.hasEdit);
                        this.menus[index].hasDelete = nodes.some(x => x.hasDelete);
                        this.menus[index].hasDeleteIndeterminate = nodes.some(x => x.hasDelete) && !nodes.every(x => x.hasDelete);
                    }
                }
            }
        }

        const nodes = this.menus.filter(x => x.tenantId === node.tenantId);
        const nodesHierachy = this.utilityService.unflatten(nodes);
        const index = this.menusHierachy.findIndex(x => x.tenantId === node.tenantId);
        this.menusHierachy[index] = nodesHierachy;
    }

    private changeHasAction(node: any, action: number) {
        const children = this.menus.filter(c => c.parentId == node.id);
        children.forEach((item: any) => {
            if (action === 1) {
                item.hasView = node.hasView;
            } else if (action === 2) {
                item.hasEdit = node.hasEdit;
            } else if (action === 3) {
                item.hasDelete = node.hasDelete;
            }

            this.changeHasAction(item, action);
        });
    }

    async save() {
        const roleId = this.searchGroup.controls['roleId'].value;
        if (!roleId) {
            this.notificationService.error('Hãy chọn quyền!');
            return;
        }

        const roleNavigationMenus = this.menus.filter(x => x.hasView || x.hasEdit || x.hasDelete)
            .map(x => {
                return { id: x.id, hasView: x.hasView, hasEdit: x.hasEdit, hasDelete: x.hasDelete };
            });

        const req = {
            roleId,
            roleNavigationMenus
        };

        await this.httpClientService.post('/api/roleNavigationMenu', req, this.isLoading).toPromise()
            .then(async () => {
                this.notificationService.updateSuccess();
                await this.loadData();
            });
    }
}
