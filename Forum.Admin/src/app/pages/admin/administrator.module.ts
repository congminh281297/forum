import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AdministratorRouting } from './administrator-routing';
import * as Admin from "./administrator-index";
import { InlineSVGModule } from 'ng-inline-svg';
import { TreeModule } from '@circlon/angular-tree-component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectConfig, NgSelectModule } from '@ng-select/ng-select';
import { DMRoleService } from 'src/app/core/services/dmRole.Service';
import { LayoutAdminModule } from '../../layouts/layout-admin.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AdministratorRouting,
        InlineSVGModule,
        TreeModule,
        NgbModule,
        NgSelectModule,
        LayoutAdminModule
    ], declarations: [
        Admin.AdministratorComponent,
        Admin.RoleComponent,
        Admin.UserComponent,
        Admin.EditUserModalComponent,
        Admin.EditRoleModalComponent,
        Admin.NavigationComponent,
        Admin.EditNavigationModalComponent,
        Admin.RoleNavigationComponent,
        Admin.EditUserModalComponent,
        Admin.EditPasswordModalComponent,
    ],
    providers: [
        DMRoleService,
        {
            provide: NgSelectConfig,
            useValue: {
                notFoundText: 'Không có dữ liệu',
                bindValue: 'id',
                bindLabel: 'text',
                clearAllText: 'Xóa tất cả',
            }
        },
    ]
})
export class AdministratorModule { }
