import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidatorConstants } from 'src/app/core/constants/validator.constants';
import { DMRoleService } from 'src/app/core/services/dmRole.Service';
import { HttpClientService, loadingModel } from 'src/app/core/services/http-client.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ValidatorService } from 'src/app/core/services/validator.service';

@Component({
    selector: 'app-edit-password-modal',
    templateUrl: './edit-password-modal.component.html'
})
export class EditPasswordModalComponent implements OnInit {
    @Input() id: any;
    @Input() userName: any;
    ValidatorConsts = ValidatorConstants;
    isLoading = new loadingModel();
    formGroup!: FormGroup;

    constructor(
        private fb: FormBuilder,
        private notificationService: NotificationService,
        private httpClientService: HttpClientService,
        public dmRoleService: DMRoleService,
        public validatorService: ValidatorService,
        public modal: NgbActiveModal) { }

    ngOnInit() {
        this.loadForm();
    }

    loadForm() {
        this.formGroup = this.fb.group({
            userId: new FormControl(this.id),
            password: new FormControl(null, [Validators.required, Validators.pattern(this.ValidatorConsts.v_password)]),
            confirmPassword: new FormControl(),
        }, {
            validator: this.validatorService.mustMatch('password', 'confirmPassword')
        });
    }

    async save() {
        if (this.formGroup.invalid) {
            this.formGroup.markAllAsTouched();
            this.notificationService.validator();
            return;
        }

        const formData = this.formGroup.getRawValue();
        await this.httpClientService.post(`/api/user/updatePassword`, formData, this.isLoading).toPromise()
            .then(() => {
                this.modal.close();
            });
    }
}
