import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { DialogService } from 'src/app/core/services/dialog.service';
import { HttpClientService, loadingModel } from 'src/app/core/services/http-client.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { PaginatorState } from 'src/app/core/models/paginator.model';
import { SortState } from 'src/app/core/models/sort.model';
import { EditUserModalComponent } from './edit-user-modal/edit-user-modal.component';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { EditPasswordModalComponent } from './edit-password-modal/edit-password-modal.component';
import { BaseUrlQueryableComponent } from 'src/app/components/url-queryable/url-queryable.component';
import { BaseRequestModel } from 'src/app/components/url-queryable/models/request-model';

@Component({
    templateUrl: "./user.component.html"
})
export class UserComponent extends BaseUrlQueryableComponent implements OnInit, AfterViewInit {
    requestModel!: BaseRequestModel;

    paginator: PaginatorState;
    sorting: SortState;
    isLoading = new loadingModel();
    users?: any[];
    searchGroup: FormGroup = new FormGroup({
        keyword: new FormControl(""),
    });

    constructor(
        private dialogService: DialogService,
        private notificationService: NotificationService,
        private modalService: NgbModal,
        private httpClientService: HttpClientService,
        protected override router: Router,
        protected override activatedRoute: ActivatedRoute) {
        super(router, activatedRoute);
        this.paginator = new PaginatorState();
        this.sorting = new SortState();
    }

    async ngOnComponentInit() { }

    async ngAfterViewInit() {
        this.searchGroup.controls['keyword'].valueChanges
            .pipe(debounceTime(150), distinctUntilChanged())
            .subscribe(async () => {
                this.paginator.page = 1;
                await this.load();
            });
    }

    protected async onUrlUpdated() {
        await this.loadData();
    }

    protected async convertUrlParamsToRequestModel(queryParams: any) {
        if (!this.requestModel) {
            this.requestModel = {} as BaseRequestModel;
        }

        Object.assign(this.requestModel, queryParams.params);

        if (this.searchGroup.controls['keyword'].value !== queryParams.params.keyword) {
            this.searchGroup.controls['keyword'].setValue(queryParams.params.keyword);
        }

        _.merge(this.sorting, {
            column: queryParams.params.column || "fullName",
            direction: queryParams.params.direction || "asc"
        });

        _.merge(this.paginator, {
            page: parseInt(queryParams.params.page || 1),
            pageSize: parseInt(queryParams.params.pageSize || 10)
        });
    }

    protected override convertRequestModelToUrlParams(requestModel: any) {
        delete requestModel.total;
        delete requestModel.pageSizes;
        return requestModel;
    }

    async loadData() {
        const params = {
            page: this.paginator.page,
            pageSize: this.paginator.pageSize,
            column: this.sorting.column,
            direction: this.sorting.direction,
            keyword: this.searchGroup.controls['keyword'].value || '',
        };

        await this.httpClientService.getWithParams('/api/user/getAllPaging', params, this.isLoading).toPromise()
            .then((res: any) => {
                this.paginator.total = res.total;
                this.users = res.results;
            });
    }

    create() {
        this.edit(undefined);
    }

    edit(id: any) {
        const modalRef = this.modalService.open(EditUserModalComponent,
            {
                backdrop: 'static',
                keyboard: false
            });

        modalRef.componentInstance.id = id;
        modalRef.result.then(async () => {
            if (id) {
                this.notificationService.updateSuccess();
            } else {
                this.notificationService.createSuccess();
            }

            await this.loadData();
        }, () => { });
    }

    async delete(id: any, name: any) {
        this.dialogService.open_dialog_confirm_delete(async () => {
            await this.httpClientService.delete(`/api/user/${id}`, this.isLoading).toPromise()
                .then(async () => {
                    this.notificationService.deleteSuccess();
                    await this.loadData();
                });
        }, 'Xóa user', `Bạn có muốn xóa user: ${name}?`)
    }

    async sort(column: string) {
        const sorting = this.sorting;
        const isActiveColumn = sorting.column === column;
        if (!isActiveColumn) {
            sorting.column = column;
            sorting.direction = 'asc';
        } else {
            sorting.direction = sorting.direction === 'asc' ? 'desc' : 'asc';
        }

        await this.load();
    }

    async paginate(_: any) {
        await this.load();
    }

    async load() {
        Object.assign(this.requestModel, this.searchGroup.value, this.sorting, this.paginator);
        await this.updateUrlParameters();
    }

    editPassword(item: any) {
        const modalRef = this.modalService.open(EditPasswordModalComponent,
            {
                backdrop: 'static',
                keyboard: false
            });

        modalRef.componentInstance.id = item.id;
        modalRef.componentInstance.userName = item.userName;
        modalRef.result.then(() => {
            this.notificationService.updateSuccess();
        }, () => { });
    }
}
