import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidatorConstants } from 'src/app/core/constants/validator.constants';
import { DMRoleService } from 'src/app/core/services/dmRole.Service';
import { HttpClientService, loadingModel } from 'src/app/core/services/http-client.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ValidatorService } from 'src/app/core/services/validator.service';

const EMPTY_DATA = {
    roles: [],
    isLocked: false,
};

@Component({
    selector: 'app-edit-user-modal',
    templateUrl: './edit-user-modal.component.html'
})
export class EditUserModalComponent implements OnInit {
    @Input() id: any;
    ValidatorConsts = ValidatorConstants;
    isLoading = new loadingModel();
    user: any;
    formGroup!: FormGroup;
    @ViewChild('file') file?: ElementRef;

    constructor(
        private fb: FormBuilder,
        private notificationService: NotificationService,
        private httpClientService: HttpClientService,
        public dmRoleService: DMRoleService,
        public validatorService: ValidatorService,
        public modal: NgbActiveModal) { }

    async ngOnInit() {
        await this.loadRole();
    }

    async loadRole() {
        if (!this.id) {
            this.user = EMPTY_DATA;
            this.loadForm();
        } else {
            await this.httpClientService.get(`/api/user/${this.id}`, this.isLoading).toPromise()
                .then(res => {
                    this.user = res;
                    this.loadForm();
                });
        }
    }

    loadForm() {
        const passwordValidate = !this.id ? [Validators.required, Validators.pattern(this.ValidatorConsts.v_password)] : null;
        this.formGroup = this.fb.group({
            fullName: new FormControl(this.user.fullName),
            userName: new FormControl(this.user.userName, [Validators.required, Validators.pattern(this.ValidatorConsts.v_username)]),
            password: new FormControl(this.user.password, passwordValidate),
            confirmPassword: new FormControl(),
            roles: new FormControl(this.user.roles),
            image: new FormControl(this.user.image),
            isLocked: new FormControl(this.user.isLocked),
        }, {
            validator: this.validatorService.mustMatch('password', 'confirmPassword')
        });
    }

    async save() {
        if (this.formGroup?.invalid) {
            this.formGroup.markAllAsTouched();
            this.notificationService.validator();
            return;
        }

        const formData = this.formGroup?.getRawValue();
        if (this.id) {
            await this.edit(formData);
        } else {
            await this.create(formData);
        }
    }

    async edit(formData: any) {
        await this.httpClientService.put(`/api/user/${this.id}`, formData, this.isLoading).toPromise()
            .then(() => {
                this.modal.close();
            });
    }

    async create(formData: any) {
        await this.httpClientService.post(`/api/user`, formData, this.isLoading).toPromise()
            .then(() => {
                this.modal.close();
            });
    }

    async uploadFile(files: any) {
        if (files.length === 0) {
            return;
        }

        await this.httpClientService.postFileToServerUpload(files).toPromise()
            .then((res: any) => {
                this.formGroup?.controls['image'].setValue(res[0]);
            })
            .finally(() => {
                this.file!.nativeElement.value = '';
            });
    }
}
