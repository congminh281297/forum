import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageConstants } from 'src/app/core/constants/message.constants';
import { UrlConstants } from 'src/app/core/constants/url.constant';
import { ValidatorConstants } from 'src/app/core/constants/validator.constants';
import { LoginService, NotificationService } from 'src/app/core/services';
import { ValidatorService } from 'src/app/core/services/validator.service';

@Component({
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
    public isLoading = false;
    public ValidatorConsts = ValidatorConstants;

    public loginForm = new FormGroup({
        userName: new FormControl(null, [Validators.required, Validators.pattern(this.ValidatorConsts.v_username)]),
        password: new FormControl(null, [Validators.required, Validators.pattern(this.ValidatorConsts.v_password)]),
    });

    constructor(
        private activated: ActivatedRoute,
        public validatorService: ValidatorService,
        private loginService: LoginService,
        private notificationService: NotificationService,
        private router: Router) {
        if (this.loginService.isUserAuthenticated()) {
            this.router.navigate([UrlConstants.HOME]);
        }
    }

    ngOnInit() { }

    async login() {
        if (this.loginForm.invalid) {
            this.loginForm.markAllAsTouched();
            this.notificationService.validator();
            return;
        }

        this.isLoading = true;
        const req = this.loginForm.getRawValue();
        await this.loginService.login(req.userName, req.password)
            .then(() => {
                let returnUrl = this.activated.snapshot.queryParams['returnUrl'] || '/';
                if (returnUrl.includes('login')) {
                    returnUrl = '/';
                }
                const url = this.sortParams(returnUrl)
                this.router.navigate([url.path], { queryParams: url.params });
            }).catch(err => {
                this.notificationService.error((err.error && err.error.message) || MessageConstants.SYSTEM_ERROR_MSG);
            }).finally(() => {
                this.isLoading = false;
            });
    }

    private sortParams(url: string): any {

        const _url = url.split('?')[0];
        const queryParams = url.split('?')[1] || '';
        const params = queryParams.split('&');
        let pair: string[] = [];
        let data: any = {};

        params.forEach((d) => {
            if (d && d != '') {
                pair = d.split('=');
                data[`${pair[0]}`] = decodeURIComponent(pair[1]);
            }
        });

        return { path: _url, params: data };
    }
}