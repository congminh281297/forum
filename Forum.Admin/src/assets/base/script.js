var SACookie = {
    getCookie: function (t) {
        var e = document.cookie.match(new RegExp("(?:^|; )" + t.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") + "=([^;]*)"));
        return e ? decodeURIComponent(e[1]) : void 0
    },
    setCookie: function (t, e, a) {
        a || (a = {}), (a = Object.assign({}, {
            path: "/"
        }, a)).expires instanceof Date && (a.expires = a.expires.toUTCString());
        var n = encodeURIComponent(t) + "=" + encodeURIComponent(e);
        for (var i in a)
            if (a.hasOwnProperty(i)) {
                n += "; " + i;
                var o = a[i];
                !0 !== o && (n += "=" + o)
            } document.cookie = n
    },
    deleteCookie: function (t) {
        setCookie(t, "", {
            "max-age": -1
        })
    }
};
"undefined" != typeof module && void 0 !== module.exports && (module.exports = SACookie);
var SADialog = function (t) {
    var e, a = this,
        n = SAUtil.getBody(),
        i = {
            placement: "top center",
            type: "loader",
            width: 100,
            state: "default",
            message: "Loading..."
        },
        o = {
            construct: function (t) {
                return o.init(t), a
            },
            init: function (t) {
                a.events = [], a.options = SAUtil.deepExtend({}, i, t), a.state = !1
            },
            show: function () {
                return o.eventTrigger("show"), e = document.createElement("DIV"), SAUtil.setHTML(e, a.options.message), SAUtil.addClass(e, "dialog dialog-shown"), SAUtil.addClass(e, "dialog-" + a.options.state), SAUtil.addClass(e, "dialog-" + a.options.type), "top center" == a.options.placement && SAUtil.addClass(e, "dialog-top-center"), n.appendChild(e), a.state = "shown", o.eventTrigger("shown"), a
            },
            hide: function () {
                return e && (o.eventTrigger("hide"), e.remove(), a.state = "hidden", o.eventTrigger("hidden")), a
            },
            eventTrigger: function (t) {
                for (var e = 0; e < a.events.length; e++) {
                    var n = a.events[e];
                    if (n.name == t) {
                        if (1 != n.one) return n.handler.call(this, a);
                        if (0 == n.fired) return a.events[e].fired = !0, n.handler.call(this, a)
                    }
                }
            },
            addEvent: function (t, e, n) {
                return a.events.push({
                    name: t,
                    handler: e,
                    one: n,
                    fired: !1
                }), a
            }
        };
    return a.setDefaults = function (t) {
        i = t
    }, a.shown = function () {
        return "shown" == a.state
    }, a.hidden = function () {
        return "hidden" == a.state
    }, a.show = function () {
        return o.show()
    }, a.hide = function () {
        return o.hide()
    }, a.on = function (t, e) {
        return o.addEvent(t, e)
    }, a.one = function (t, e) {
        return o.addEvent(t, e, !0)
    }, o.construct.apply(a, [t]), a
};
"undefined" != typeof module && void 0 !== module.exports && (module.exports = SADialog);
var SAHeader = function (t, e) {
    var a = this,
        n = SAUtil.getById(t),
        i = SAUtil.getBody();
    if (void 0 !== n) {
        var o = {
            offset: {
                desktop: !0,
                tabletAndMobile: !0
            },
            releseOnReverse: {
                desktop: !1,
                tabletAndMobile: !1
            }
        },
            l = {
                construct: function (t) {
                    return SAUtil.data(n).has("header") ? a = SAUtil.data(n).get("header") : (l.init(t), l.build(), SAUtil.data(n).set("header", a)), a
                },
                init: function (t) {
                    a.events = [], a.options = SAUtil.deepExtend({}, o, t)
                },
                build: function () {
                    var t = !0,
                        e = 0;
                    window.addEventListener("scroll", (function () {
                        var n, o = 0;
                        SAUtil.isBreakpointDown("lg") && !1 === a.options.offset.tabletAndMobile || SAUtil.isBreakpointUp("lg") && !1 === a.options.offset.desktop || (SAUtil.isBreakpointUp("lg") ? o = a.options.offset.desktop : SAUtil.isBreakpointDown("lg") && (o = a.options.offset.tabletAndMobile), n = SAUtil.getScrollTop(), SAUtil.isBreakpointDown("lg") && a.options.releseOnReverse.tabletAndMobile || SAUtil.isBreakpointUp("lg") && a.options.releseOnReverse.desktop ? (n > o && e < n ? (!1 === i.hasAttribute("data-header-scroll") && i.setAttribute("data-header-scroll", "on"), t && (l.eventTrigger("scrollOn", a), t = !1)) : (!0 === i.hasAttribute("data-header-scroll") && i.removeAttribute("data-header-scroll"), 0 == t && (l.eventTrigger("scrollOff", a), t = !0)), e = n) : n > o ? (!1 === i.hasAttribute("data-header-scroll") && i.setAttribute("data-header-scroll", "on"), t && (l.eventTrigger("scrollOn", a), t = !1)) : (!0 === i.hasAttribute("data-header-scroll") && i.removeAttribute("data-header-scroll"), 0 == t && (l.eventTrigger("scrollOff", a), t = !0)))
                    }))
                },
                eventTrigger: function (t, e) {
                    for (var n = 0; n < a.events.length; n++) {
                        var i = a.events[n];
                        if (i.name == t) {
                            if (1 != i.one) return i.handler.call(this, a, e);
                            if (0 == i.fired) return a.events[n].fired = !0, i.handler.call(this, a, e)
                        }
                    }
                },
                addEvent: function (t, e, n) {
                    a.events.push({
                        name: t,
                        handler: e,
                        one: n,
                        fired: !1
                    })
                }
            };
        return a.setDefaults = function (t) {
            o = t
        }, a.on = function (t, e) {
            return l.addEvent(t, e)
        }, l.construct.apply(a, [e]), !0, a
    }
};
"undefined" != typeof module && void 0 !== module.exports && (module.exports = SAHeader);

var SAMenu = function (t, e) {
    var a = this,
        n = !1,
        i = SAUtil.getById(t),
        o = SAUtil.getBody();
    if (i) {
        var l = {
            scroll: {
                rememberPosition: !1
            },
            accordion: {
                slideSpeed: 200,
                autoScroll: !1,
                autoScrollSpeed: 1200,
                expandAll: !0
            },
            dropdown: {
                timeout: 500
            }
        },
            r = {
                construct: function (t) {
                    return SAUtil.data(i).has("menu") ? a = SAUtil.data(i).get("menu") : (r.init(t), r.reset(), r.build(), SAUtil.data(i).set("menu", a)), a
                },
                init: function (t) {
                    a.events = [], a.eventHandlers = {}, a.options = SAUtil.deepExtend({}, l, t), a.pauseDropdownHoverTime = 0, a.uid = SAUtil.getUniqueID()
                },
                update: function (t) {
                    a.options = SAUtil.deepExtend({}, l, t), a.pauseDropdownHoverTime = 0, r.reset(), a.eventHandlers = {}, r.build(), SAUtil.data(i).set("menu", a)
                },
                reload: function () {
                    r.reset(), r.build(), r.resetSubmenuProps()
                },
                build: function () {
                    a.eventHandlers.event_1 = SAUtil.on(i, ".menu-toggle", "click", r.handleSubmenuAccordion), ("dropdown" === r.getSubmenuMode() || r.isConditionalSubmenuDropdown()) && (a.eventHandlers.event_2 = SAUtil.on(i, '[data-menu-toggle="hover"]', "mouseover", r.handleSubmenuDrodownHoverEnter), a.eventHandlers.event_3 = SAUtil.on(i, '[data-menu-toggle="hover"]', "mouseout", r.handleSubmenuDrodownHoverExit), a.eventHandlers.event_4 = SAUtil.on(i, '[data-menu-toggle="click"] > .menu-toggle, [data-menu-toggle="click"] > .menu-link .menu-toggle', "click", r.handleSubmenuDropdownClick), a.eventHandlers.event_5 = SAUtil.on(i, '[data-menu-toggle="tab"] > .menu-toggle, [data-menu-toggle="tab"] > .menu-link .menu-toggle', "click", r.handleSubmenuDropdownTabClick)), a.eventHandlers.event_6 = SAUtil.on(i, ".menu-item > .menu-link:not(.menu-toggle):not(.menu-link-toggle-skip)", "click", r.handleLinkClick), a.options.scroll && a.options.scroll.height && r.scrollInit()
                },
                reset: function () {
                    SAUtil.off(i, "click", a.eventHandlers.event_1), SAUtil.off(i, "mouseover", a.eventHandlers.event_2), SAUtil.off(i, "mouseout", a.eventHandlers.event_3), SAUtil.off(i, "click", a.eventHandlers.event_4), SAUtil.off(i, "click", a.eventHandlers.event_5), SAUtil.off(i, "click", a.eventHandlers.event_6)
                },
                scrollInit: function () {
                    a.options.scroll && a.options.scroll.height ? (SAUtil.scrollDestroy(i, !0), SAUtil.scrollInit(i, {
                        mobileNativeScroll: !0,
                        windowScroll: !1,
                        resetHeightOnDestroy: !0,
                        handleWindowResize: !0,
                        height: a.options.scroll.height,
                        rememberPosition: a.options.scroll.rememberPosition
                    })) : SAUtil.scrollDestroy(i, !0)
                },
                scrollUpdate: function () {
                    a.options.scroll && a.options.scroll.height && SAUtil.scrollUpdate(i)
                },
                scrollTop: function () {
                    a.options.scroll && a.options.scroll.height && SAUtil.scrollTop(i)
                },
                getSubmenuMode: function (t) {
                    return SAUtil.isBreakpointUp("lg") ? t && SAUtil.hasAttr(t, "data-menu-toggle") && "hover" == SAUtil.attr(t, "data-menu-toggle") ? "dropdown" : SAUtil.isset(a.options.submenu, "desktop.state.body") ? SAUtil.hasClasses(o, a.options.submenu.desktop.state.body) ? a.options.submenu.desktop.state.mode : a.options.submenu.desktop.default : SAUtil.isset(a.options.submenu, "desktop") ? a.options.submenu.desktop : void 0 : SAUtil.isBreakpointUp("md") && SAUtil.isBreakpointDown("lg") && SAUtil.isset(a.options.submenu, "tablet") ? a.options.submenu.tablet : !(!SAUtil.isBreakpointDown("md") || !SAUtil.isset(a.options.submenu, "mobile")) && a.options.submenu.mobile
                },
                isConditionalSubmenuDropdown: function () {
                    return !(!SAUtil.isBreakpointUp("lg") || !SAUtil.isset(a.options.submenu, "desktop.state.body"))
                },
                resetSubmenuProps: function (t) {
                    var e = SAUtil.findAll(i, ".menu-submenu");
                    if (e)
                        for (var a = 0, n = e.length; a < n; a++) {
                            var o = e[0];
                            SAUtil.css(o, "display", ""), SAUtil.css(o, "overflow", ""), o.hasAttribute("data-hor-direction") && (SAUtil.removeClass(o, "menu-submenu-left"), SAUtil.removeClass(o, "menu-submenu-right"), SAUtil.addClass(o, o.getAttribute("data-hor-direction")))
                        }
                },
                handleSubmenuDrodownHoverEnter: function (t) {
                    if ("accordion" !== r.getSubmenuMode(this) && !1 !== a.resumeDropdownHover()) {
                        var e = this;
                        "1" == e.getAttribute("data-hover") && (e.removeAttribute("data-hover"), clearTimeout(e.getAttribute("data-timeout")), e.removeAttribute("data-timeout")), r.showSubmenuDropdown(e)
                    }
                },
                handleSubmenuDrodownHoverExit: function (t) {
                    if (!1 !== a.resumeDropdownHover() && "accordion" !== r.getSubmenuMode(this)) {
                        var e = this,
                            n = a.options.dropdown.timeout,
                            i = setTimeout((function () {
                                "1" == e.getAttribute("data-hover") && r.hideSubmenuDropdown(e, !0)
                            }), n);
                        e.setAttribute("data-hover", "1"), e.setAttribute("data-timeout", i)
                    }
                },
                handleSubmenuDropdownClick: function (t) {
                    if ("accordion" !== r.getSubmenuMode(this)) {
                        var e = this.closest(".menu-item");
                        !1 !== r.eventTrigger("submenuToggle", this, t) && "accordion" != e.getAttribute("data-menu-submenu-mode") && (!1 === SAUtil.hasClass(e, "menu-item-hover") ? (SAUtil.addClass(e, "menu-item-open-dropdown"), r.showSubmenuDropdown(e)) : (SAUtil.removeClass(e, "menu-item-open-dropdown"), r.hideSubmenuDropdown(e, !0)), t.preventDefault())
                    }
                },
                handleSubmenuDropdownTabClick: function (t) {
                    if ("accordion" !== r.getSubmenuMode(this)) {
                        var e = this.closest(".menu-item");
                        !1 !== r.eventTrigger("submenuToggle", this, t) && "accordion" != e.getAttribute("data-menu-submenu-mode") && (0 == SAUtil.hasClass(e, "menu-item-hover") && (SAUtil.addClass(e, "menu-item-open-dropdown"), r.showSubmenuDropdown(e)), t.preventDefault())
                    }
                },
                handleLinkClick: function (t) {
                    var e = this.closest(".menu-item.menu-item-submenu");
                    !1 !== r.eventTrigger("linkClick", this, t) && e && "dropdown" === r.getSubmenuMode(e) && r.hideSubmenuDropdowns()
                },
                handleSubmenuDropdownClose: function (t, e) {
                    if ("accordion" !== r.getSubmenuMode(e)) {
                        var a = i.querySelectorAll(".menu-item.menu-item-submenu.menu-item-hover:not(.menu-item-tabs)");
                        if (a.length > 0 && !1 === SAUtil.hasClass(e, "menu-toggle") && 0 === e.querySelectorAll(".menu-toggle").length)
                            for (var n = 0, o = a.length; n < o; n++) r.hideSubmenuDropdown(a[0], !0)
                    }
                },
                handleSubmenuAccordion: function (t, e) {
                    var n, i = e || this;
                    if (!1 !== r.eventTrigger("submenuToggle", this, t))
                        if ("dropdown" === r.getSubmenuMode(e) && (n = i.closest(".menu-item")) && "accordion" != n.getAttribute("data-menu-submenu-mode")) t.preventDefault();
                        else {
                            var o = i.closest(".menu-item"),
                                l = SAUtil.child(o, ".menu-submenu, .menu-inner");
                            if (!SAUtil.hasClass(i.closest(".menu-item"), "menu-item-open-always") && o && l) {
                                t.preventDefault();
                                var s = a.options.accordion.slideSpeed;
                                if (!1 === SAUtil.hasClass(o, "menu-item-open")) {
                                    if (!1 === a.options.accordion.expandAll) {
                                        var d = i.closest(".menu-nav, .menu-subnav"),
                                            c = SAUtil.children(d, ".menu-item.menu-item-open.menu-item-submenu:not(.menu-item-here):not(.menu-item-open-always)");
                                        if (d && c)
                                            for (var u = 0, p = c.length; u < p; u++) {
                                                var f = c[0],
                                                    g = SAUtil.child(f, ".menu-submenu");
                                                g && SAUtil.slideUp(g, s, (function () {
                                                    r.scrollUpdate(), SAUtil.removeClass(f, "menu-item-open")
                                                }))
                                            }
                                    }
                                    SAUtil.slideDown(l, s, (function () {
                                        r.scrollToItem(i), r.scrollUpdate(), r.eventTrigger("submenuToggle", l, t)
                                    })), SAUtil.addClass(o, "menu-item-open")
                                } else SAUtil.slideUp(l, s, (function () {
                                    r.scrollToItem(i), r.scrollUpdate(), r.eventTrigger("submenuToggle", l, t)
                                })), SAUtil.removeClass(o, "menu-item-open")
                            }
                        }
                },
                scrollToItem: function (t) {
                    SAUtil.isBreakpointUp("lg") && a.options.accordion.autoScroll && "1" !== i.getAttribute("data-menu-scroll") && SAUtil.scrollTo(t, a.options.accordion.autoScrollSpeed)
                },
                hideSubmenuDropdown: function (t, e) {
                    e && (SAUtil.removeClass(t, "menu-item-hover"), SAUtil.removeClass(t, "menu-item-active-tab")), t.removeAttribute("data-hover"), t.getAttribute("data-menu-toggle-class") && SAUtil.removeClass(o, t.getAttribute("data-menu-toggle-class"));
                    var a = t.getAttribute("data-timeout");
                    t.removeAttribute("data-timeout"), clearTimeout(a)
                },
                hideSubmenuDropdowns: function () {
                    var t;
                    if (t = i.querySelectorAll('.menu-item-submenu.menu-item-hover:not(.menu-item-tabs):not([data-menu-toggle="tab"])'))
                        for (var e = 0, a = t.length; e < a; e++) r.hideSubmenuDropdown(t[e], !0)
                },
                showSubmenuDropdown: function (t) {
                    var e = i.querySelectorAll(".menu-item-submenu.menu-item-hover, .menu-item-submenu.menu-item-active-tab");
                    if (e)
                        for (var a = 0, n = e.length; a < n; a++) {
                            var l = e[a];
                            t !== l && !1 === l.contains(t) && !1 === t.contains(l) && r.hideSubmenuDropdown(l, !0)
                        }
                    SAUtil.addClass(t, "menu-item-hover");
                    var s = SAUtil.find(t, ".menu-submenu");
                    s && !1 === s.hasAttribute("data-hor-direction") && (SAUtil.hasClass(s, "menu-submenu-left") ? s.setAttribute("data-hor-direction", "menu-submenu-left") : SAUtil.hasClass(s, "menu-submenu-right") && s.setAttribute("data-hor-direction", "menu-submenu-right")), s && !0 === SAUtil.isOffscreen(s, "left", 15) ? (SAUtil.removeClass(s, "menu-submenu-left"), SAUtil.addClass(s, "menu-submenu-right")) : s && !0 === SAUtil.isOffscreen(s, "right", 15) && (SAUtil.removeClass(s, "menu-submenu-right"), SAUtil.addClass(s, "menu-submenu-left")), t.getAttribute("data-menu-toggle-class") && SAUtil.addClass(o, t.getAttribute("data-menu-toggle-class"))
                },
                createSubmenuDropdownClickDropoff: function (t) {
                    var e, a = (e = SAUtil.child(t, ".menu-submenu") ? SAUtil.css(e, "z-index") : 0) - 1,
                        n = document.createElement('<div class="menu-dropoff" style="background: transparent; position: fixed; top: 0; bottom: 0; left: 0; right: 0; z-index: ' + a + '"></div>');
                    o.appendChild(n), SAUtil.addEvent(n, "click", (function (e) {
                        e.stopPropagation(), e.preventDefault(), SAUtil.remove(this), r.hideSubmenuDropdown(t, !0)
                    }))
                },
                pauseDropdownHover: function (t) {
                    var e = new Date;
                    a.pauseDropdownHoverTime = e.getTime() + t
                },
                resumeDropdownHover: function () {
                    return (new Date).getTime() > a.pauseDropdownHoverTime
                },
                resetActiveItem: function (t) {
                    for (var e, n, o = 0, l = (e = i.querySelectorAll(".menu-item-active")).length; o < l; o++) {
                        var r = e[0];
                        SAUtil.removeClass(r, "menu-item-active"), SAUtil.hide(SAUtil.child(r, ".menu-submenu"));
                        for (var s = 0, d = (n = SAUtil.parents(r, ".menu-item-submenu") || []).length; s < d; s++) {
                            var c = n[o];
                            SAUtil.removeClass(c, "menu-item-open"), SAUtil.hide(SAUtil.child(c, ".menu-submenu"))
                        }
                    }
                    if (!1 === a.options.accordion.expandAll && (e = i.querySelectorAll(".menu-item-open")))
                        for (o = 0, l = e.length; o < l; o++) SAUtil.removeClass(n[0], "menu-item-open")
                },
                setActiveItem: function (t) {
                    r.resetActiveItem();
                    for (var e = SAUtil.parents(t, ".menu-item-submenu") || [], a = 0, n = e.length; a < n; a++) SAUtil.addClass(e[a], "menu-item-open");
                    SAUtil.addClass(t, "menu-item-active")
                },
                getBreadcrumbs: function (t) {
                    var e, a = [],
                        n = SAUtil.child(t, ".menu-link");
                    a.push({
                        text: e = SAUtil.child(n, ".menu-text") ? e.innerHTML : "",
                        title: n.getAttribute("title"),
                        href: n.getAttribute("href")
                    });
                    for (var i = SAUtil.parents(t, ".menu-item-submenu"), o = 0, l = i.length; o < l; o++) {
                        var r = SAUtil.child(i[o], ".menu-link");
                        a.push({
                            text: e = SAUtil.child(r, ".menu-text") ? e.innerHTML : "",
                            title: r.getAttribute("title"),
                            href: r.getAttribute("href")
                        })
                    }
                    return a.reverse()
                },
                getPageTitle: function (t) {
                    var e;
                    return SAUtil.child(t, ".menu-text") ? e.innerHTML : ""
                },
                eventTrigger: function (t, e, n) {
                    for (var i = 0; i < a.events.length; i++) {
                        var o = a.events[i];
                        if (o.name == t) {
                            if (1 != o.one) return o.handler.call(this, e, n);
                            if (0 == o.fired) return a.events[i].fired = !0, o.handler.call(this, e, n)
                        }
                    }
                },
                addEvent: function (t, e, n) {
                    a.events.push({
                        name: t,
                        handler: e,
                        one: n,
                        fired: !1
                    })
                },
                removeEvent: function (t) {
                    a.events[t] && delete a.events[t]
                }
            };
        return a.setDefaults = function (t) {
            l = t
        }, a.scrollUpdate = function () {
            return r.scrollUpdate()
        }, a.scrollReInit = function () {
            return r.scrollInit()
        }, a.scrollTop = function () {
            return r.scrollTop()
        }, a.setActiveItem = function (t) {
            return r.setActiveItem(t)
        }, a.reload = function () {
            return r.reload()
        }, a.update = function (t) {
            return r.update(t)
        }, a.getBreadcrumbs = function (t) {
            return r.getBreadcrumbs(t)
        }, a.getPageTitle = function (t) {
            return r.getPageTitle(t)
        }, a.getSubmenuMode = function (t) {
            return r.getSubmenuMode(t)
        }, a.hideDropdown = function (t) {
            r.hideSubmenuDropdown(t, !0)
        }, a.hideDropdowns = function () {
            r.hideSubmenuDropdowns()
        }, a.pauseDropdownHover = function (t) {
            r.pauseDropdownHover(t)
        }, a.resumeDropdownHover = function () {
            return r.resumeDropdownHover()
        }, a.on = function (t, e) {
            return r.addEvent(t, e)
        }, a.off = function (t) {
            return r.removeEvent(t)
        }, a.one = function (t, e) {
            return r.addEvent(t, e, !0)
        }, r.construct.apply(a, [e]), SAUtil.addResizeHandler((function () {
            n && a.reload()
        })), n = !0, a
    }
};
"undefined" != typeof module && void 0 !== module.exports && (module.exports = SAMenu), document.addEventListener("click", (function (t) {
    var e;
    if (e = SAUtil.getByTagName("body")[0].querySelectorAll('.menu-nav .menu-item.menu-item-submenu.menu-item-hover:not(.menu-item-tabs)[data-menu-toggle="click"]'))
        for (var a = 0, n = e.length; a < n; a++) {
            var i = e[a].closest(".menu-nav").parentNode;
            if (i) {
                var o = SAUtil.data(i).get("menu");
                if (!o) break;
                if (!o || "dropdown" !== o.getSubmenuMode()) break;
                t.target !== i && !1 === i.contains(t.target) && o.hideDropdowns()
            }
        }
}));
var SAOffcanvas = function (t, e) {
    var a = this,
        n = SAUtil.getById(t),
        i = SAUtil.getBody();
    if (n) {
        var o = {
            attrCustom: ""
        },
            l = {
                construct: function (t) {
                    return SAUtil.data(n).has("offcanvas") ? a = SAUtil.data(n).get("offcanvas") : (l.init(t), l.build(), SAUtil.data(n).set("offcanvas", a)), a
                },
                init: function (t) {
                    a.events = [], a.options = SAUtil.deepExtend({}, o, t), a.classBase = a.options.baseClass, a.attrCustom = a.options.attrCustom, a.classShown = a.classBase + "-on", a.classOverlay = a.classBase + "-overlay", a.target, a.state = SAUtil.hasClass(n, a.classShown) ? "shown" : "hidden"
                },
                build: function () {
                    if (a.options.toggleBy)
                        if ("string" == typeof a.options.toggleBy) SAUtil.addEvent(SAUtil.getById(a.options.toggleBy), "click", (function (t) {
                            t.preventDefault(), a.target = this, l.toggle()
                        }));
                        else if (a.options.toggleBy && a.options.toggleBy[0])
                            if (a.options.toggleBy[0].target)
                                for (var t in a.options.toggleBy) SAUtil.addEvent(SAUtil.getById(a.options.toggleBy[t].target), "click", (function (t) {
                                    t.preventDefault(), a.target = this, l.toggle()
                                }));
                            else
                                for (var t in a.options.toggleBy) SAUtil.addEvent(SAUtil.getById(a.options.toggleBy[t]), "click", (function (t) {
                                    t.preventDefault(), a.target = this, l.toggle()
                                }));
                        else a.options.toggleBy && a.options.toggleBy.target && SAUtil.addEvent(SAUtil.getById(a.options.toggleBy.target), "click", (function (t) {
                            t.preventDefault(), a.target = this, l.toggle()
                        }));
                    var e = SAUtil.getById(a.options.closeBy);
                    e && SAUtil.addEvent(e, "click", (function (t) {
                        t.preventDefault(), a.target = this, l.hide()
                    }))
                },
                isShown: function () {
                    return "shown" == a.state
                },
                toggle: function () {
                    l.eventTrigger("toggle"), "shown" == a.state ? l.hide() : l.show()
                },
                show: function () {
                    "shown" != a.state && (l.eventTrigger("beforeShow"), l.toggleClass("show"), SAUtil.attr(i, "data-offcanvas-" + a.classBase, "on"), SAUtil.addClass(n, a.classShown), a.attrCustom.length > 0 && SAUtil.attr(i, "data-offcanvas-" + a.classCustom, "on"), a.state = "shown", a.options.overlay && (a.overlay = SAUtil.insertAfter(document.createElement("DIV"), n), SAUtil.addClass(a.overlay, a.classOverlay), SAUtil.addEvent(a.overlay, "click", (function (t) {
                        t.preventDefault(), l.hide(a.target)
                    }))), l.eventTrigger("afterShow"))
                },
                hide: function () {
                    "hidden" != a.state && (l.eventTrigger("beforeHide"), l.toggleClass("hide"), SAUtil.removeAttr(i, "data-offcanvas-" + a.classBase), SAUtil.removeClass(n, a.classShown), a.attrCustom.length > 0 && SAUtil.removeAttr(i, "data-offcanvas-" + a.attrCustom), a.state = "hidden", a.options.overlay && a.overlay && SAUtil.remove(a.overlay), l.eventTrigger("afterHide"))
                },
                toggleClass: function (t) {
                    var e, n = SAUtil.attr(a.target, "id");
                    if (a.options.toggleBy && a.options.toggleBy[0] && a.options.toggleBy[0].target)
                        for (var i in a.options.toggleBy) a.options.toggleBy[i].target === n && (e = a.options.toggleBy[i]);
                    else a.options.toggleBy && a.options.toggleBy.target && (e = a.options.toggleBy);
                    if (e) {
                        var o = SAUtil.getById(e.target);
                        "show" === t && SAUtil.addClass(o, e.state), "hide" === t && SAUtil.removeClass(o, e.state)
                    }
                },
                eventTrigger: function (t, e) {
                    for (var n = 0; n < a.events.length; n++) {
                        var i = a.events[n];
                        if (i.name == t) {
                            if (1 != i.one) return i.handler.call(this, a, e);
                            if (0 == i.fired) return a.events[n].fired = !0, i.handler.call(this, a, e)
                        }
                    }
                },
                addEvent: function (t, e, n) {
                    a.events.push({
                        name: t,
                        handler: e,
                        one: n,
                        fired: !1
                    })
                }
            };
        return a.setDefaults = function (t) {
            o = t
        }, a.isShown = function () {
            return l.isShown()
        }, a.hide = function () {
            return l.hide()
        }, a.show = function () {
            return l.show()
        }, a.on = function (t, e) {
            return l.addEvent(t, e)
        }, a.one = function (t, e) {
            return l.addEvent(t, e, !0)
        }, l.construct.apply(a, [e]), !0, a
    }
};
"undefined" != typeof module && void 0 !== module.exports && (module.exports = SAOffcanvas);
var SAScrolltop = function (t, e) {
    var a = this,
        n = SAUtil.getById(t),
        i = SAUtil.getBody();
    if (n) {
        var o = {
            offset: 300,
            speed: 6e3
        },
            l = {
                construct: function (t) {
                    return SAUtil.data(n).has("scrolltop") ? a = SAUtil.data(n).get("scrolltop") : (l.init(t), l.build(), SAUtil.data(n).set("scrolltop", a)), a
                },
                init: function (t) {
                    a.events = [], a.options = SAUtil.deepExtend({}, o, t)
                },
                build: function () {
                    window.addEventListener("scroll", (function () {
                        SAUtil.throttle(undefined, (function () {
                            l.handle()
                        }), 200)
                    })), SAUtil.addEvent(n, "click", l.scroll)
                },
                handle: function () {
                    SAUtil.getScrollTop() > a.options.offset ? !1 === i.hasAttribute("data-scrolltop") && i.setAttribute("data-scrolltop", "on") : !0 === i.hasAttribute("data-scrolltop") && i.removeAttribute("data-scrolltop")
                },
                scroll: function (t) {
                    t.preventDefault(), SAUtil.scrollTop(0, a.options.speed)
                },
                eventTrigger: function (t, e) {
                    for (var n = 0; n < a.events.length; n++) {
                        var i = a.events[n];
                        if (i.name == t) {
                            if (1 != i.one) return i.handler.call(this, a, e);
                            if (0 == i.fired) return a.events[n].fired = !0, i.handler.call(this, a, e)
                        }
                    }
                },
                addEvent: function (t, e, n) {
                    a.events.push({
                        name: t,
                        handler: e,
                        one: n,
                        fired: !1
                    })
                }
            };
        return a.setDefaults = function (t) {
            o = t
        }, a.on = function (t, e) {
            return l.addEvent(t, e)
        }, a.one = function (t, e) {
            return l.addEvent(t, e, !0)
        }, l.construct.apply(a, [e]), !0, a
    }
};
"undefined" != typeof module && void 0 !== module.exports && (module.exports = SAScrolltop);
var SAToggle = function (t, e, a) {
    var n = this,
        i = t,
        o = e;
    if (i) {
        var l = {
            targetToggleMode: "class"
        },
            r = {
                construct: function (t) {
                    return SAUtil.data(i).has("toggle") ? n = SAUtil.data(i).get("toggle") : (r.init(t), r.build(), SAUtil.data(i).set("toggle", n)), n
                },
                init: function (t) {
                    n.element = i, n.events = [], n.options = SAUtil.deepExtend({}, l, t), n.target = o, n.targetState = n.options.targetState, n.toggleState = n.options.toggleState, "class" == n.options.targetToggleMode ? n.state = SAUtil.hasClasses(n.target, n.targetState) ? "on" : "off" : n.state = SAUtil.hasAttr(n.target, "data-" + n.targetState) ? SAUtil.attr(n.target, "data-" + n.targetState) : "off"
                },
                build: function () {
                    SAUtil.addEvent(i, "mouseup", r.toggle)
                },
                toggle: function (t) {
                    return r.eventTrigger("beforeToggle"), "off" == n.state ? r.toggleOn() : r.toggleOff(), r.eventTrigger("afterToggle"), t.preventDefault(), n
                },
                toggleOn: function () {
                    return r.eventTrigger("beforeOn"), "class" == n.options.targetToggleMode ? SAUtil.addClass(n.target, n.targetState) : SAUtil.attr(n.target, "data-" + n.targetState, "on"), n.toggleState && SAUtil.addClass(i, n.toggleState), n.state = "on", r.eventTrigger("afterOn"), r.eventTrigger("toggle"), n
                },
                toggleOff: function () {
                    return r.eventTrigger("beforeOff"), "class" == n.options.targetToggleMode ? SAUtil.removeClass(n.target, n.targetState) : SAUtil.removeAttr(n.target, "data-" + n.targetState), n.toggleState && SAUtil.removeClass(i, n.toggleState), n.state = "off", r.eventTrigger("afterOff"), r.eventTrigger("toggle"), n
                },
                eventTrigger: function (t) {
                    for (var e = 0; e < n.events.length; e++) {
                        var a = n.events[e];
                        if (a.name == t) {
                            if (1 != a.one) return a.handler.call(this, n);
                            if (0 == a.fired) return n.events[e].fired = !0, a.handler.call(this, n)
                        }
                    }
                },
                addEvent: function (t, e, a) {
                    return n.events.push({
                        name: t,
                        handler: e,
                        one: a,
                        fired: !1
                    }), n
                }
            };
        return n.setDefaults = function (t) {
            l = t
        }, n.getState = function () {
            return n.state
        }, n.toggle = function () {
            return r.toggle()
        }, n.toggleOn = function () {
            return r.toggleOn()
        }, n.toggleOff = function () {
            return r.toggleOff()
        }, n.on = function (t, e) {
            return r.addEvent(t, e)
        }, n.one = function (t, e) {
            return r.addEvent(t, e, !0)
        }, r.construct.apply(n, [a]), n
    }
};
"undefined" != typeof module && void 0 !== module.exports && (module.exports = SAToggle),
    Element.prototype.matches || (Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector),
    Element.prototype.closest || (Element.prototype.matches || (Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector),
        Element.prototype.closest = function (t) {
            var e = this;
            if (!document.documentElement.contains(this)) return null;
            do {
                if (e.matches(t)) return e;
                e = e.parentElement
            } while (null !== e);
            return null
        }),
    function (t) {
        for (var e = 0; e < t.length; e++) window[t[e]] && !("remove" in window[t[e]].prototype) && (window[t[e]].prototype.remove = function () {
            this.parentNode.removeChild(this)
        })
    }(["Element", "CharacterData", "DocumentType"]),
    function () {
        for (var t = 0, e = ["webkit", "moz"], a = 0; a < e.length && !window.requestAnimationFrame; ++a) window.requestAnimationFrame = window[e[a] + "RequestAnimationFrame"], window.cancelAnimationFrame = window[e[a] + "CancelAnimationFrame"] || window[e[a] + "CancelRequestAnimationFrame"];
        window.requestAnimationFrame || (window.requestAnimationFrame = function (e) {
            var a = (new Date).getTime(),
                n = Math.max(0, 16 - (a - t)),
                i = window.setTimeout((function () {
                    e(a + n)
                }), n);
            return t = a + n, i
        }), window.cancelAnimationFrame || (window.cancelAnimationFrame = function (t) {
            clearTimeout(t)
        })
    }(), [Element.prototype, Document.prototype, DocumentFragment.prototype].forEach((function (t) {
        t.hasOwnProperty("prepend") || Object.defineProperty(t, "prepend", {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            value: function () {
                var t = Array.prototype.slice.call(arguments),
                    e = document.createDocumentFragment();
                t.forEach((function (t) {
                    var a = t instanceof Node;
                    e.appendChild(a ? t : document.createTextNode(String(t)))
                })), this.insertBefore(e, this.firstChild)
            }
        })
    })), null == Element.prototype.getAttributeNames && (Element.prototype.getAttributeNames = function () {
        for (var t = this.attributes, e = t.length, a = new Array(e), n = 0; n < e; n++) a[n] = t[n].name;
        return a
    }), window.SAUtilElementDataStore = {}, window.SAUtilElementDataStoreID = 0, window.SAUtilDelegatedEventHandlers = {};
var SAUtil = function () {
    var t = [],
        e = {
            sm: 544,
            md: 768,
            lg: 992,
            xl: 1200
        },
        a = function () {
            window.addEventListener("resize", (function () {
                SAUtil.throttle(undefined, (function () {
                    ! function () {
                        for (var e = 0; e < t.length; e++) t[e].call()
                    }()
                }), 200)
            }))
        };
    return {
        init: function (t) {
            t && t.breakpoints && (e = t.breakpoints), a()
        },
        addResizeHandler: function (e) {
            t.push(e)
        },
        removeResizeHandler: function (e) {
            for (var a = 0; a < t.length; a++) e === t[a] && delete t[a]
        },
        runResizeHandlers: function () {
            _runResizeHandlers()
        },
        resize: function () {
            if ("function" == typeof Event) window.dispatchEvent(new Event("resize"));
            else {
                var t = window.document.createEvent("UIEvents");
                t.initUIEvent("resize", !0, !1, window, 0), window.dispatchEvent(t)
            }
        },
        getURLParam: function (t) {
            var e, a, n = window.location.search.substring(1).split("&");
            for (e = 0; e < n.length; e++)
                if ((a = n[e].split("="))[0] == t) return unescape(a[1]);
            return null
        },
        isMobileDevice: function () {
            var t = this.getViewPort().width < this.getBreakpoint("lg");
            return !1 === t && (t = null != navigator.userAgent.match(/iPad/i)), t
        },
        isDesktopDevice: function () {
            return !SAUtil.isMobileDevice()
        },
        getViewPort: function () {
            var t = window,
                e = "inner";
            return "innerWidth" in window || (e = "client", t = document.documentElement || document.body), {
                width: t[e + "Width"],
                height: t[e + "Height"]
            }
        },
        isInResponsiveRange: function (t) {
            var e = this.getViewPort().width;
            return "general" == t || ("desktop" == t && e >= this.getBreakpoint("lg") + 1 || ("tablet" == t && e >= this.getBreakpoint("md") + 1 && e < this.getBreakpoint("lg") || ("mobile" == t && e <= this.getBreakpoint("md") || ("desktop-and-tablet" == t && e >= this.getBreakpoint("md") + 1 || ("tablet-and-mobile" == t && e <= this.getBreakpoint("lg") || "minimal-desktop-and-below" == t && e <= this.getBreakpoint("xl"))))))
        },
        isBreakpointUp: function (t) {
            return this.getViewPort().width >= this.getBreakpoint(t)
        },
        isBreakpointDown: function (t) {
            return this.getViewPort().width < this.getBreakpoint(t)
        },
        getUniqueID: function (t) {
            return t + Math.floor(Math.random() * (new Date).getTime())
        },
        getBreakpoint: function (t) {
            return e[t]
        },
        isset: function (t, e) {
            var a;
            if (-1 !== (e = e || "").indexOf("[")) throw new Error("Unsupported object path notation.");
            e = e.split(".");
            do {
                if (void 0 === t) return !1;
                if (a = e.shift(), !t.hasOwnProperty(a)) return !1;
                t = t[a]
            } while (e.length);
            return !0
        },
        getHighestZindex: function (t) {
            for (var e, a; t && t !== document;) {
                if (("absolute" === (e = SAUtil.css(t, "position")) || "relative" === e || "fixed" === e) && (a = parseInt(SAUtil.css(t, "z-index")), !isNaN(a) && 0 !== a)) return a;
                t = t.parentNode
            }
            return null
        },
        hasFixedPositionedParent: function (t) {
            for (; t && t !== document;) {
                if ("fixed" === SAUtil.css(t, "position")) return !0;
                t = t.parentNode
            }
            return !1
        },
        sleep: function (t) {
            for (var e = (new Date).getTime(), a = 0; a < 1e7 && !((new Date).getTime() - e > t); a++);
        },
        getRandomInt: function (t, e) {
            return Math.floor(Math.random() * (e - t + 1)) + t
        },
        isAngularVersion: function () {
            return void 0 !== window.Zone
        },
        deepExtend: function (t) {
            t = t || {};
            for (var e = 1; e < arguments.length; e++) {
                var a = arguments[e];
                if (a)
                    for (var n in a) a.hasOwnProperty(n) && ("[object Object]" !== Object.prototype.toString.call(a[n]) ? t[n] = a[n] : t[n] = SAUtil.deepExtend(t[n], a[n]))
            }
            return t
        },
        extend: function (t) {
            t = t || {};
            for (var e = 1; e < arguments.length; e++)
                if (arguments[e])
                    for (var a in arguments[e]) arguments[e].hasOwnProperty(a) && (t[a] = arguments[e][a]);
            return t
        },
        getById: function (t) {
            return "string" == typeof t ? document.getElementById(t) : t
        },
        getByTag: function (t) {
            return document.getElementsByTagName(t)
        },
        getByTagName: function (t) {
            return document.getElementsByTagName(t)
        },
        getByClass: function (t) {
            return document.getElementsByClassName(t)
        },
        getBody: function () {
            return document.getElementsByTagName("body")[0]
        },
        hasClasses: function (t, e) {
            if (t) {
                for (var a = e.split(" "), n = 0; n < a.length; n++)
                    if (0 == SAUtil.hasClass(t, SAUtil.trim(a[n]))) return !1;
                return !0
            }
        },
        hasClass: function (t, e) {
            if (t) return t.classList ? t.classList.contains(e) : new RegExp("\\b" + e + "\\b").test(t.className)
        },
        addClass: function (t, e) {
            if (t && void 0 !== e) {
                var a = e.split(" ");
                if (t.classList)
                    for (var n = 0; n < a.length; n++) a[n] && a[n].length > 0 && t.classList.add(SAUtil.trim(a[n]));
                else if (!SAUtil.hasClass(t, e))
                    for (var i = 0; i < a.length; i++) t.className += " " + SAUtil.trim(a[i])
            }
        },
        removeClass: function (t, e) {
            if (t && void 0 !== e) {
                var a = e.split(" ");
                if (t.classList)
                    for (var n = 0; n < a.length; n++) t.classList.remove(SAUtil.trim(a[n]));
                else if (SAUtil.hasClass(t, e))
                    for (var i = 0; i < a.length; i++) t.className = t.className.replace(new RegExp("\\b" + SAUtil.trim(a[i]) + "\\b", "g"), "")
            }
        },
        triggerCustomEvent: function (t, e, a) {
            var n;
            window.CustomEvent ? n = new CustomEvent(e, {
                detail: a
            }) : (n = document.createEvent("CustomEvent")).initCustomEvent(e, !0, !0, a), t.dispatchEvent(n)
        },
        triggerEvent: function (t, e) {
            var a;
            if (t.ownerDocument) a = t.ownerDocument;
            else {
                if (9 != t.nodeType) throw new Error("Invalid node passed to fireEvent: " + t.id);
                a = t
            }
            if (t.dispatchEvent) {
                var n = "";
                switch (e) {
                    case "click":
                    case "mouseenter":
                    case "mouseleave":
                    case "mousedown":
                    case "mouseup":
                        n = "MouseEvents";
                        break;
                    case "focus":
                    case "change":
                    case "blur":
                    case "select":
                        n = "HTMLEvents";
                        break;
                    default:
                        throw "fireEvent: Couldn't find an event class for event '" + e + "'."
                }
                var i = "change" != e;
                (o = a.createEvent(n)).initEvent(e, i, !0), o.synthetic = !0, t.dispatchEvent(o, !0)
            } else if (t.fireEvent) {
                var o;
                (o = a.createEventObject()).synthetic = !0, t.fireEvent("on" + e, o)
            }
        },
        index: function (t) {
            for (var e = t.parentNode.children, a = 0; a < e.length; a++)
                if (e[a] == t) return a
        },
        trim: function (t) {
            return t.trim()
        },
        eventTriggered: function (t) {
            return !!t.currentTarget.dataset.triggered || (t.currentTarget.dataset.triggered = !0, !1)
        },
        remove: function (t) {
            t && t.parentNode && t.parentNode.removeChild(t)
        },
        find: function (t, e) {
            if (t = SAUtil.getById(t)) return t.querySelector(e)
        },
        findAll: function (t, e) {
            if (t = SAUtil.getById(t)) return t.querySelectorAll(e)
        },
        insertAfter: function (t, e) {
            return e.parentNode.insertBefore(t, e.nextSibling)
        },
        parents: function (t, e) {
            Element.prototype.matches || (Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector || function (t) {
                for (var e = (this.document || this.ownerDocument).querySelectorAll(t), a = e.length; --a >= 0 && e.item(a) !== this;);
                return a > -1
            });
            for (var a = []; t && t !== document; t = t.parentNode) e ? t.matches(e) && a.push(t) : a.push(t);
            return a
        },
        children: function (t, e, a) {
            if (t && t.childNodes) {
                for (var n = [], i = 0, o = t.childNodes.length; i < o; ++i) 1 == t.childNodes[i].nodeType && SAUtil.matches(t.childNodes[i], e, a) && n.push(t.childNodes[i]);
                return n
            }
        },
        child: function (t, e, a) {
            var n = SAUtil.children(t, e, a);
            return n ? n[0] : null
        },
        matches: function (t, e, a) {
            var n = Element.prototype,
                i = n.matches || n.webkitMatchesSelector || n.mozMatchesSelector || n.msMatchesSelector || function (t) {
                    return -1 !== [].indexOf.call(document.querySelectorAll(t), this)
                };
            return !(!t || !t.tagName) && i.call(t, e)
        },
        data: function (t) {
            return {
                set: function (e, a) {
                    t && (void 0 === t.customDataTag && (window.SAUtilElementDataStoreID++, t.customDataTag = window.SAUtilElementDataStoreID), void 0 === window.SAUtilElementDataStore[t.customDataTag] && (window.SAUtilElementDataStore[t.customDataTag] = {}), window.SAUtilElementDataStore[t.customDataTag][e] = a)
                },
                get: function (e) {
                    if (t) return void 0 === t.customDataTag ? null : this.has(e) ? window.SAUtilElementDataStore[t.customDataTag][e] : null
                },
                has: function (e) {
                    return !!t && (void 0 !== t.customDataTag && !(!window.SAUtilElementDataStore[t.customDataTag] || !window.SAUtilElementDataStore[t.customDataTag][e]))
                },
                remove: function (e) {
                    t && this.has(e) && delete window.SAUtilElementDataStore[t.customDataTag][e]
                }
            }
        },
        outerWidth: function (t, e) {
            var a;
            return !0 === e ? (a = parseFloat(t.offsetWidth), a += parseFloat(SAUtil.css(t, "margin-left")) + parseFloat(SAUtil.css(t, "margin-right")), parseFloat(a)) : a = parseFloat(t.offsetWidth)
        },
        offset: function (t) {
            var e, a;
            if (t) return t.getClientRects().length ? (e = t.getBoundingClientRect(), a = t.ownerDocument.defaultView, {
                top: e.top + a.pageYOffset,
                left: e.left + a.pageXOffset
            }) : {
                top: 0,
                left: 0
            }
        },
        height: function (t) {
            return SAUtil.css(t, "height")
        },
        outerHeight: function (t, e) {
            var a, n = t.offsetHeight;
            return void 0 !== e && !0 === e ? (a = getComputedStyle(t), n += parseInt(a.marginTop) + parseInt(a.marginBottom)) : n
        },
        visible: function (t) {
            return !(0 === t.offsetWidth && 0 === t.offsetHeight)
        },
        attr: function (t, e, a) {
            if (null != t) return void 0 === a ? t.getAttribute(e) : void t.setAttribute(e, a)
        },
        hasAttr: function (t, e) {
            if (null != t) return !!t.getAttribute(e)
        },
        removeAttr: function (t, e) {
            null != t && t.removeAttribute(e)
        },
        animate: function (t, e, a, n, i, o) {
            var l = {};
            if (l.linear = function (t, e, a, n) {
                return a * t / n + e
            }, i = l.linear, "number" == typeof t && "number" == typeof e && "number" == typeof a && "function" == typeof n) {
                "function" != typeof o && (o = function () { });
                var r = window.requestAnimationFrame || function (t) {
                    window.setTimeout(t, 20)
                },
                    s = e - t;
                n(t);
                var d = window.performance && window.performance.now ? window.performance.now() : +new Date;
                r((function l(c) {
                    var u = (c || +new Date) - d;
                    u >= 0 && n(i(u, t, s, a)), u >= 0 && u >= a ? (n(e), o()) : r(l)
                }))
            }
        },
        actualCss: function (t, e, a) {
            var n, i = "";
            if (t instanceof HTMLElement != !1) return t.getAttribute("hidden-" + e) && !1 !== a ? parseFloat(t.getAttribute("hidden-" + e)) : (i = t.style.cssText, t.style.cssText = "position: absolute; visibility: hidden; display: block;", "width" == e ? n = t.offsetWidth : "height" == e && (n = t.offsetHeight), t.style.cssText = i, t.setAttribute("hidden-" + e, n), parseFloat(n))
        },
        actualHeight: function (t, e) {
            return SAUtil.actualCss(t, "height", e)
        },
        actualWidth: function (t, e) {
            return SAUtil.actualCss(t, "width", e)
        },
        getScroll: function (t, e) {
            return e = "scroll" + e, t == window || t == document ? self["scrollTop" == e ? "pageYOffset" : "pageXOffset"] || browserSupportsBoxModel && document.documentElement[e] || document.body[e] : t[e]
        },
        css: function (t, e, a) {
            if (t)
                if (void 0 !== a) t.style[e] = a;
                else {
                    var n = (t.ownerDocument || document).defaultView;
                    if (n && n.getComputedStyle) return e = e.replace(/([A-Z])/g, "-$1").toLowerCase(), n.getComputedStyle(t, null).getPropertyValue(e);
                    if (t.currentStyle) return e = e.replace(/\-(\w)/g, (function (t, e) {
                        return e.toUpperCase()
                    })), a = t.currentStyle[e], /^\d+(em|pt|%|ex)?$/i.test(a) ? function (e) {
                        var a = t.style.left,
                            n = t.runtimeStyle.left;
                        return t.runtimeStyle.left = t.currentStyle.left, t.style.left = e || 0, e = t.style.pixelLeft + "px", t.style.left = a, t.runtimeStyle.left = n, e
                    }(a) : a
                }
        },
        slide: function (t, e, a, n, i) {
            if (!(!t || "up" == e && !1 === SAUtil.visible(t) || "down" == e && !0 === SAUtil.visible(t))) {
                a = a || 600;
                var o = SAUtil.actualHeight(t),
                    l = !1,
                    r = !1;
                SAUtil.css(t, "padding-top") && !0 !== SAUtil.data(t).has("slide-padding-top") && SAUtil.data(t).set("slide-padding-top", SAUtil.css(t, "padding-top")), SAUtil.css(t, "padding-bottom") && !0 !== SAUtil.data(t).has("slide-padding-bottom") && SAUtil.data(t).set("slide-padding-bottom", SAUtil.css(t, "padding-bottom")), SAUtil.data(t).has("slide-padding-top") && (l = parseInt(SAUtil.data(t).get("slide-padding-top"))), SAUtil.data(t).has("slide-padding-bottom") && (r = parseInt(SAUtil.data(t).get("slide-padding-bottom"))), "up" == e ? (t.style.cssText = "display: block; overflow: hidden;", l && SAUtil.animate(0, l, a, (function (e) {
                    t.style.paddingTop = l - e + "px"
                }), "linear"), r && SAUtil.animate(0, r, a, (function (e) {
                    t.style.paddingBottom = r - e + "px"
                }), "linear"), SAUtil.animate(0, o, a, (function (e) {
                    t.style.height = o - e + "px"
                }), "linear", (function () {
                    t.style.height = "", t.style.display = "none", "function" == typeof n && n()
                }))) : "down" == e && (t.style.cssText = "display: block; overflow: hidden;", l && SAUtil.animate(0, l, a, (function (e) {
                    t.style.paddingTop = e + "px"
                }), "linear", (function () {
                    t.style.paddingTop = ""
                })), r && SAUtil.animate(0, r, a, (function (e) {
                    t.style.paddingBottom = e + "px"
                }), "linear", (function () {
                    t.style.paddingBottom = ""
                })), SAUtil.animate(0, o, a, (function (e) {
                    t.style.height = e + "px"
                }), "linear", (function () {
                    t.style.height = "", t.style.display = "", t.style.overflow = "", "function" == typeof n && n()
                })))
            }
        },
        slideUp: function (t, e, a) {
            SAUtil.slide(t, "up", e, a)
        },
        slideDown: function (t, e, a) {
            SAUtil.slide(t, "down", e, a)
        },
        show: function (t, e) {
            void 0 !== t && (t.style.display = e || "block")
        },
        hide: function (t) {
            void 0 !== t && (t.style.display = "none")
        },
        addEvent: function (t, e, a, n) {
            null != t && t.addEventListener(e, a)
        },
        removeEvent: function (t, e, a) {
            null !== t && t.removeEventListener(e, a)
        },
        on: function (t, e, a, n) {
            if (e) {
                var i = SAUtil.getUniqueID("event");
                return window.SAUtilDelegatedEventHandlers[i] = function (a) {
                    for (var i = t.querySelectorAll(e), o = a.target; o && o !== t;) {
                        for (var l = 0, r = i.length; l < r; l++) o === i[l] && n.call(o, a);
                        o = o.parentNode
                    }
                }, SAUtil.addEvent(t, a, window.SAUtilDelegatedEventHandlers[i]), i
            }
        },
        off: function (t, e, a) {
            t && window.SAUtilDelegatedEventHandlers[a] && (SAUtil.removeEvent(t, e, window.SAUtilDelegatedEventHandlers[a]), delete window.SAUtilDelegatedEventHandlers[a])
        },
        one: function (t, e, a) {
            t.addEventListener(e, (function e(n) {
                return n.target && n.target.removeEventListener && n.target.removeEventListener(n.type, e), t && t.removeEventListener && n.currentTarget.removeEventListener(n.type, e), a(n)
            }))
        },
        hash: function (t) {
            var e, a = 0;
            if (0 === t.length) return a;
            for (e = 0; e < t.length; e++) a = (a << 5) - a + t.charCodeAt(e), a |= 0;
            return a
        },
        animateClass: function (t, e, a) {
            var n, i = {
                animation: "animationend",
                OAnimation: "oAnimationEnd",
                MozAnimation: "mozAnimationEnd",
                WebkitAnimation: "webkitAnimationEnd",
                msAnimation: "msAnimationEnd"
            };
            for (var o in i) void 0 !== t.style[o] && (n = i[o]);
            SAUtil.addClass(t, "animated " + e), SAUtil.one(t, n, (function () {
                SAUtil.removeClass(t, "animated " + e)
            })), a && SAUtil.one(t, n, a)
        },
        transitionEnd: function (t, e) {
            var a, n = {
                transition: "transitionend",
                OTransition: "oTransitionEnd",
                MozTransition: "mozTransitionEnd",
                WebkitTransition: "webkitTransitionEnd",
                msTransition: "msTransitionEnd"
            };
            for (var i in n) void 0 !== t.style[i] && (a = n[i]);
            SAUtil.one(t, a, e)
        },
        animationEnd: function (t, e) {
            var a, n = {
                animation: "animationend",
                OAnimation: "oAnimationEnd",
                MozAnimation: "mozAnimationEnd",
                WebkitAnimation: "webkitAnimationEnd",
                msAnimation: "msAnimationEnd"
            };
            for (var i in n) void 0 !== t.style[i] && (a = n[i]);
            SAUtil.one(t, a, e)
        },
        animateDelay: function (t, e) {
            for (var a = ["webkit-", "moz-", "ms-", "o-", ""], n = 0; n < a.length; n++) SAUtil.css(t, a[n] + "animation-delay", e)
        },
        animateDuration: function (t, e) {
            for (var a = ["webkit-", "moz-", "ms-", "o-", ""], n = 0; n < a.length; n++) SAUtil.css(t, a[n] + "animation-duration", e)
        },
        scrollTo: function (t, e, a) {
            a = a || 500;
            var n, i, o = t ? SAUtil.offset(t).top : 0,
                l = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
            e && (l += e), n = l, i = o, SAUtil.animate(n, i, a, (function (t) {
                document.documentElement.scrollTop = t, document.body.parentNode.scrollTop = t, document.body.scrollTop = t
            }))
        },
        scrollTop: function (t, e) {
            SAUtil.scrollTo(null, t, e)
        },
        isArray: function (t) {
            return t && Array.isArray(t)
        },
        ready: function (t) {
            (document.attachEvent ? "complete" === document.readyState : "loading" !== document.readyState) ? t() : document.addEventListener("DOMContentLoaded", t)
        },
        isEmpty: function (t) {
            for (var e in t)
                if (t.hasOwnProperty(e)) return !1;
            return !0
        },
        numberString: function (t) {
            for (var e = (t += "").split("."), a = e[0], n = e.length > 1 ? "." + e[1] : "", i = /(\d+)(\d{3})/; i.test(a);) a = a.replace(i, "$1,$2");
            return a + n
        },
        detectIE: function () {
            var t = window.navigator.userAgent,
                e = t.indexOf("MSIE ");
            if (e > 0) return parseInt(t.substring(e + 5, t.indexOf(".", e)), 10);
            if (t.indexOf("Trident/") > 0) {
                var a = t.indexOf("rv:");
                return parseInt(t.substring(a + 3, t.indexOf(".", a)), 10)
            }
            var n = t.indexOf("Edge/");
            return n > 0 && parseInt(t.substring(n + 5, t.indexOf(".", n)), 10)
        },
        isRTL: function () {
            var t = SAUtil.getByTagName("html")[0];
            if (t) return "rtl" == SAUtil.attr(t, "direction")
        },
        scrollInit: function (t, e) {
            if (t) {
                e = SAUtil.deepExtend({}, {
                    wheelSpeed: .5,
                    swipeEasing: !0,
                    wheelPropagation: !1,
                    minScrollbarLength: 40,
                    maxScrollbarLength: 300,
                    suppressScrollX: !0
                }, e), a(), e.handleWindowResize && SAUtil.addResizeHandler((function () {
                    a()
                }))
            }
            function a() {
                var a, n, i = t.getAttributeNames();
                if (i.length > 0 && i.forEach((function (a) {
                    if (/^data-.*/g.test(a) && 0 == ["scroll", "height", "mobile-height"].includes(n)) {
                        var n = a.replace("data-", "").toLowerCase().replace(/(?:[\s-])\w/g, (function (t) {
                            return t.replace("-", "").toUpperCase()
                        }));
                        e[n] = SAUtil.filterBoolean(t.getAttribute(a))
                    }
                })), !1 !== (n = e.height instanceof Function ? e.height.call() : !0 === SAUtil.isMobileDevice() && e.mobileHeight ? parseInt(e.mobileHeight) : e.height ? parseInt(e.height) : parseInt(SAUtil.css(t, "height"))))
                    if (n = parseInt(n), !e.mobileNativeScroll && !e.disableForMobile || !0 !== SAUtil.isMobileDevice())
                        if (n > 0 && SAUtil.css(t, "height", n + "px"), e.desktopNativeScroll) SAUtil.css(t, "overflow", "auto");
                        else {
                            "true" == SAUtil.attr(t, "data-window-scroll") && (e.windowScroll = !0), (a = SAUtil.data(t).get("ps")) ? a.update() : (SAUtil.css(t, "overflow", "hidden"), SAUtil.addClass(t, "scroll"), a = new PerfectScrollbar(t, e), SAUtil.data(t).set("ps", a));
                            var o = SAUtil.attr(t, "id");
                            if (!0 === e.rememberPosition && SACookie && o) {
                                if (SACookie.getCookie(o)) {
                                    var l = parseInt(SACookie.getCookie(o));
                                    l > 0 && (t.scrollTop = l)
                                }
                                t.addEventListener("ps-scroll-y", (function () {
                                    SACookie.setCookie(o, t.scrollTop)
                                }))
                            }
                        }
                    else (a = SAUtil.data(t).get("ps")) ? (e.resetHeightOnDestroy ? SAUtil.css(t, "height", "auto") : (SAUtil.css(t, "overflow", "auto"), n > 0 && SAUtil.css(t, "height", n + "px")), a.destroy(), a = SAUtil.data(t).remove("ps")) : n > 0 && (SAUtil.css(t, "overflow", "auto"), SAUtil.css(t, "height", n + "px"));
                else SAUtil.scrollDestroy(t, !0)
            }
        },
        scrollUpdate: function (t) {
            var e = SAUtil.data(t).get("ps");
            e && e.update()
        },
        scrollUpdateAll: function (t) {
            for (var e = SAUtil.findAll(t, ".ps"), a = 0, n = e.length; a < n; a++) SAUtil.scrollUpdate(e[a])
        },
        scrollDestroy: function (t, e) {
            var a = SAUtil.data(t).get("ps");
            a && (a.destroy(), a = SAUtil.data(t).remove("ps")), t && e && (t.style.setProperty("overflow", ""), t.style.setProperty("height", ""))
        },
        filterBoolean: function (t) {
            return !0 === t || "true" === t || !1 !== t && "false" !== t && t
        },
        setHTML: function (t, e) {
            t.innerHTML = e
        },
        getHTML: function (t) {
            if (t) return t.innerHTML
        },
        getDocumentHeight: function () {
            var t = document.body,
                e = document.documentElement;
            return Math.max(t.scrollHeight, t.offsetHeight, e.clientHeight, e.scrollHeight, e.offsetHeight)
        },
        getScrollTop: function () {
            return (document.scrollingElement || document.documentElement).scrollTop
        },
        changeColor: function (t, e) {
            var a = !1;
            "#" == t[0] && (t = t.slice(1), a = !0);
            var n = parseInt(t, 16),
                i = (n >> 16) + e;
            i > 255 ? i = 255 : i < 0 && (i = 0);
            var o = (n >> 8 & 255) + e;
            o > 255 ? o = 255 : o < 0 && (o = 0);
            var l = (255 & n) + e;
            return l > 255 ? l = 255 : l < 0 && (l = 0), (a ? "#" : "") + (l | o << 8 | i << 16).toString(16)
        },
        throttle: function (t, e, a) {
            t || (t = setTimeout((function () {
                e(), t = void 0
            }), a))
        },
        debounce: function (t, e, a) {
            clearTimeout(t), t = setTimeout(e, a)
        },
        btnWait: function (t, e, a, n) {
            if (t && (void 0 !== n && !0 === n && SAUtil.attr(t, "disabled", !0), e && (SAUtil.addClass(t, e), SAUtil.attr(t, "wait-class", e)), a)) {
                var i = SAUtil.find(t, ".btn-caption");
                i ? (SAUtil.data(i).set("caption", SAUtil.getHTML(i)), SAUtil.setHTML(i, a)) : (SAUtil.data(t).set("caption", SAUtil.getHTML(t)), SAUtil.setHTML(t, a))
            }
        },
        btnRelease: function (t) {
            if (t) {
                SAUtil.removeAttr(t, "disabled"), SAUtil.hasAttr(t, "wait-class") && SAUtil.removeClass(t, SAUtil.attr(t, "wait-class"));
                var e = SAUtil.find(t, ".btn-caption");
                e && SAUtil.data(e).has("caption") ? SAUtil.setHTML(e, SAUtil.data(e).get("caption")) : SAUtil.data(t).has("caption") && SAUtil.setHTML(t, SAUtil.data(t).get("caption"))
            }
        },
        isOffscreen: function (t, e, a) {
            a = a || 0;
            var n = SAUtil.getViewPort().width,
                i = SAUtil.getViewPort().height,
                o = SAUtil.offset(t).top,
                l = SAUtil.outerHeight(t) + a,
                r = SAUtil.offset(t).left,
                s = SAUtil.outerWidth(t) + a;
            if ("bottom" == e) {
                if (i < o + l) return !0;
                if (i > o + 1.5 * l) return !0
            }
            if ("top" == e) {
                if (o < 0) return !0;
                if (o > l) return !0
            }
            return "left" == e && r < 0 || "right" == e && n < r + s
        }
    }
}();
"undefined" != typeof module && void 0 !== module.exports && (module.exports = SAUtil), SAUtil.ready((function () {
    SAUtil.init()
}));
window.onload = function() {
    SAUtil.removeClass(SAUtil.getBody(), 'page-loading');
}
