import { InjectionToken } from "@angular/core";
export interface CurrencyMaskConfig {
    align: string;
    allowNegative: boolean;
    allowZero: boolean;
    decimal: string;
    precision: number;
    prefix: string;
    suffix: string;
    thousands: string;
    max: number;
    min: number;
    integer: boolean;
}
export declare let CURRENCY_MASK_CONFIG: InjectionToken<CurrencyMaskConfig>;
