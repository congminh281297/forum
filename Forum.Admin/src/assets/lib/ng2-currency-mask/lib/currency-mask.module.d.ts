import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './currency-mask.directive';
import * as ɵngcc2 from '@angular/common';
import * as ɵngcc3 from '@angular/forms';
export declare class CurrencyMaskModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<CurrencyMaskModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDeclaration<CurrencyMaskModule, [typeof ɵngcc1.CurrencyMaskDirective], [typeof ɵngcc2.CommonModule, typeof ɵngcc3.FormsModule], [typeof ɵngcc1.CurrencyMaskDirective]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDeclaration<CurrencyMaskModule>;
}

//# sourceMappingURL=currency-mask.module.d.ts.map