import { LayoutModule } from './layouts/layout.module';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  NgbDateAdapter,
  NgbDateParserFormatter,
  NgbDatepickerI18n,
  NgbModule
} from '@ng-bootstrap/ng-bootstrap';
import {
  PerfectScrollbarConfigInterface,
  PERFECT_SCROLLBAR_CONFIG
} from 'ngx-perfect-scrollbar';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { LayoutConfig } from './core/common/layout.config';
import { AuthGuard } from './core/guards/auth.guard';
import { NumberFormat } from './core/pipes/number-format.pipe';
import {
  AppConfigService,
  HtmlClassService,
  LayoutConfigService,
  LayoutRefService,
  MenuAsideService,
  MenuConfigService,
  MenuHorizontalService,
  PageConfigService,
  SplashScreenService,
  SubheaderService
} from './core/services';
import { DialogService } from './core/services/dialog.service';
import { DMCoSoService } from './core/services/dmCoSo.Service';
import { HttpClientService } from './core/services/http-client.service';
import { LoginService } from './core/services/login.service';
import { NotificationService } from './core/services/notification.service';
import { UtilityService } from './core/services/utility.service';
import { ValidatorService } from './core/services/validator.service';
import {
  CustomAdapter,
  CustomDateParserFormatter,
  CustomDatepickerI18n
} from './core/utils/date-picker.utils';
import { StatisticsComponent } from './pages/home/components/statistics/statistics.component';
import { UserOnlineComponent } from './pages/home/components/user-online/user-online.component';
import { HomeComponent } from './pages/home/home.component';
import { LanguageClientComponent } from './pages/language/language-client.component';
import { LanguageDetailComponent } from './pages/language/language-detail/language-detail.component';
import { RegisterComponent } from './pages/register/register.component';


export function initializeLayoutConfig(
  layoutConfigService: LayoutConfigService
) {
  return () => {
    if (layoutConfigService.getConfig() === null) {
      layoutConfigService.loadConfigs(new LayoutConfig().configs);
    }
  };
}

export function initializeApp(appConfigService: AppConfigService) {
  return () => appConfigService.load();
}
// tslint:disable-next-line:class-name
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  wheelSpeed: 0.5,
  swipeEasing: true,
  minScrollbarLength: 40,
  maxScrollbarLength: 300,
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LanguageClientComponent,
    LanguageDetailComponent,
    RegisterComponent,
    StatisticsComponent,
    UserOnlineComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    NgbModule,
    AppRoutingModule,
    HttpClientModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      timeOut: 3000,
      enableHtml: true,
    }),
    LayoutModule
  ],
  providers: [
    NumberFormat,
    TitleCasePipe,
    AuthGuard,
    LayoutConfigService,
    AppConfigService,
    LayoutRefService,
    MenuConfigService,
    PageConfigService,
    SplashScreenService,
    SubheaderService,
    MenuHorizontalService,
    MenuAsideService,
    HtmlClassService,
    ValidatorService,
    DMCoSoService,
    LoginService,
    NotificationService,
    UtilityService,
    HttpClientService,
    DialogService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeLayoutConfig,
      multi: true,
      deps: [LayoutConfigService],
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (loginService: LoginService) => () =>
        loginService.startRefreshTokenTimer(),
      multi: true,
      deps: [LoginService],
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (utilityService: UtilityService) => () =>
        utilityService.printConsoleWarning(),
      multi: true,
      deps: [UtilityService],
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter },
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
