import { MessageConstants } from './../../core/constants/message.constants';
import { UrlConstants } from './../../core/constants/url.constant';
import { NotificationService } from './../../core/services/notification.service';
import { LoginService } from './../../core/services/login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ValidatorConstants } from './../../core/constants/validator.constants';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'login-modal',
  templateUrl: 'login-modal.component.html'
})

export class LoginModalComponent implements OnInit {
  public isLoading = false;
  public ValidatorConsts = ValidatorConstants;

  public loginForm = new FormGroup({
    userName: new FormControl(null, [Validators.required, Validators.pattern(this.ValidatorConsts.v_username)]),
    password: new FormControl(null, [Validators.required, Validators.pattern(this.ValidatorConsts.v_password)]),
  });

  constructor(private modalService: NgbModal, public modal: NgbActiveModal,
    private activated: ActivatedRoute,
    private loginService: LoginService,
    private notificationService: NotificationService,
    private router: Router) {
    if (this.loginService.isUserAuthenticated()) {
      this.router.navigate([UrlConstants.HOME]);
    }
  }

  ngOnInit() { }


  async login() {
    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      this.notificationService.validator();
      return;
    }

    this.isLoading = true;
    const req = this.loginForm.getRawValue();
    await this.loginService.login(req.userName, req.password)
      .then(() => {
        this.modal.close(true);
      }).catch(err => {
        this.notificationService.error((err.error && err.error.message) || MessageConstants.SYSTEM_ERROR_MSG);
      }).finally(() => {
        this.isLoading = false;
      });

  }

  redirectToRegisterPage() {
    this.modal.close();
    this.router.navigate(["/dang-ky"])
  }

}
