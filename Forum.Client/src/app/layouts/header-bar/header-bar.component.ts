import { Component, OnInit } from '@angular/core';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { faList } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'header-bar',
  templateUrl: 'header-bar.component.html',
})
export class HeaderBarComponent implements OnInit {
  faQuestionCircle = faQuestionCircle;
  faList = faList;
  constructor() {}

  ngOnInit() {}
}
