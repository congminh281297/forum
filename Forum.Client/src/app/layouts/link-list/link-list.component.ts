import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { faHome } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'link-list',
  templateUrl: 'link-list.component.html',
})

export class LinkListComponent implements OnInit {
  faHome = faHome;
  linkList: string = "Trang chủ";
  constructor(private router: Router) {
    router.url.split('/').forEach(s => {
      this.linkList += `>${this.getNameLinkUrl(s)}`;
    });
    router.events.subscribe((val: any) => {
      this.linkList = "Trang chủ";
      if (val.url)
        val.url.split('/').forEach((s: any) => {
          this.linkList += `${this.getNameLinkUrl(s)}`;
        });;
    })
  }

  getNameLinkUrl(name: string) {
    switch (name) {
      case "ngon-ngu":
        return " > Ngôn ngữ";
      case "thu-thuat":
        return " > Thủ thuật";
      case "dang-ky":
        return " > Đăng ký";
      default:
        return "";
    }
  }
  ngOnInit() { }
}
