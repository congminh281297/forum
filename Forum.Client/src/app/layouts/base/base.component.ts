import { LoginService } from './../../core/services/login.service';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
	selector: 'sa-base',
	templateUrl: './base.component.html',
	styleUrls: ['./base.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class BaseComponent implements OnInit, OnDestroy {

	constructor(public loginService: LoginService
	) {
	}

	ngOnInit(): void {

	}


	ngOnDestroy(): void {
	}

}
