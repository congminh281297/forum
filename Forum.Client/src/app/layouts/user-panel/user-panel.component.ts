import { OffcanvasOptions } from './../../core/directives/offcanvas.directive';
import { LoginService } from 'src/app/core/services/login.service';
import { HttpClientService } from './../../core/services/http-client.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'user-panel',
    templateUrl: 'user-panel.component.html'
})

export class UserPanelComponent implements OnInit {
    offcanvasOptions: OffcanvasOptions = {
        overlay: true,
        baseClass: "offcanvas",
        placement: "right",
        closeBy: "sa_quick_user_close",
        toggleBy: "sa_quick_user_toggle"
    };
    public currentUser: any;
    constructor(
        private modalService: NgbModal,
        private httpClientService: HttpClientService,
        private router: Router,
        private loginService: LoginService) {
        this.currentUser = this.loginService.getLoggedInUser();
    }

    ngOnInit() { }

    logout() {
        this.loginService.logout();
        this.close();
    }
    close() {
        const el = document.getElementById('sa_quick_user_close');
        el?.click()
    }

    changePassword() {
        // const modalRef = this.modalService.open(
        //     ChangPasswordComponent,
        //     {
        //       backdrop: 'static',
        //       keyboard: false,
        //     }
        //   );

        //   modalRef.result.then(
        //     async () => {
        //     },
        //     () => {}
        //   );
    }
}