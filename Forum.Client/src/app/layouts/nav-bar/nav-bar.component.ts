import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { faCog, faSearch, faSignInAlt, faUser } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginModalComponent } from '../../components';
import { Language } from 'src/app/core/models/language';
import { HttpClientService, LoginService } from 'src/app/core/services';
import { PaginatorState } from 'src/app/core/models/paginator.model';

@Component({
    selector: 'nav-bar',
    templateUrl: 'nav-bar.component.html',
})

export class NavbarComponent implements OnInit {
    faSearch = faSearch;
    faCog = faCog;
    faSignInAlt = faSignInAlt;
    faUser = faUser;
    user: any;
    languages: Language[] = [];
    paginator: PaginatorState = new PaginatorState();
    url: string;

    constructor(private httpClientService: HttpClientService,
        private modalService: NgbModal,
        public loginService: LoginService,
        private router: Router) {
        this.url = router.url;

        router.events.subscribe((val: any) => {
            this.url = val.url;
        })
    }

    isActived(name: string): string {
        return this.url.includes(name) ? "active" : "";
    }
    isActivedHome() {
        return this.url.length == 1 ? "active" : "";
    }
    ngOnInit() {
        this.user = this.loginService.getLoggedInUser();
        this.getLanguages();
    }
    getLanguages() {
        this.paginator.page = 1;
        this.paginator.pageSize = 100;
        let params = {
            ...this.paginator
        };
        this.httpClientService.getWithParams('/api/language/GetAllPaging', params)
            .toPromise().then((res: any) => {
                this.languages = res.results.map((s: any) => {
                    return {
                        id: s.id,
                        slug: s.slug,
                        name: s.name,
                        isLocked: s.isLocked
                    }
                }).filter((s: any) => {
                    return !s.isLocked;
                });
                console.log(this.languages);
            })
    }
    openLoginModal() {
        this.modalService.open(LoginModalComponent,
            {
                windowClass: 'login-modal',
                backdrop: 'static',
                keyboard: false,
            }).result.then(res => {
                if (res) this.user = this.loginService.getLoggedInUser();
            });
    }

    backToHome() {
        this.router.navigate(["/"]);
    }
}
