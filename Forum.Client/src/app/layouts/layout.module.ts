import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import * as Layout from './index';
import * as Component from '../components/index';
import * as Directive from '../core/directives/index';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FontAwesomeModule,
    RouterModule,
    PerfectScrollbarModule
  ],
  declarations: [
    Layout.NavbarComponent,
    Layout.HeaderBarComponent,
    Layout.LinkListComponent,
    Layout.FooterComponent,
    Component.LoginModalComponent,
    Component.ConfirmDeleteComponent,
    Layout.TopBarComponent,
    Layout.BaseComponent,
    Layout.UserPanelComponent,
    Directive.ToggleDirective,
    Directive.OffcanvasDirective,
    Directive.MenuDirective,
    Directive.HeaderDirective,
    Directive.ScrollTopDirective,
    Directive.BtnLoaderDirective,
    Directive.SADateMaskDirective,
    Directive.SAGenderIconDirective,
    Directive.SATextareaHeightDirective,
    Directive.SANumberMaskDirective,
    Directive.ContentEditableFormDirective,
  ],
  exports: [
    Layout.NavbarComponent,
    Layout.HeaderBarComponent,
    Layout.LinkListComponent,
    Layout.FooterComponent,
    Layout.UserPanelComponent,
    Layout.BaseComponent,
    Directive.ToggleDirective,
    Directive.OffcanvasDirective,
    Directive.MenuDirective,
    Directive.HeaderDirective,
    Directive.ScrollTopDirective,
    Directive.BtnLoaderDirective,
    Directive.SADateMaskDirective,
    Directive.SAGenderIconDirective,
    Directive.SATextareaHeightDirective,
    Directive.SANumberMaskDirective,
    Directive.ContentEditableFormDirective,

  ],
  entryComponents: [
    Component.ConfirmDeleteComponent,
  ],
  providers: [],
})
export class LayoutModule { }
