
import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';
import * as objectPath from 'object-path';

export interface MenuOptions {
	scroll?: any;
	submenu?: any;
	accordion?: any;
	dropdown?: any;
}

@Directive({
	selector: '[saMenu]',
	exportAs: 'saMenu',
})
export class MenuDirective implements AfterViewInit {
	@Input() options?: MenuOptions;
	private menu: any;

	constructor(private el: ElementRef) {
	}

	ngAfterViewInit(): void {
		this.setupOptions();
		this.menu = new SAMenu(this.el.nativeElement, this.options);
	}

	getMenu() {
		return this.menu;
	}

	private setupOptions() {
		let menuDesktopMode = 'accordion';
		if (this.el.nativeElement.getAttribute('data-samenu-dropdown') === '1') {
			menuDesktopMode = 'dropdown';
		}

		if (typeof objectPath.get(this.options!, 'submenu.desktop') === 'object') {
			objectPath.set(this.options!, 'submenu.desktop', menuDesktopMode);
		}
	}
}
