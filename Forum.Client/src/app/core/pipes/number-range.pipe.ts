import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "numberRange"
})
export class NumberRange implements PipeTransform {

    transform(num: number): number[] {
       return  Array.from(new Array(num), (x, i) => i+1)
    }
}