export class UrlConstants {
  static readonly HOME = '/admin/tong-quan';
  static readonly LOGIN = '/admin/login';
  static readonly FORBIDDEN = '/admin/forbidden';
}