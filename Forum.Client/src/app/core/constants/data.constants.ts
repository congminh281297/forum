import { CurrencyMaskConfig } from 'src/assets/lib/ng2-currency-mask';
export class DataConstants {


    /** Trạng thái tương tác */
    public static trangThaiTuongTac = [
        { id: 1, text: 'Thành công' },
        { id: 2, text: 'Lỗi' },
    ];

    /** Trạng thái thiết bị */
    public static trangThaiThietBi = [
        { id: '2', text: 'Trong kho' },
        { id: '1', text: 'Đang sử dụng' },
    ];


    /** Giới tính */
    public static Gender = [
        { id: 1, text: 'Nam' },
        { id: 2, text: 'Nữ' },
    ]


    /** Loại lý do */
    public static ReasonType = [
        { id: 0, text: 'Bảo trì' },
        { id: 1, text: 'Xuất huỷ - thanh lý' },
    ]


    public static MaintenanceStatus = [
        { id: '1', text: 'Đã bảo trì' },
        { id: '2', text: 'Không bảo trì' },
    ]

    public static Status = [
        { id: '1', text: 'Chưa duyệt' },
        { id: '2', text: 'Đã duyệt' },
        { id: '3', text: 'Từ chối' },
    ]

    // code phiếu xuất(configNumber)
    public static codeNumberExport = 'GoodsIssue';
    public static codeNumberImport = 'GoodsReceipt';
    public static codeNumberPlan = 'Plan';
    public static codeNumberCheckWarehouse = 'CheckWarehouse';
    public static codeNumberAllocation = 'Allocation';
    public static codeNumberMaintenance = 'Maintenance';


    public static CustomCurrencyMaskConfig: CurrencyMaskConfig = {
        align: "right",
        allowNegative: true,
        decimal: ",",
        precision: 2,
        prefix: "",
        suffix: "",
        thousands: "."
    };

    public static OptionCurrencyMask_Money: CurrencyMaskConfig = {
        align: 'right',
        allowNegative: true,
        precision: 4,
        thousands: '.',
        decimal: ',',
        prefix: '',
        suffix: ' đ',
    };

    public static OptionCurrencyMask_Percent: CurrencyMaskConfig = {
        align: 'right',
        allowNegative: true,
        precision: 4,
        thousands: '.',
        decimal: ',',
        prefix: '',
        suffix: ' %',
    };

    public static UnitIncreaseShelfLife =
        [
            { id: 0, text: 'Ngày' },
            { id: 1, text: 'Tháng' },
            { id: 2, text: 'Quý' },
            { id: 3, text: 'Năm' }
        ];

    public static ExpirationStatus =
        [
            { id: '0', text: 'Sắp hết hạn sử dụng' },
            { id: '1', text: 'Hết hạn sử dụng' },
        ]

    public static thang = Array.from(new Array(12), (x, i) => {
        i = i + 1;
        return {
            id: i, text: 'Tháng ' + i
        }
    });

    public static quy = [
        { id: 1, text: 'Quý 1' },
        { id: 2, text: 'Quý 2' },
        { id: 3, text: 'Quý 3' },
        { id: 4, text: 'Quý 4' }
    ];

    public static nam = Array.from(new Array(new Date().getFullYear() - 2017), (x, i) => {
        i = i + 2017 + 1;
        return {
            id: i, text: 'Năm ' + i
        }
    }).reverse();
}

export enum ImportType {
    Head = 1,
    New,
    ReImport,
    Take
}

export enum ExportType {
    Transfer,
    Liquidation,
    Use,
}