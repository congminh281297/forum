import { Inject, Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SystemConstants } from '../constants/system.constants';
import { Router } from '@angular/router';
import { MessageConstants } from '../constants/message.constants';
import { UrlConstants } from '../constants/url.constant';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class LoginService {
    private headers = new HttpHeaders();

    constructor(
        @Inject(Injector) private injector: Injector,
        private router: Router,
        private http: HttpClient) {
        this.headers = this.headers.set('Content-Type', 'application/json');
    }

    private get toastrService(): ToastrService {
        return this.injector.get(ToastrService);
    }

    async login(userName: string, password: string) {
        const body = { userName, password };
        const promise = new Promise((resolve, reject) => {
            this.http.post(environment.BASE_API + '/api/auth', body, { headers: this.headers })
                .subscribe((response: any) => {
                    console.log(response    );
                    this.logout();
                    this.saveToken(response);
                    this.startRefreshTokenTimer();
                    resolve(true);
                }, error => {
                    reject(error);
                });
        });

        return await promise;
    }

    logout() {
        localStorage.removeItem(SystemConstants.CURRENT_USER);
        this.stopRefreshTokenTimer();
    }

    isUserAuthenticated() {
        return localStorage.getItem(SystemConstants.CURRENT_USER) != null;
    }

    getLoggedInUser() {
        if (!this.isUserAuthenticated()) return null;
        const obj = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER) || '');
        const decodedToken = this.decodeJwt(obj.token);
        const user = Object.assign({}, obj, decodedToken.payload)
        return user;
    }

    saveToken(token: any) {
        localStorage.setItem(SystemConstants.CURRENT_USER, JSON.stringify(token));
        const userlogin = this.getLoggedInUser();
        const expires = new Date(userlogin.exp * 1000);
        console.warn('Token expires at', expires);
    }

    //#region refresh token
    refreshToken() {
        const userlogin = this.getLoggedInUser();
        const body = { refreshToken: userlogin.refreshToken };
        return this.http.post(`${environment.BASE_API}/api/auth/refreshToken`, body, { headers: this.headers })
            .subscribe((response: any) => {
                this.logout();
                this.saveToken(response);
                this.startRefreshTokenTimer();
            }, () => {
                this.logout();
                this.toastrService.error(MessageConstants.LOGIN_AGAIN_MSG);
            });
    }

    private refreshTokenTimeout: any;

    startRefreshTokenTimer() {
        if (this.isUserAuthenticated()) {
            const userlogin = this.getLoggedInUser();
            // set a timeout to refresh the token a minute before it expires
            const expires = new Date(userlogin.exp * 1000);
            const timeout = expires.getTime() - Date.now() - (5 * 60 * 1000);
            this.refreshTokenTimeout = setTimeout(() => this.refreshToken(), timeout);
        }
    }

    stopRefreshTokenTimer() {
        clearTimeout(this.refreshTokenTimeout);
    }
    //#endregion refresh token

    //#region helper method
    private decodeJwt(token: any) {
        const stringSplit = token.split('.');
        const tokenObject: any = {};
        tokenObject.raw = tokenObject;
        tokenObject.header = JSON.parse(this.urlBase64Decode(stringSplit[0]));
        tokenObject.payload = JSON.parse(this.urlBase64Decode(stringSplit[1]));

        return (tokenObject);
    }

    private urlBase64Decode(str: any) {
        let output = str.replace(/-/g, '+').replace(/_/g, '/');
        switch (output.length % 4) {
            case 0:
                break;
            case 2:
                output += '==';
                break;
            case 3:
                output += '=';
                break;
            default:
                throw new Error('Illegal base64url string!');
        }

        const result = window.atob(output);
        try {
            return decodeURIComponent(escape(result));
        } catch (err) {
            return result;
        }
    }
    //#endregion helper method
}
