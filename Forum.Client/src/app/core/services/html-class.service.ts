import { Injectable } from '@angular/core';
import * as objectPath from 'object-path';
import { BehaviorSubject } from 'rxjs';
import { LayoutConfigModel } from '../models/layout-config.model';

export interface ClassType {
	header: string[];
	header_mobile: string[];
	header_menu: string[];
	aside_menu: string[];
}

@Injectable()
export class HtmlClassService {
	config?: LayoutConfigModel;
	classes?: ClassType;
	onClassesUpdated$: BehaviorSubject<ClassType>;
	private loaded: string[] = [];

	constructor() {
		this.onClassesUpdated$ = new BehaviorSubject(this.classes!);
	}

	setConfig(layoutConfig: LayoutConfigModel) {
		this.config = layoutConfig;

		this.classes = {
			header: [],
			header_mobile: [],
			header_menu: [],
			aside_menu: [],
		};

		this.initLayout();
		this.initLoader();

		this.initHeader();
		this.initSubheader();

		this.initAside();

		this.initFooter();

		this.initSkins();

		this.onClassesUpdated$.next(this.classes);
	}

	getClasses(path?: string, toString?: boolean): ClassType | string[] | string {
		if (path) {
			const classes = objectPath.get(this.classes!, path) || '';
			if (toString && Array.isArray(classes)) {
				return classes.join(' ');
			}
			return classes.toString();
		}
		return this.classes!;
	}

	private initLayout() {
		if (objectPath.has(this.config!, 'self.body.class')) {
			const selfBodyClass = (objectPath.get(this.config!, 'self.body.class')).toString();
			if (selfBodyClass) {
				const bodyClasses: string[] = selfBodyClass.split(' ');
				bodyClasses.forEach(cssClass => document.body.classList.add(cssClass));
			}
		}

		if (objectPath.get(this.config!, 'self.layout') === 'boxed' && objectPath.has(this.config!, 'self.body.background-image')) {
			document.body.style.backgroundImage = 'url("' + objectPath.get(this.config!, 'self.body.background-image') + '")';
		}
	}


	private initLoader() {
	}

	private initHeader() {
		if (objectPath.get(this.config!, 'header.self.fixed.desktop')) {
			document.body.classList.add('header-fixed');
			objectPath.push(this.classes!, 'header', 'header-fixed');
		} else {
			document.body.classList.add('header-static');
		}

		if (objectPath.get(this.config!, 'header.self.fixed.mobile')) {
			document.body.classList.add('header-mobile-fixed');
			objectPath.push(this.classes!, 'header_mobile', 'header-mobile-fixed');
		}

		if (objectPath.get(this.config!, 'header.menu.self.layout')) {
			objectPath.push(this.classes!, 'header_menu', 'header-menu-layout-' + objectPath.get(this.config!, 'header.menu.self.layout'));
		}
	}

	private initSubheader() {
		if (objectPath.get(this.config!, 'subheader.fixed')) {
			document.body.classList.add('subheader-fixed');
		}

		if (objectPath.get(this.config!, 'subheader.display')) {
			document.body.classList.add('subheader-enabled');
		}

		if (objectPath.has(this.config!, 'subheader.style')) {
			document.body.classList.add('subheader-' + objectPath.get(this.config!, 'subheader.style'));
		}
	}

	private initAside() {
		if (objectPath.get(this.config!, 'aside.self.display') !== true) {
			return;
		}

		document.body.classList.add('aside-enabled');

		if (objectPath.get(this.config!, 'aside.self.fixed')) {
			document.body.classList.add('aside-fixed');
			objectPath.push(this.classes!, 'aside', 'aside-fixed');
		} else {
			document.body.classList.add('aside-static');
		}

		if (objectPath.get(this.config!, 'aside.self.minimize.default')) {
			document.body.classList.add('aside-minimize');
		}

		if (objectPath.get(this.config!, 'aside.menu.dropdown')) {
			objectPath.push(this.classes!, 'aside_menu', 'aside-menu-dropdown');
		}
	}

	private initFooter() {
		if (objectPath.get(this.config!, 'footer.self.fixed')) {
			document.body.classList.add('footer-fixed');
		}
	}

	private initSkins() {
		if (objectPath.get(this.config!, 'header.self.skin')) {
			document.body.classList.add('header-base-' + objectPath.get(this.config!, 'header.self.skin'));
		}
		if (objectPath.get(this.config!, 'header.menu.desktop.submenu.skin')) {
			document.body.classList.add('header-menu-' + objectPath.get(this.config!, 'header.menu.desktop.submenu.skin'));
		}
		if (objectPath.get(this.config!, 'brand.self.skin')) {
			document.body.classList.add('brand-' + objectPath.get(this.config!, 'brand.self.skin'));
		}
		if (objectPath.get(this.config!, 'aside.self.skin')) {
			document.body.classList.add('aside-' + objectPath.get(this.config!, 'aside.self.skin'));
		}
	}
}
