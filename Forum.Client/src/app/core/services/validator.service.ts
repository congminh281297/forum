import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Injectable()
export class ValidatorService {
    isControlValid(control: AbstractControl): boolean {
        return control.valid && (control.dirty || control.touched);
    }

    isControlInvalid(control: AbstractControl): boolean {
        return control.invalid && (control.dirty || control.touched);
    }

    controlHasError(validation: string, control: AbstractControl): boolean {
        return control.hasError(validation) && (control.dirty || control.touched);
    }

    isControlTouched(control: AbstractControl): boolean {
        return control.dirty || control.touched;
    }

    mustMatch(controlName: string, matchingControlName: string) {
        return (formGroup: FormGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];

            if (matchingControl.errors && !matchingControl.errors['mustMatch']) {
                // return if another validator has already found an error on the matchingControl
                return;
            }

            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            } else {
                matchingControl.setErrors(null);
            }
        }
    }
}