import { ConfirmDeleteComponent } from './../../components/confirm-delete/confirm-delete.component';
import { Injectable } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Injectable()
export class DialogService {
    constructor(private modalService: NgbModal) { }

    open_dialog_confirm_delete(callback: any, title?: string, question?: string) {
        let modalRef = this.modalService.open(ConfirmDeleteComponent,
            {
                backdrop: 'static',
                keyboard: false
            });
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.question = question;
        modalRef.componentInstance.callback = callback;
        modalRef.result.then(async () => {

        }, () => {

        });
    }
}