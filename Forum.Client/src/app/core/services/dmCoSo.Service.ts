import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClientService } from './http-client.service';

@Injectable({
    providedIn: 'root',
})
export class DMCoSoService {
    private _dataSubject: BehaviorSubject<any> = new BehaviorSubject([]);
    public readonly data$: Observable<any> = this._dataSubject.asObservable();

    constructor(private httpClientService: HttpClientService) {
        this.loadData();
    }

    loadData() {
        this.httpClientService.get('/api/dmcoso/getselect')
            .subscribe(res => this._dataSubject.next(res));
    }
}