import { Injectable } from '@angular/core';
import * as moment from 'moment';
@Injectable({
    providedIn: 'root',
})
export class DateTimeService {
    /**Lấy Ngày bỏ giờ */
    public getdate_truncate_time(date: any = null): Date {
        let _date: Date = new Date();
        if (!date)
            _date = new Date();
        else if (date instanceof Date)
            _date = moment(date).toDate();
        else if (date instanceof String || typeof date === 'string')
            _date = new Date(date.toString())
        _date.setHours(0, 0, 0, 0);
        return _date;
    }
    /**Lấy giờ bỏ ngày */
    gettime_truncate_date(date: any = null): string {
        let _date: Date = new Date();
        if (!date)
            _date = new Date();
        else if (date instanceof Date)
            _date = moment(date).toDate();
        else if (date instanceof String || typeof date === 'string')
            _date = new Date(date.toString())
        return ('00' + _date.getHours()).slice(-2) + ':' + ('00' + _date.getMinutes()).slice(-2)
    }
    /**Nối thành ngày giờ */
    getdatetime(date: Date, time: string): Date {
        if (time == null)
            return this.getdate_truncate_time(date);
        const _time = time.split(':');
        let _date = new Date(date);
        _date.setHours(parseInt(_time[0]), parseInt(_time[1]), 0, 0);
        return _date;
    }
    /**Chuyển thành kiểu ngày của server
     * @date Ngày giờ client
    */
    formatdate_toserve(date: any): string | null {
        if (date && (date instanceof Date))
            return moment(date).format('YYYY-MM-DD');
        return null;
    }
    /**Chuyển thành kiều ngày giờ của server
     * @date Ngày giờ client
     */
    formatdatetiem_toserve(date: any): string | null {
        if (date && (date instanceof Date))
            return moment(date).format('YYYY-MM-DDTHH:mm:ss');
        return null;
    }
    /**Chuyển thành kiểu ngày của client 
     * @date Ngày giờ server
     */
    formatdate_toclient(date: any): Date | null {
        if (date)
            return new Date(date);
        return null;
    }
    /**Format ngày giờ kiểu MM/YYYY*/
    formatdate_mmyyyy(date: any): string | null {
        let _date: Date = new Date();
        if (!date)
            return '';
        else if (date instanceof Date)
            _date = date;
        else if (date instanceof String || typeof date === 'string')
            _date = new Date(date.toString())
        return moment(date).format('MM-YYYY');
    }
    /**Format ngày giờ kiểu DD/MM/YYYY*/
    formatdate_ddmmyyyy(date: any): string | null {
        let _date: Date = new Date();
        if (!date || date == 'null')
            return '';
        else if (date instanceof Date)
            _date = date;
        else if (date instanceof String || typeof date === 'string')
            _date = new Date(date.toString())
        return moment(date).format('DD-MM-YYYY');
    }
    /**Format ngày giờ kiểu DD/MM*/
    formatdate_ddmm(date: any): string | null {
        let _date: Date = new Date();
        if (!date)
            return '';
        else if (date instanceof Date)
            _date = date;
        else if (date instanceof String || typeof date === 'string')
            _date = new Date(date.toString())
        return moment(date).format('DD-MM');
    }
    /**Format ngày giờ kiểu DD/MM*/
    formatdate_topart(date: any): string | null {
        let _date: Date = new Date();
        if (!date)
            return '';
        else if (date instanceof Date)
            _date = date;
        else if (date instanceof String || typeof date === 'string')
            _date = new Date(date.toString())
        return moment(date).format('MMYY');
    }
    /**Format ngày giờ kiểu DD/MM/YYYY HH:mm*/
    formatdate_ddmmyyyyhhmm(date: any): string | null {
        let _date: Date = new Date();
        if (!date)
            return '';
        else if (date instanceof Date)
            _date = date;
        else if (date instanceof String || typeof date === 'string')
            _date = new Date(date.toString())
        return moment(date).format('DD-MM-YYYY HH:mm');
    }

    /**Format time */
    formartdate_hhmm(time: any) {
        return moment(time, 'HH:mm:ss')
    }


    /**format date picker value thành kiều ngày giờ của server */
    format_datepicker_toserver(date: any): string {
        return moment(date, 'DD-MM-YYYY').format('YYYY-MM-DD');
    }

    /**So sánh 2 ngày */
    public compare_date(date1: any, date2: any): boolean {
        const _date1 = this.formatdate_ddmmyyyy(date1);
        const _date2 = this.formatdate_ddmmyyyy(date2);
        return (_date1 == _date2);
    }

    /**So sánh 2 ngày */
    public compare_date_time(date1: any, date2: any): boolean {
        const _date1 = this.formatdate_ddmmyyyyhhmm(date1);
        const _date2 = this.formatdate_ddmmyyyyhhmm(date2);
        return (_date1 == _date2);
    }

    /**Tính ngày */
    calculatorDate(from: any, to: any) {
        from = moment(from);
        to = moment(to);

        var years = to.diff(from, 'year');
        from.add(years, 'years');

        var months = to.diff(from, 'months');
        from.add(months, 'months');

        var days = to.diff(from, 'days');

        return { days: days, months: months, years: years };
    }
}

