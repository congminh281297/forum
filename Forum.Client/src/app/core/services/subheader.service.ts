
import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject, ReplaySubject, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import * as objectPath from 'object-path'
import { PageConfigService } from './page-config.service';
import { MenuConfigService } from './menu-config.service';

export interface Breadcrumb {
	title: string;
	page: string | any;
}

export interface BreadcrumbTitle {
	title: string;
	desc?: string;
}

@Injectable()
export class SubheaderService {
	title$: BehaviorSubject<BreadcrumbTitle> = new BehaviorSubject<BreadcrumbTitle>({ title: '', desc: '' });
	breadcrumbs$: BehaviorSubject<Breadcrumb[]> = new BehaviorSubject<Breadcrumb[]>([]);
	disabled$: Subject<boolean> = new Subject<boolean>();
	private manualBreadcrumbs: any = {};
	private appendingBreadcrumbs: any = {};
	private manualTitle: any = {};

	private asideMenus: any;
	private headerMenus: any;
	private pageConfig: any;

	dateChange = new ReplaySubject<any>(1);
	dateChange$ = this.dateChange.asObservable();

	constructor(
		private router: Router,
		private pageConfigService: PageConfigService,
		private menuConfigService: MenuConfigService) {
		const initBreadcrumb = () => {
			this.pageConfig = this.pageConfigService.getCurrentPageConfig();

			this.headerMenus = objectPath.get(this.menuConfigService.getMenus(), 'header');
			this.asideMenus = objectPath.get(this.menuConfigService.getMenus(), 'aside');

			this.updateBreadcrumbs();

			if (objectPath.get(this.manualTitle, this.router.url)) {
				//this.setTitle(this.manualTitle[this.router.url]);
			} else {
				//this.title$.next(objectPath.get(this.pageConfig, 'page'));
				this.title$.next(this.getTitle(this.router.routerState, this.router.routerState.root));
				const hideSubheader = objectPath.get(this.pageConfig, 'page.subheader');
				this.disabled$.next(typeof hideSubheader !== 'undefined' && !hideSubheader);

				if (objectPath.get(this.manualBreadcrumbs, this.router.url)) {
					this.setBreadcrumbs(this.manualBreadcrumbs[this.router.url]);
				} else {
					this.updateBreadcrumbs();
					if (objectPath.get(this.appendingBreadcrumbs, this.router.url)) {
						this.appendBreadcrumbs(this.appendingBreadcrumbs[this.router.url]);
					}
				}
			}
		};

		initBreadcrumb();

		this.router.events
			.pipe(filter(event => event instanceof NavigationEnd))
			.subscribe(initBreadcrumb);
	}

	updateBreadcrumbs() {
		let breadcrumbs = this.getBreadcrumbs(this.headerMenus);
		if (breadcrumbs.length === 0) {
			breadcrumbs = this.getBreadcrumbs(this.asideMenus);
		}

		if (
			breadcrumbs.length === 1 &&
			breadcrumbs[0].title.indexOf(objectPath.get(this.pageConfig, 'page.title')) !== -1) {
			breadcrumbs = [];
		}

		this.breadcrumbs$.next(breadcrumbs);
	}

	setBreadcrumbs(breadcrumbs: Breadcrumb[] | any[]) {
		this.manualBreadcrumbs[this.router.url] = breadcrumbs;
		this.breadcrumbs$.next(breadcrumbs);
	}

	appendBreadcrumbs(breadcrumbs: Breadcrumb[] | any[]) {
		this.appendingBreadcrumbs[this.router.url] = breadcrumbs;
		const prev = this.breadcrumbs$.getValue();
		this.breadcrumbs$.next(prev.concat(breadcrumbs));
	}

	getBreadcrumbs(menus: any) {
		let url = this.pageConfigService.cleanUrl(this.router.url);
		url = url.replace(new RegExp(/\./, 'g'), '/');

		let breadcrumbs: any[] = [];
		const menuPath = this.getPath(menus, url) || [];
		menuPath.forEach((key: any) => {
			menus = menus[key];
			if (typeof menus !== 'undefined' && menus.title) {
				breadcrumbs.push(menus);
			}
		});

		return breadcrumbs;
	}

	setTitle(title: string) {
		this.manualTitle[this.router.url] = title;
		this.title$.next({ title });
	}

	getTitle(state: any, parent: any): any {
		const data: any[] = [];
		if (parent && parent.snapshot.data && parent.snapshot.data.title) {
			data.push(parent.snapshot.data.title);
		}
		if (state && parent) {
			data.push(... this.getTitle(state, state.firstChild(parent)));
		}
		return data;
	}

	getPath(obj: any, value: any): any {
		if (typeof obj !== 'object') {
			return;
		}
		let path: any[] = [];
		let found = false;

		const search = (haystack: any) => {
			for (const key in haystack) {
				path.push(key);
				if (haystack[key] === value) {
					found = true;
					break;
				}
				if (typeof haystack[key] === 'object') {
					search(haystack[key]);
					if (found) {
						break;
					}
				}
				path.pop();
			}
		};

		search(obj);
		return path;
	}


}
