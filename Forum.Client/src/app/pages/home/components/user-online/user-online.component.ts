import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'user-online',
  template: `
    <div class="card">
      <div class="card-header bg-black text-white">Who is online</div>
      <div class="card-body">
        <p>
          In total there are 2 users online :: 0 registered, 0 hidden and 2
          guests (based on users active over the past 5 minutes) Most users ever
          online was 569 on Sat Jul 11, 2020 11:51 am
        </p>
      </div>
    </div>
  `,
})
export class UserOnlineComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
