import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'statistics',
  template: `
  <div class="card">
      <div class="card-header bg-black text-white">Statistics</div>
      <div class="card-body">
        <p>
        Total posts 19 • Total topics 13 • Total members 11 • Our newest member Marina
        </p>
      </div>
    </div>
  `
})

export class StatisticsComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}
