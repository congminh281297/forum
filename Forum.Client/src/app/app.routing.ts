import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { BaseComponent } from './layouts';
import { HomeComponent } from './pages/home/home.component';
import { RegisterComponent } from './pages/register/register.component';
import { LanguageClientComponent } from './pages/language/language-client.component';
import { LanguageDetailComponent } from './pages/language/language-detail/language-detail.component';

export const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'dang-ky', component: RegisterComponent },
      {
        path: 'ngon-ngu', component: LanguageClientComponent, children: [
          { path: ':id', component: LanguageDetailComponent },
        ]
      }
    ]
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
