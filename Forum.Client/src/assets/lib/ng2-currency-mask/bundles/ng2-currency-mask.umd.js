(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/forms'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('ng2-currency-mask', ['exports', '@angular/core', '@angular/forms', '@angular/common'], factory) :
    (global = global || self, factory(global['ng2-currency-mask'] = {}, global.ng.core, global.ng.forms, global.ng.common));
}(this, (function (exports, core, forms, common) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var CURRENCY_MASK_CONFIG = new core.InjectionToken("currency.mask.config");

    var InputManager = /** @class */ (function () {
        function InputManager(htmlInputElement) {
            this.htmlInputElement = htmlInputElement;
        }
        InputManager.prototype.setCursorAt = function (position) {
            if (this.htmlInputElement.setSelectionRange) {
                this.htmlInputElement.focus();
                this.htmlInputElement.setSelectionRange(position, position);
            }
            else if (this.htmlInputElement.createTextRange) {
                var textRange = this.htmlInputElement.createTextRange();
                textRange.collapse(true);
                textRange.moveEnd("character", position);
                textRange.moveStart("character", position);
                textRange.select();
            }
        };
        InputManager.prototype.updateValueAndCursor = function (newRawValue, oldLength, selectionStart) {
            this.rawValue = newRawValue;
            var newLength = newRawValue.length;
            selectionStart = selectionStart - (oldLength - newLength);
            this.setCursorAt(selectionStart);
        };
        Object.defineProperty(InputManager.prototype, "canInputMoreNumbers", {
            get: function () {
                var haventReachedMaxLength = !(this.rawValue.length >= this.htmlInputElement.maxLength && this.htmlInputElement.maxLength >= 0);
                var selectionStart = this.inputSelection.selectionStart;
                var selectionEnd = this.inputSelection.selectionEnd;
                var haveNumberSelected = (selectionStart != selectionEnd && this.htmlInputElement.value.substring(selectionStart, selectionEnd).match(/\d/)) ? true : false;
                var startWithZero = (this.htmlInputElement.value.substring(0, 1) == "0");
                return haventReachedMaxLength || haveNumberSelected || startWithZero;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(InputManager.prototype, "inputSelection", {
            get: function () {
                var selectionStart = 0;
                var selectionEnd = 0;
                if (typeof this.htmlInputElement.selectionStart == "number" && typeof this.htmlInputElement.selectionEnd == "number") {
                    selectionStart = this.htmlInputElement.selectionStart;
                    selectionEnd = this.htmlInputElement.selectionEnd;
                }
                else {
                    var range = document.getSelection().anchorNode;
                    if (range && range.firstChild == this.htmlInputElement) {
                        var lenght = this.htmlInputElement.value.length;
                        var normalizedValue = this.htmlInputElement.value.replace(/\r\n/g, "\n");
                        var startRange = this.htmlInputElement.createTextRange();
                        var endRange = this.htmlInputElement.createTextRange();
                        endRange.collapse(false);
                        if (startRange.compareEndPoints("StartToEnd", endRange) > -1) {
                            selectionStart = selectionEnd = lenght;
                        }
                        else {
                            selectionStart = -startRange.moveStart("character", -lenght);
                            selectionStart += normalizedValue.slice(0, selectionStart).split("\n").length - 1;
                            if (startRange.compareEndPoints("EndToEnd", endRange) > -1) {
                                selectionEnd = lenght;
                            }
                            else {
                                selectionEnd = -startRange.moveEnd("character", -lenght);
                                selectionEnd += normalizedValue.slice(0, selectionEnd).split("\n").length - 1;
                            }
                        }
                    }
                }
                return {
                    selectionStart: selectionStart,
                    selectionEnd: selectionEnd
                };
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(InputManager.prototype, "rawValue", {
            get: function () {
                return this.htmlInputElement && this.htmlInputElement.value;
            },
            set: function (value) {
                this._storedRawValue = value;
                if (this.htmlInputElement) {
                    this.htmlInputElement.value = value;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(InputManager.prototype, "storedRawValue", {
            get: function () {
                return this._storedRawValue;
            },
            enumerable: true,
            configurable: true
        });
        return InputManager;
    }());

    var InputService = /** @class */ (function () {
        function InputService(htmlInputElement, options) {
            this.htmlInputElement = htmlInputElement;
            this.options = options;
            this.inputManager = new InputManager(htmlInputElement);
        }
        InputService.prototype.addNumber = function (keyCode) {
            if (!this.rawValue) {
                this.rawValue = this.applyMask(false, "0");
            }
            var keyChar = String.fromCharCode(keyCode);
            var selectionStart = this.inputSelection.selectionStart;
            var selectionEnd = this.inputSelection.selectionEnd;
            this.rawValue = this.rawValue.substring(0, selectionStart) + keyChar + this.rawValue.substring(selectionEnd, this.rawValue.length);
            this.updateFieldValue(selectionStart + 1);
        };
        InputService.prototype.applyMask = function (isNumber, rawValue) {
            var _a = this.options, allowNegative = _a.allowNegative, decimal = _a.decimal, precision = _a.precision, prefix = _a.prefix, suffix = _a.suffix, thousands = _a.thousands;
            rawValue = isNumber ? new Number(rawValue).toFixed(precision) : rawValue;
            var onlyNumbers = rawValue.replace(/[^0-9]/g, "");
            if (!onlyNumbers) {
                return "";
            }
            var integerPart = onlyNumbers.slice(0, onlyNumbers.length - precision).replace(/^0*/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, thousands);
            if (integerPart == "") {
                integerPart = "0";
            }
            var newRawValue = integerPart;
            var decimalPart = onlyNumbers.slice(onlyNumbers.length - precision);
            if (precision > 0) {
                decimalPart = "0".repeat(precision - decimalPart.length) + decimalPart;
                newRawValue += decimal + decimalPart;
            }
            var isZero = parseInt(integerPart) == 0 && (parseInt(decimalPart) == 0 || decimalPart == "");
            var operator = (rawValue.indexOf("-") > -1 && allowNegative && !isZero) ? "-" : "";
            return operator + prefix + newRawValue + suffix;
        };
        InputService.prototype.clearMask = function (rawValue) {
            if (rawValue == null || rawValue == "") {
                return null;
            }
            var value = rawValue.replace(this.options.prefix, "").replace(this.options.suffix, "");
            if (this.options.thousands) {
                value = value.replace(new RegExp("\\" + this.options.thousands, "g"), "");
            }
            if (this.options.decimal) {
                value = value.replace(this.options.decimal, ".");
            }
            return parseFloat(value);
        };
        InputService.prototype.changeToNegative = function () {
            if (this.options.allowNegative && this.rawValue != "" && this.rawValue.charAt(0) != "-" && this.value != 0) {
                var selectionStart = this.inputSelection.selectionStart;
                this.rawValue = "-" + this.rawValue;
                this.updateFieldValue(selectionStart + 1);
            }
        };
        InputService.prototype.changeToPositive = function () {
            var selectionStart = this.inputSelection.selectionStart;
            this.rawValue = this.rawValue.replace("-", "");
            this.updateFieldValue(selectionStart - 1);
        };
        InputService.prototype.fixCursorPosition = function (forceToEndPosition) {
            var currentCursorPosition = this.inputSelection.selectionStart;
            //if the current cursor position is after the number end position, it is moved to the end of the number, ignoring the prefix or suffix. this behavior can be forced with forceToEndPosition flag
            if (currentCursorPosition > this.getRawValueWithoutSuffixEndPosition() || forceToEndPosition) {
                this.inputManager.setCursorAt(this.getRawValueWithoutSuffixEndPosition());
                //if the current cursor position is before the number start position, it is moved to the start of the number, ignoring the prefix or suffix
            }
            else if (currentCursorPosition < this.getRawValueWithoutPrefixStartPosition()) {
                this.inputManager.setCursorAt(this.getRawValueWithoutPrefixStartPosition());
            }
        };
        InputService.prototype.getRawValueWithoutSuffixEndPosition = function () {
            return this.rawValue.length - this.options.suffix.length;
        };
        InputService.prototype.getRawValueWithoutPrefixStartPosition = function () {
            return this.value != null && this.value < 0 ? this.options.prefix.length + 1 : this.options.prefix.length;
        };
        InputService.prototype.removeNumber = function (keyCode) {
            var _a = this.options, decimal = _a.decimal, thousands = _a.thousands;
            var selectionEnd = this.inputSelection.selectionEnd;
            var selectionStart = this.inputSelection.selectionStart;
            if (selectionStart > this.rawValue.length - this.options.suffix.length) {
                selectionEnd = this.rawValue.length - this.options.suffix.length;
                selectionStart = this.rawValue.length - this.options.suffix.length;
            }
            //there is no selection
            if (selectionEnd == selectionStart) {
                //delete key and the target digit is a number
                if ((keyCode == 46 || keyCode == 63272) && /^\d+$/.test(this.rawValue.substring(selectionStart, selectionEnd + 1))) {
                    selectionEnd = selectionEnd + 1;
                }
                //delete key and the target digit is the decimal or thousands divider
                if ((keyCode == 46 || keyCode == 63272) && (this.rawValue.substring(selectionStart, selectionEnd + 1) == decimal || this.rawValue.substring(selectionStart, selectionEnd + 1) == thousands)) {
                    selectionEnd = selectionEnd + 2;
                    selectionStart = selectionStart + 1;
                }
                //backspace key and the target digit is a number
                if (keyCode == 8 && /^\d+$/.test(this.rawValue.substring(selectionStart - 1, selectionEnd))) {
                    selectionStart = selectionStart - 1;
                }
                //backspace key and the target digit is the decimal or thousands divider
                if (keyCode == 8 && (this.rawValue.substring(selectionStart - 1, selectionEnd) == decimal || this.rawValue.substring(selectionStart - 1, selectionEnd) == thousands)) {
                    selectionStart = selectionStart - 2;
                    selectionEnd = selectionEnd - 1;
                }
            }
            this.rawValue = this.rawValue.substring(0, selectionStart) + this.rawValue.substring(selectionEnd, this.rawValue.length);
            this.updateFieldValue(selectionStart);
        };
        InputService.prototype.updateFieldValue = function (selectionStart) {
            var newRawValue = this.applyMask(false, this.rawValue || "");
            selectionStart = selectionStart == undefined ? this.rawValue.length : selectionStart;
            this.inputManager.updateValueAndCursor(newRawValue, this.rawValue.length, selectionStart);
        };
        InputService.prototype.updateOptions = function (options) {
            var value = this.value;
            this.options = options;
            this.value = value;
        };
        Object.defineProperty(InputService.prototype, "canInputMoreNumbers", {
            get: function () {
                return this.inputManager.canInputMoreNumbers;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(InputService.prototype, "inputSelection", {
            get: function () {
                return this.inputManager.inputSelection;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(InputService.prototype, "rawValue", {
            get: function () {
                return this.inputManager.rawValue;
            },
            set: function (value) {
                this.inputManager.rawValue = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(InputService.prototype, "storedRawValue", {
            get: function () {
                return this.inputManager.storedRawValue;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(InputService.prototype, "value", {
            get: function () {
                return this.clearMask(this.rawValue);
            },
            set: function (value) {
                this.rawValue = this.applyMask(true, "" + value);
            },
            enumerable: true,
            configurable: true
        });
        return InputService;
    }());

    var InputHandler = /** @class */ (function () {
        function InputHandler(htmlInputElement, options) {
            this.inputService = new InputService(htmlInputElement, options);
            this.htmlInputElement = htmlInputElement;
        }
        InputHandler.prototype.handleClick = function (event, chromeAndroid) {
            var selectionRangeLength = Math.abs(this.inputService.inputSelection.selectionEnd - this.inputService.inputSelection.selectionStart);
            //if there is no selection and the value is not null, the cursor position will be fixed. if the browser is chrome on android, the cursor will go to the end of the number.
            if (selectionRangeLength == 0 && !isNaN(this.inputService.value)) {
                this.inputService.fixCursorPosition(chromeAndroid);
            }
        };
        InputHandler.prototype.handleCut = function (event) {
            var _this = this;
            if (this.isReadOnly()) {
                return;
            }
            setTimeout(function () {
                _this.inputService.updateFieldValue();
                _this.setValue(_this.inputService.value);
                _this.onModelChange(_this.inputService.value);
            }, 0);
        };
        InputHandler.prototype.handleInput = function (event) {
            if (this.isReadOnly()) {
                return;
            }
            var keyCode = this.getNewKeyCode(this.inputService.storedRawValue, this.inputService.rawValue);
            var rawValueLength = this.inputService.rawValue.length;
            var rawValueSelectionEnd = this.inputService.inputSelection.selectionEnd;
            var rawValueWithoutSuffixEndPosition = this.inputService.getRawValueWithoutSuffixEndPosition();
            var storedRawValueLength = this.inputService.storedRawValue.length;
            this.inputService.rawValue = this.inputService.storedRawValue;
            if ((rawValueSelectionEnd != rawValueWithoutSuffixEndPosition || Math.abs(rawValueLength - storedRawValueLength) != 1) && storedRawValueLength != 0) {
                this.setCursorPosition(event);
                return;
            }
            if (rawValueLength < storedRawValueLength) {
                if (this.inputService.value != 0) {
                    this.inputService.removeNumber(8);
                }
                else {
                    this.setValue(null);
                }
            }
            if (rawValueLength > storedRawValueLength) {
                switch (keyCode) {
                    case 43:
                        this.inputService.changeToPositive();
                        break;
                    case 45:
                        this.inputService.changeToNegative();
                        break;
                    default:
                        if (!this.inputService.canInputMoreNumbers || (isNaN(this.inputService.value) && String.fromCharCode(keyCode).match(/\d/) == null)) {
                            return;
                        }
                        this.inputService.addNumber(keyCode);
                }
            }
            this.setCursorPosition(event);
            this.onModelChange(this.inputService.value);
        };
        InputHandler.prototype.handleKeydown = function (event) {
            if (this.isReadOnly()) {
                return;
            }
            var keyCode = event.which || event.charCode || event.keyCode;
            if (keyCode == 8 || keyCode == 46 || keyCode == 63272) {
                event.preventDefault();
                var selectionRangeLength = Math.abs(this.inputService.inputSelection.selectionEnd - this.inputService.inputSelection.selectionStart);
                if (selectionRangeLength == this.inputService.rawValue.length || this.inputService.value == 0) {
                    this.setValue(null);
                    this.onModelChange(this.inputService.value);
                }
                if (selectionRangeLength == 0 && !isNaN(this.inputService.value)) {
                    this.inputService.removeNumber(keyCode);
                    this.onModelChange(this.inputService.value);
                }
                if ((keyCode === 8 || keyCode === 46) && selectionRangeLength != 0 && !isNaN(this.inputService.value)) {
                    this.inputService.removeNumber(keyCode);
                    this.onModelChange(this.inputService.value);
                }
            }
        };
        InputHandler.prototype.handleKeypress = function (event) {
            if (this.isReadOnly()) {
                return;
            }
            var keyCode = event.which || event.charCode || event.keyCode;
            if (keyCode == undefined || [9, 13].indexOf(keyCode) != -1 || this.isArrowEndHomeKeyInFirefox(event)) {
                return;
            }
            switch (keyCode) {
                case 43:
                    this.inputService.changeToPositive();
                    break;
                case 45:
                    this.inputService.changeToNegative();
                    break;
                default:
                    if (this.inputService.canInputMoreNumbers && (!isNaN(this.inputService.value) || String.fromCharCode(keyCode).match(/\d/) != null)) {
                        this.inputService.addNumber(keyCode);
                    }
            }
            event.preventDefault();
            this.onModelChange(this.inputService.value);
        };
        InputHandler.prototype.handleKeyup = function (event) {
            this.inputService.fixCursorPosition();
        };
        InputHandler.prototype.handlePaste = function (event) {
            var _this = this;
            if (this.isReadOnly()) {
                return;
            }
            setTimeout(function () {
                _this.inputService.updateFieldValue();
                _this.setValue(_this.inputService.value);
                _this.onModelChange(_this.inputService.value);
            }, 1);
        };
        InputHandler.prototype.updateOptions = function (options) {
            this.inputService.updateOptions(options);
        };
        InputHandler.prototype.getOnModelChange = function () {
            return this.onModelChange;
        };
        InputHandler.prototype.setOnModelChange = function (callbackFunction) {
            this.onModelChange = callbackFunction;
        };
        InputHandler.prototype.getOnModelTouched = function () {
            return this.onModelTouched;
        };
        InputHandler.prototype.setOnModelTouched = function (callbackFunction) {
            this.onModelTouched = callbackFunction;
        };
        InputHandler.prototype.setValue = function (value) {
            this.inputService.value = value;
        };
        InputHandler.prototype.getNewKeyCode = function (oldString, newString) {
            if (oldString.length > newString.length) {
                return null;
            }
            for (var x = 0; x < newString.length; x++) {
                if (oldString.length == x || oldString[x] != newString[x]) {
                    return newString.charCodeAt(x);
                }
            }
        };
        InputHandler.prototype.isArrowEndHomeKeyInFirefox = function (event) {
            if ([35, 36, 37, 38, 39, 40].indexOf(event.keyCode) != -1 && (event.charCode == undefined || event.charCode == 0)) {
                return true;
            }
            return false;
        };
        InputHandler.prototype.isReadOnly = function () {
            return this.htmlInputElement && this.htmlInputElement.readOnly;
        };
        InputHandler.prototype.setCursorPosition = function (event) {
            var rawValueWithoutSuffixEndPosition = this.inputService.getRawValueWithoutSuffixEndPosition();
            setTimeout(function () {
                event.target.setSelectionRange(rawValueWithoutSuffixEndPosition, rawValueWithoutSuffixEndPosition);
            }, 0);
        };
        return InputHandler;
    }());

    var CURRENCYMASKDIRECTIVE_VALUE_ACCESSOR = {
        provide: forms.NG_VALUE_ACCESSOR,
        useExisting: core.forwardRef(function () { return CurrencyMaskDirective; }),
        multi: true
    };
    var CurrencyMaskDirective = /** @class */ (function () {
        function CurrencyMaskDirective(currencyMaskConfig, elementRef, keyValueDiffers) {
            this.currencyMaskConfig = currencyMaskConfig;
            this.elementRef = elementRef;
            this.keyValueDiffers = keyValueDiffers;
            this.options = {};
            this.optionsTemplate = {
                align: "right",
                allowNegative: true,
                decimal: ".",
                precision: 2,
                prefix: "$ ",
                suffix: "",
                thousands: ","
            };
            if (currencyMaskConfig) {
                this.optionsTemplate = currencyMaskConfig;
            }
            this.keyValueDiffer = keyValueDiffers.find({}).create();
        }
        CurrencyMaskDirective_1 = CurrencyMaskDirective;
        CurrencyMaskDirective.prototype.ngAfterViewInit = function () {
            this.elementRef.nativeElement.style.textAlign = this.options.align ? this.options.align : this.optionsTemplate.align;
        };
        CurrencyMaskDirective.prototype.ngDoCheck = function () {
            if (this.keyValueDiffer.diff(this.options)) {
                this.elementRef.nativeElement.style.textAlign = this.options.align ? this.options.align : this.optionsTemplate.align;
                this.inputHandler.updateOptions(Object.assign({}, this.optionsTemplate, this.options));
            }
        };
        CurrencyMaskDirective.prototype.ngOnInit = function () {
            this.inputHandler = new InputHandler(this.elementRef.nativeElement, Object.assign({}, this.optionsTemplate, this.options));
        };
        CurrencyMaskDirective.prototype.handleBlur = function (event) {
            this.inputHandler.getOnModelTouched().apply(event);
        };
        CurrencyMaskDirective.prototype.handleClick = function (event) {
            this.inputHandler.handleClick(event, this.isChromeAndroid());
        };
        CurrencyMaskDirective.prototype.handleCut = function (event) {
            if (!this.isChromeAndroid()) {
                this.inputHandler.handleCut(event);
            }
        };
        CurrencyMaskDirective.prototype.handleInput = function (event) {
            if (this.isChromeAndroid()) {
                this.inputHandler.handleInput(event);
            }
        };
        CurrencyMaskDirective.prototype.handleKeydown = function (event) {
            if (!this.isChromeAndroid()) {
                this.inputHandler.handleKeydown(event);
            }
        };
        CurrencyMaskDirective.prototype.handleKeypress = function (event) {
            if (!this.isChromeAndroid()) {
                this.inputHandler.handleKeypress(event);
            }
        };
        CurrencyMaskDirective.prototype.handleKeyup = function (event) {
            if (!this.isChromeAndroid()) {
                this.inputHandler.handleKeyup(event);
            }
        };
        CurrencyMaskDirective.prototype.handlePaste = function (event) {
            if (!this.isChromeAndroid()) {
                this.inputHandler.handlePaste(event);
            }
        };
        CurrencyMaskDirective.prototype.isChromeAndroid = function () {
            return /chrome/i.test(navigator.userAgent) && /android/i.test(navigator.userAgent);
        };
        CurrencyMaskDirective.prototype.registerOnChange = function (callbackFunction) {
            this.inputHandler.setOnModelChange(callbackFunction);
        };
        CurrencyMaskDirective.prototype.registerOnTouched = function (callbackFunction) {
            this.inputHandler.setOnModelTouched(callbackFunction);
        };
        CurrencyMaskDirective.prototype.setDisabledState = function (value) {
            this.elementRef.nativeElement.disabled = value;
        };
        CurrencyMaskDirective.prototype.validate = function (abstractControl) {
            var result = {};
            if (abstractControl.value > this.max) {
                result.max = true;
            }
            if (abstractControl.value < this.min) {
                result.min = true;
            }
            return result != {} ? result : null;
        };
        CurrencyMaskDirective.prototype.writeValue = function (value) {
            this.inputHandler.setValue(value);
        };
        var CurrencyMaskDirective_1;
        CurrencyMaskDirective.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [CURRENCY_MASK_CONFIG,] }] },
            { type: core.ElementRef },
            { type: core.KeyValueDiffers }
        ]; };
        __decorate([
            core.Input()
        ], CurrencyMaskDirective.prototype, "max", void 0);
        __decorate([
            core.Input()
        ], CurrencyMaskDirective.prototype, "min", void 0);
        __decorate([
            core.Input()
        ], CurrencyMaskDirective.prototype, "options", void 0);
        __decorate([
            core.HostListener("blur", ["$event"])
        ], CurrencyMaskDirective.prototype, "handleBlur", null);
        __decorate([
            core.HostListener("click", ["$event"])
        ], CurrencyMaskDirective.prototype, "handleClick", null);
        __decorate([
            core.HostListener("cut", ["$event"])
        ], CurrencyMaskDirective.prototype, "handleCut", null);
        __decorate([
            core.HostListener("input", ["$event"])
        ], CurrencyMaskDirective.prototype, "handleInput", null);
        __decorate([
            core.HostListener("keydown", ["$event"])
        ], CurrencyMaskDirective.prototype, "handleKeydown", null);
        __decorate([
            core.HostListener("keypress", ["$event"])
        ], CurrencyMaskDirective.prototype, "handleKeypress", null);
        __decorate([
            core.HostListener("keyup", ["$event"])
        ], CurrencyMaskDirective.prototype, "handleKeyup", null);
        __decorate([
            core.HostListener("paste", ["$event"])
        ], CurrencyMaskDirective.prototype, "handlePaste", null);
        CurrencyMaskDirective = CurrencyMaskDirective_1 = __decorate([
            core.Directive({
                selector: "[currencyMask]",
                providers: [
                    CURRENCYMASKDIRECTIVE_VALUE_ACCESSOR,
                    { provide: forms.NG_VALIDATORS, useExisting: CurrencyMaskDirective_1, multi: true }
                ]
            }),
            __param(0, core.Optional()), __param(0, core.Inject(CURRENCY_MASK_CONFIG))
        ], CurrencyMaskDirective);
        return CurrencyMaskDirective;
    }());

    var CurrencyMaskModule = /** @class */ (function () {
        function CurrencyMaskModule() {
        }
        CurrencyMaskModule = __decorate([
            core.NgModule({
                imports: [
                    common.CommonModule,
                    forms.FormsModule
                ],
                declarations: [
                    CurrencyMaskDirective
                ],
                exports: [
                    CurrencyMaskDirective
                ]
            })
        ], CurrencyMaskModule);
        return CurrencyMaskModule;
    }());

    exports.CURRENCYMASKDIRECTIVE_VALUE_ACCESSOR = CURRENCYMASKDIRECTIVE_VALUE_ACCESSOR;
    exports.CURRENCY_MASK_CONFIG = CURRENCY_MASK_CONFIG;
    exports.CurrencyMaskDirective = CurrencyMaskDirective;
    exports.CurrencyMaskModule = CurrencyMaskModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ng2-currency-mask.umd.js.map
